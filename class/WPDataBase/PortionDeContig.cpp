#include "WPDataBase.h"
#include "librairies/StringExtention/stringExtention.h"
#include <string_view>
#include "class/DataBase/DataBase.h"
PortionDeContig::PortionDeContig(DataBase *db, int from, string_view chemin,
               char formatDuChemin, bool savePaths, int noLigne) {
    this->start = from;
    this->chemin = ListeDeSommetsOrientesAvecPosition();
    int cptLen = 0;
    if (savePaths) {
        this->chemin.initialiser(from);
    } else {
        this->chemin.pointer(noLigne);
    }
    if (formatDuChemin == 'P') { // sommet+,sommet-,sommet+,...

        vector<string_view> path = split(chemin, ',');
        InfosSommet *sommet;
        for (int i = 0; ((long unsigned int)i) < path.size(); i++) {
            string_view nomSommet = string_view(path[i].data(), path[i].size() - 1);
            sommet = db->gS().getSommet(nomSommet);
            if (sommet == 0) { // Le sommet n'existe pas
                cerr << "Le sommet " << nomSommet
                     << " n'existe pas dans la base de données (remplacé par "
                        "une erreur dans le chemin)"
                     << endl;
                if (savePaths) {
                    this->chemin.ajouterErreur();
                }
            } else { // Le sommet existe
                sommet->add1Passage();
                char sensSommet = path[i][path[i].size() - 1];
                if (savePaths) {
                    this->chemin.ajouterSommet(
                        db->gS().getIdSommet(nomSommet), sensSommet,
                        sommet->getLongueur());
                } else {
                    cptLen += sommet->getLongueur();
                }
            }
        }
    } else if (formatDuChemin == 'W') {
        int pos = 0, lenTillNextChevron;
        char sensSommet;
        string nomSommet;
        InfosSommet *sommet;
        while (pos != (int)chemin.size()) {
            sensSommet = chemin[pos];
            if (sensSommet == '>')
                sensSommet = '+';
            if (sensSommet == '<')
                sensSommet = '-';
            pos += 1;
            lenTillNextChevron = getLenTillNextChevron(chemin, pos);
            nomSommet = chemin.substr(pos, lenTillNextChevron);
            sommet = db->gS().getSommet(&nomSommet);
            if (sommet == 0) { // Le sommet n'existe pas
                cerr << "Le sommet " << nomSommet
                     << " n'existe pas dans la base de données (remplacé par "
                        "une erreur dans le chemin)"
                     << endl;
                if (savePaths)
                    this->chemin.ajouterErreur();
            } else { // Le sommet existe
                sommet->add1Passage();
                if (savePaths) {
                    this->chemin.ajouterSommet(
                        db->gS().getIdSommet(nomSommet), sensSommet,
                        sommet->getLongueur());
                } else {
                    cptLen += sommet->getLongueur();
                }
            }
            pos += lenTillNextChevron;
        }

    } else {
        cerr << "Erreur sur le format : à l'appel de PortionDeContig ou ajouterPortionDeContig, "
                "le paramètre formatDuChemin doit etr 'W' ou 'P'"
             << endl;
    }
    if (savePaths) {
        this->end = this->chemin.getEnd();
    } else {
        this->end = cptLen;
    }
};

int PortionDeContig::getStart() { return this->start; };

int PortionDeContig::getEnd() { return this->end; };

bool PortionDeContig::estDansCeContig(int position) {
    return position > this->getStart() && position <= this->getEnd();
};

SommetOriente PortionDeContig::extrairePosition(int position) {
    return this->chemin.extrairePosition(position);
};

ListeDeSommetsOrientesAvecPosition PortionDeContig::getChemin() {
    return this->chemin;
};

string PortionDeContig::str() {
    string str = string("<");
    str += to_string(this->getStart());
    str += " ";
    str += this->chemin.str();
    str += " ";
    str += to_string(this->getEnd());
    str += ">";
    return str;
};