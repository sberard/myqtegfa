#include "WPDataBase.h"



map<string, ListeDeContigs > BddContig::getData() { return this->dt; }

bool BddContig::estDefini(const string &nomDuContig) {
    return dt.find(nomDuContig) != dt.end();
};

bool BddContig::estDefini(const string &nomDuContig, int position) {
    return estDefini(nomDuContig) && dt[nomDuContig].estDefini(position);
};

SommetOriente BddContig::extrairePosition(const string &nomDuContig,
                                           int position) {
    return dt[nomDuContig].extrairePosition(position);
};

void BddContig::ajouterContig(DataBase *db,
                              string_view nomDuContig, int from,
                              string_view chemin, char formatDuChemin,
                              bool savePaths, int noLigne) {
    const string cont(nomDuContig);
    if (!estDefini(cont)) {
        dt[cont] = ListeDeContigs();
    }
    dt[cont].ajouterContig(db, from, chemin, formatDuChemin,
                               savePaths, noLigne);
    return;
};

string BddContig::str(string pred) {
    string str = string("{");

    for (auto [key,value] : dt) {
        str += key;
        str += ":\n";
        str += pred;
        str += "    ";
        str += value.str(pred + string("    "));
        str += "\n";
        str += pred;
    }
    str += "}";
    return str;
};