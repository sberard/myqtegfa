#include "WPDataBase.h"
#include "class/DataBase/DataBase.h"
#include "class/SegmentDataBase/SegmentDataBase.h"
#include <vector>

vector<PortionDeContig > ListeDeContigs::getData() { return this->dt; };

bool ListeDeContigs::estDefini(int position) {
    for (PortionDeContig c : dt) {
        if (c.estDansCeContig(position)) {
            return true;
        }
    }
    return false;
};

SommetOriente ListeDeContigs::extrairePosition(int position) {
    for (PortionDeContig c : dt) {
        if (c.estDansCeContig(position)) {
            return c.extrairePosition(position);
        }
    }
    return SommetOriente();
};

void ListeDeContigs::ajouterContig(DataBase *db, int from,
                                   string_view chemin, char formatDuChemin,
                                   bool savePaths, int noLigne) {
    PortionDeContig c =PortionDeContig(db, from, chemin, formatDuChemin, savePaths, noLigne);
    dt.push_back(c);
};

string ListeDeContigs::str(string pred) {
    string str = string("[\n");
    for (PortionDeContig c : dt) {
        str += pred;
        str += "    ";
        str += c.str();
        str += "\n";
    }
    str += pred;
    str += "]";
    return str;
};