#include "WPDataBase.h"

bool BddHaplotype::estDefini(int idHaplotype) {
    return dt.find(idHaplotype) != dt.end();
};
bool BddHaplotype::estDefini(int idHaplotype, const string &nomDuContig) {
    return estDefini(idHaplotype) && dt[idHaplotype].estDefini(nomDuContig);
};

map<int, BddContig> BddHaplotype::getData() { return dt; };

bool BddHaplotype::estDefini(int idHaplotype, const string &nomDuContig,
                             int position) {
    return estDefini(idHaplotype) &&
           dt[idHaplotype].estDefini(nomDuContig, position);
};

SommetOriente BddHaplotype::extrairePosition(int idHaplotype,
                                              const string &nomDuContig,
                                              int position) {
    return dt[idHaplotype].extrairePosition(nomDuContig, position);
};
void BddHaplotype::ajouterContig(DataBase *db, int idHaplotype,
                                 string_view nomDuContig, int from,
                                 string_view chemin, char formatDuChemin,
                                 bool savePaths, int noLigne) {
    if (!estDefini(idHaplotype)) {
        dt[idHaplotype] = BddContig();
    }
    dt[idHaplotype].ajouterContig(db, nomDuContig, from, chemin,
                                      formatDuChemin, savePaths, noLigne);
    return;
};

string BddHaplotype::str(string pred) {
    string str = string("{");
    map<int, BddContig *>::iterator it;

    for (auto [key, value] : dt) {
        str += "Hap. n°";
        str += to_string(key);
        str += ":\n";
        str += pred;
        str += "    ";
        str += value.str(pred + string("    "));
        str += "\n";
        str += pred;
    }
    str += "}";
    return str;
};