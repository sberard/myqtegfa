#include "WPDataBase.h"

SommetOrienteAvecPosition::SommetOrienteAvecPosition(
    int positionDeFin) { // En cas d'erreur, le sommet d'erreur est construit
    this->id = -1;
    this->dirrection = 'E';
    this->positionDeFin = positionDeFin;
}

SommetOrienteAvecPosition::SommetOrienteAvecPosition(int id, char dirrection,
                                                     int positionDeFin) {
    this->id = id;
    this->dirrection = dirrection;
    this->positionDeFin = positionDeFin;
}

bool SommetOrienteAvecPosition::estErreur() {
    return this->getNumeroSommet() == -1;
}

int SommetOrienteAvecPosition::getNumeroSommet() { return this->id; }
char SommetOrienteAvecPosition::getDirrection() { return this->dirrection; }
int SommetOrienteAvecPosition::getPositionDeFin() {
    return this->positionDeFin;
}
SommetOriente SommetOrienteAvecPosition::extraireSommet() {
    return SommetOriente(this->getNumeroSommet(), this->getDirrection());
}
string SommetOrienteAvecPosition::str() {
    string str = string("(");
    if (this->estErreur()) {
        str += "ERR";
    } else {
        str += "\'";
        str += to_string(this->getNumeroSommet());
        str += this->getDirrection();
        str += "\'>";
        str += to_string(this->getPositionDeFin());
    }
    str += ")";

    return str;
}