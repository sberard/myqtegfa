#include "WPDataBase.h"
#include "librairies/warning/warning.h"

int ListeDeSommetsOrientesAvecPosition::getEnd() {
    if (dt.size() == 0) {
        return 0;
    } else {
        return dt[dt.size() - 1].getPositionDeFin();
    }
};

void ListeDeSommetsOrientesAvecPosition::ajouterSommet(int noSommet,
                                                       char dirrection,
                                                       int longueur) {
    if (dt.size() == 0) { // La liste est vide, on doit donc ajouter le sommet a partir de 0
        dt.push_back(
            SommetOrienteAvecPosition(noSommet, dirrection, longueur + 1));
    }else if (dt.size() == 1){ // La liste contient un element, on doit donc verifier si c'est
            // l'initialisateur (-1,'i',pos)
            // ou le pointeur (-1, 'p', ligne)
        switch(dt[0].getDirrection()){
            case 'i' :{
                int from = this->getEnd();
            dt.erase(dt.begin());
            dt.push_back(SommetOrienteAvecPosition(noSommet, dirrection,
                                                        longueur + from + 1));
                                                        break;
            }
            case 'p' :{
                warning("Impossible d'ajouter un sommet dans une liste pointée !");
                break;
            }
            default:{
                dt.push_back(SommetOrienteAvecPosition(
                noSommet, dirrection, longueur + this->getEnd()));
            }

        }
    }else{ // On doit ajouter le sommet a partir du dernier sans se poser de questions
        dt.push_back(SommetOrienteAvecPosition(noSommet, dirrection,
                                                    longueur + this->getEnd()));
    }
}

void ListeDeSommetsOrientesAvecPosition::ajouterErreur() {
    switch (dt.size()) {
    case 0: // La liste est vide, on doit donc ajouter le sommet a partir de 0
        dt.push_back(SommetOrienteAvecPosition(1));
        break;
    case 1: // La liste contient un element, on doit donc verifier si c'est
            // l'initialisateur (-1,'i',pos)
        if (dt[0].getDirrection() == 'i' &&
            dt[0].getNumeroSommet() == -1) {
            int from = this->getEnd();
            dt.erase(dt.begin());
            dt.push_back(SommetOrienteAvecPosition(from + 1));
        } else if(dt[0].getDirrection() == 'p' &&
            dt[0].getNumeroSommet() == -1){
                warning("Impossible d'ajouter une erreur à une liste pointée");
        } else {
            dt.push_back(SommetOrienteAvecPosition(this->getEnd()));
        }
        break;
    default: // On doit ajouter le sommet a partir du dernier sans se poser de
             // questions
        dt.push_back(SommetOrienteAvecPosition(this->getEnd()));
        break;
    }
}

bool ListeDeSommetsOrientesAvecPosition::contientErreur() {
    for (SommetOrienteAvecPosition s : dt) {
        if (s.estErreur())
            return true;
    }
    return false;
}

void ListeDeSommetsOrientesAvecPosition::initialiser(int from) {
    if (dt.size() == 0) {
        dt.push_back(SommetOrienteAvecPosition(-1, 'i', from - 1));
    } else {
        warning("Tentative de pointer une instance deListeDeSommetsOrientesAvecPosition non vide !");
    }
};

void ListeDeSommetsOrientesAvecPosition::pointer(int ligne){
    if (dt.size() == 0) {
        dt.push_back(SommetOrienteAvecPosition(-1, 'p', ligne));
    } else {
        warning("Tentative de pointer une instance deListeDeSommetsOrientesAvecPosition non vide !");
    }
};

void ListeDeSommetsOrientesAvecPosition::depointer(){
    if (dt.size() == 1 && dt[1].getDirrection() == 'p') {
        dt.pop_back();
    } else {
        warning("Tentative de dépointer une instance deListeDeSommetsOrientesAvecPosition non pointée !");
    }
}

int ListeDeSommetsOrientesAvecPosition::getPointeur(){
    if (dt.size() == 1 && dt[1].getDirrection() == 'p') {
        return dt[1].getPositionDeFin();
    } else {
        warning("Tentative de récuperer le pointeur d' une instance deListeDeSommetsOrientesAvecPosition non pointée !");
        return -1;
    }
}

SommetOriente 
ListeDeSommetsOrientesAvecPosition::extrairePosition(int position) {
    int deb = 0, fin = dt.size(), mil;

    if (dt.size() == 1 && dt[0].getDirrection() == 'i' &&
        dt[0].getNumeroSommet() ==
            -1) { // Si la liste ne contient que l'initialisateur, on renvoie
                  // une erreur
        return SommetOrienteAvecPosition(dt[fin - 1].getPositionDeFin())
            .extraireSommet();
    }

    if (position <=
        dt[deb].getPositionDeFin()) { // Si le sommet cherché est plus petit
                                          // que le 1er, on renvoie le premier
        return dt[0].extraireSommet();

    } else if (position >
               dt[fin - 1]
                   .getPositionDeFin()) { // Si le sommet cherché est plus
                                           // grand strictement que le dernier,
                                           // on renvoie une erreur
        return SommetOrienteAvecPosition(dt[fin - 1].getPositionDeFin())
            .extraireSommet();

    } else { // Sinon on recherche par dichotomie
        bool find = false;
        while (!find) {
            mil = (deb + fin) / 2;
            if (position > dt[mil].getPositionDeFin()) {
                if (position <= dt[mil + 1].getPositionDeFin()) {
                    find = true;
                    mil += 1;
                }
                deb = mil + 1;
            } else {
                if (position > dt[mil - 1].getPositionDeFin()) {
                    find = true;
                }
                fin = mil - 1;
            }
        }
        return dt[mil].extraireSommet();
    }
};

vector<SommetOrienteAvecPosition > 
ListeDeSommetsOrientesAvecPosition::getData() {
    return this->dt;
}

string ListeDeSommetsOrientesAvecPosition::str() {
    string ch = "[";
    if (dt.size() == 1 && dt[0].getDirrection() == 'i' &&
        dt[0].getNumeroSommet() ==
            -1) { // On vérifie si la liste ne contient que l'initialisateur
        ch += " INIT:"; // Si oui on change l'affichage
        ch += to_string(this->getEnd() + 1);
    } else {
        if (dt.size() <= 6) {
            for (SommetOrienteAvecPosition s : dt) {
                ch += " ";
                ch += s.str();
            }
        } else {
            for (int i = 0; i < 3; i++) {
                ch += " ";
                ch += dt[i].str();
            }
            ch += " ..[+";
            ch += to_string(dt.size() - 6);
            ch += "]..";
            for (int i = 0; i < 3; i++) {
                ch += " ";
                ch += dt[dt.size() - 3 + i].str();
            }
        }
    }
    ch += " ]";
    return ch;
};