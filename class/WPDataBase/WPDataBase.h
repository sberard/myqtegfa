/**
 * @file WPDataBase.h
 * @author Nael BB
 * @brief Classes servant au stockage des chemins
 */
#ifndef WP_DATABASE
#define WP_DATABASE
#include <map>
#include <string>
#include <vector>
class DataBase;

using namespace std;

typedef pair<int, char> SommetOriente;
/**
 * @brief Classe représentant un sommet orienté à une certaine position dans un chemin 
 * 
 * Est contenu par #ListeDeSommetsOrientesAvecPosition
 */
class SommetOrienteAvecPosition {
    int id;
    char dirrection;
    int positionDeFin;
  public:
    /**
     * @brief Construit un sommet de type erreur
     *
     * @param positionDeFin
     */
    SommetOrienteAvecPosition(int positionDeFin);
    /**
     * @brief Construit un nouveau sommet orienté avec position
     *
     * @param id
     * @param dirrection
     * @param positionDeFin
     */
    SommetOrienteAvecPosition(int id, char dirrection, int positionDeFin);
    /**
     * @brief Renvoie l'identifiant du sommet
     *
     * @return int
     */
    int getNumeroSommet();
    /**
     * @brief Renvoie la dirrection du sommet
     *
     * @return char
     */
    char getDirrection();
    /**
     * @brief Renvoie la position de fin du sommet dans la chaine qui le
     * contient
     *
     * @return int
     */
    int getPositionDeFin();
    /**
     * @brief Renvoie vrai si ce sommet est une erreur
     *
     * @return true
     * @return false
     */
    bool estErreur();
    /**
     * @brief Transforme en SommetOrienteAvecPosition
     *
     * @return SommetOriente
     */
    SommetOriente extraireSommet();
    /**
     * @brief Permet d'afficher un sommet orienté
     *
     * @return string
     */
    string str();
};

/**
 * @brief Classe représentant une liste de sommets orientés et triée dans l'ordre d'apparition des sommets dans un chemin
 *
 * La classe contient une liste de #SommetOrienteAvecPosition dont la position est calculée automatiquement par la longueur des sommets :
 * Si le sommet en position s à pour positionDeFin la valeur n, le sommet s+1 aura pour positionDeFin la valeur n+k, où k est la longueur du sommet en position s+1
 * 
 * <U>Sommets d'erreur :</U>
 * Dans un cas où un sommet non défini serait détéctée dans le fichier, le logiciel ajoutera une erreur à la place dans la liste, pour conserver le lieu de bug
 * Un sommet d'erreur est identifiable par la methode "estErreur", ou par sa dirrection qui aura la valeur 'E'
 *
 * <U>Initialisation de la liste :</U>
 * Par défaut, la liste commence à 1, la positionDeFin du premier sommet sera sa longueur + 1
 * Mais certaines portions de contig ne commencent pas au caractère 0, donc il est possible d'initialiser la liste à une certaine valeur avec #ListeDeSommetsOrientesAvecPosition::initialiser, le décompte commencera donc de cette valeur.
 * Dans les faits, la liste ne contiendra qu'un élément, un SommetOrienteAvecPosition dont le numéro sera -1, la dirrection 'i', et la position de fin la valeur de l'initialisateur
 * 
 * <U>Transformer la liste en pointeur :</U>
 * Dans le choix du fichier, une option permet de ne pas enregistrer les chemins. 
 * Dans le cas de sélection de cette option, la liste des sommets n'est pas enrtegistrée, mais la ligne ou le chemin est est stockée, sous forme de pointeur, crée avec #ListeDeSommetsOrientesAvecPosition::pointer.
 * Dans les faits, la liste ne contiendra qu'un élément, un SommetOrienteAvecPosition dont le numéro sera -1, la dirrection 'p', et la position de fin le numéro de la ligne ou est écrit le chemin
 * 
 * Classe contenue dans #PortionDeContig
 */
class ListeDeSommetsOrientesAvecPosition {
    vector<SommetOrienteAvecPosition> dt{};

  public:
    /**
     * @brief Initialise la liste à une position de début (par défaut 0)
     *
     * @param from Position de début
     */
    void initialiser(int from);
    /**
     * @brief Transforme la liste en pointeur vers une ligne du fichier
     *
     * @param position Ligne de déclaration du chemin dans le fichier gfa
     */
    void pointer(int position);
    /**
     * @brief Supprime le pointeur et permet d'acceder à une liste vide
     *
     */
    void depointer();
    /**
     * @brief Retourne la ligne ou est définie ce chemin dans le fichier
     *
     * @return int
     */
    int getPointeur();
    /**
     * @brief Ajoute un sommet au chemin
     *
     * La position de fin est calculée automatiquement avec la longueur. Pensez
     * à intialiser la liste si vous ne voulez pas commencer à 0
     *
     * @param noSommet Identifiant du sommet
     * @param dirrection Dirrection du sommet (+ ou -)
     * @param longueur Longueur du sommet
     */
    void ajouterSommet(int noSommet, char dirrection, int longueur);
    /**
     * @brief Retourne le sommet à la position demandée dans le chemin
     *
     * @param position
     * @return SommetOriente
     */
    SommetOriente extrairePosition(int position);
    /**
     * @brief Ajoute une erreur au chemin
     *
     */
    void ajouterErreur();
    /**
     * @brief Renvoie vrai si la liste contient une erreur
     *
     * @return true
     * @return false
     */
    bool contientErreur();
    /**
     * @brief Renvoie la position de fin du dernier sommet de la liste
     *
     * @return int
     */
    int getEnd();
    /**
     * @brief Renvoie un vecteur des sommets du chemin (utile pour iterer par
     * exemple)
     *
     * @return vector<SommetOrienteAvecPosition >
     */
    vector<SommetOrienteAvecPosition> getData();
    /**
     * @brief Permet d'afficher la liste des chemins
     *
     * @return string
     */
    string str();
};
/**
 * @brief Classe représentant une portion de contig : son début, sa fin et sa liste de sommets définie par une #ListeDeSommetsOrientesAvecPosition
 * 
 * Classe contenue dans #ListeDeContig
 */
class PortionDeContig {
    long start;
    long end;
    ListeDeSommetsOrientesAvecPosition chemin;

  public:
    /**
     * @brief Renvoie la liste des sommets de ce chemin (Attention : non
     * itérable)
     *
     * @return ListeDeSommetsOrientesAvecPosition
     */
    ListeDeSommetsOrientesAvecPosition getChemin();
    /**
     * @brief Remplis une portion de contig avec les informations données
     *
     * @param db La base de données générale
     * @param from La position de début du contig
     * @param chemin Le chemin en string, au format >noSom<noSom<noSom... ou
     * noSom+,noSom-,noSom-...
     * @param formatDuChemin 'P' ou 'W' : indique le format du chemin
     * @param savePaths indique si le chemin doit etre enregistré, ou juste le
     * pointeur vers la ligne
     * @param noLigne numéro de la ligne a afficher en cas d'erreur
     */
    PortionDeContig(DataBase *db, int from, string_view chemin,
           char formatDuChemin = 'W', bool savePaths = true, int noLigne = -1);
    /**
     * @brief Renvoie la position de début de cette portion de contig
     *
     * @return int
     */
    int getStart();
    /**
     * @brief Renvoie la position de fin de cette portion de contig
     *
     * @return int
     */
    int getEnd();
    /**
     * @brief Retourne vrai si cette position est définie dans cette portion de
     * contig
     *
     * @param position
     * @return true
     * @return false
     */
    bool estDansCeContig(int position);
    /**
     * @brief Retourne le sommet à cette position dans ce contig
     *
     * @param position
     * @return SommetOriente
     */
    SommetOriente extrairePosition(int position);
    /**
     * @brief Permet d'afficher le contig
     *
     * @return string
     */
    string str();
};
/**
 * @brief Classe modélisant un contig complet, contenant une liste des différentes portions de contig
 * 
 */
class ListeDeContigs {
    vector<PortionDeContig> dt{};

  public:
    /**
     * @brief Renvoie la base de données (utile pour iterer par exemple)
     *
     * @return vector<Contig >
     */
    vector<PortionDeContig> getData();
    /**
     * @brief Retourne vrai si cette position est définie
     *
     * @param position
     * @return true
     * @return false
     */
    bool estDefini(int position);
    /**
     * @brief Extrait le sommet à une position précise d'un chemin
     *
     * @param position La position à extraire
     * @return SommetOriente
     */
    SommetOriente extrairePosition(int position);
    /**
     * @brief Permet d'ajouter un contig à la base de données
     *
     * @param db La base de données générale
     * @param from La position de début du contig
     * @param chemin Le chemin en string, au format >noSom<noSom<noSom... ou
     * noSom+,noSom-,noSom-...
     * @param formatDuChemin 'P' ou 'W' : indique le format du chemin
     * @param savePaths indique si le chemin doit etre enregistré, ou juste le
     * pointeur vers la ligne
     * @param noLigne numéro de la ligne a afficher en cas d'erreur
     */
    void ajouterContig(DataBase *db, int from, string_view chemin,
                       char formatDuChemin = 'W', bool savePaths = true,
                       int noLigne = -1);
    /**
     * @brief Permet d'afficher la base de données
     *
     * @param pred le préfixe des lignes
     * @return string
     */
    string str(string pred = "");
};
/**
 * @brief Classe permetant la navigation entre les #ListeDeContigs
 * 
 */
class BddContig {
    map<string, ListeDeContigs> dt{};

  public:
    /**
     * @brief Renvoie la base de données (utile pour iterer par exemple)
     *
     * @return map<string, ListeDeContigs>
     */
    map<string, ListeDeContigs> getData();
    /**
     * @brief Retourne vrai si le contig existe
     *
     * @param nomDuContig
     * @return true
     * @return false
     */
    bool estDefini(const string &nomDuContig);
    /**
     * @brief Retourne vrai si le contig existe et est défini à cette position
     *
     * @param nomDuContig
     * @param position
     * @return true
     * @return false
     */
    bool estDefini(const string &nomDuContig, int position);
    /**
     * @brief Extrait le sommet à une position précise d'un chemin
     *
     * @param nomDuContig Le nom du contig
     * @param position La position à extraire
     * @return SommetOriente
     */
    SommetOriente extrairePosition(const string &nomDuContig, int position);
    /**
     * @brief Permet d'ajouter un contig à la base de données
     *
     * @param db La base de données générale
     * @param nomDuContig Le nom du contig
     * @param from La position de début du contig
     * @param chemin Le chemin en string, au format >noSom<noSom<noSom... ou
     * noSom+,noSom-,noSom-...
     * @param formatDuChemin 'P' ou 'W' : indique le format du chemin
     * @param savePaths indique si le chemin doit etre enregistré, ou juste le
     * pointeur vers la ligne
     * @param noLigne numéro de la ligne a afficher en cas d'erreur
     */
    void ajouterContig(DataBase *db, string_view nomDuContig, int from,
                       string_view chemin, char formatDuChemin = 'W',
                       bool savePaths = true, int noLigne = -1);
    /**
     * @brief Permet d'afficher la base de données
     *
     * @param pred le préfixe des lignes
     * @return string
     */
    string str(string pred = "");
};
/**
 * @brief Classe permetant la navigation entre les haplotypes en contenant #BddContig
 * 
 */
class BddHaplotype {
    map<int, BddContig> dt{};

  public:
    /**
     * @brief Renvoie la base de données (utile pour iterer par exemple)
     *
     * @return map<int, BddContig >
     */
    map<int, BddContig> getData();
    /**
     * @brief Retourne vrai si l'haplotype existe
     *
     * @param idHaplotype
     * @return true
     * @return false
     */
    bool estDefini(int idHaplotype);
    /**
     * @brief Retourne vrai si l'haplotype et le contig existent
     *
     * @param idHaplotype
     * @param nomDuContig
     * @return true
     * @return false
     */
    bool estDefini(int idHaplotype, const string &nomDuContig);
    /**
     * @brief Retourne vrai si l'haplotype et le contig existent et sont définis
     * à cette position
     *
     * @param idHaplotype
     * @param nomDuContig
     * @param position
     * @return true
     * @return false
     */
    bool estDefini(int idHaplotype, const string &nomDuContig, int position);
    /**
     * @brief Extrait le sommet à une position précise d'un chemin
     *
     * @param idHaplotype Le numéro de l'haplotype
     * @param nomDuContig Le nom du contig
     * @param position La position à extraire
     * @return SommetOriente
     */
    SommetOriente extrairePosition(int idHaplotype, const string &nomDuContig,
                                   int position);
    /**
     * @brief Permet d'ajouter un contig à la base de données
     *
     * @param db La base de données générale
     * @param idHaplotype Le numéro de l'haplotype
     * @param nomDuContig Le nom du contig
     * @param from La position de début du contig
     * @param chemin Le chemin en string, au format >noSom<noSom<noSom... ou
     * noSom+,noSom-,noSom-...
     * @param formatDuChemin 'P' ou 'W' : indique le format du chemin
     * @param savePaths indique si le chemin doit etre enregistré, ou juste le
     * pointeur vers la ligne
     * @param noLigne numéro de la ligne a afficher en cas d'erreur
     */
    void ajouterContig(DataBase *db, int idHaplotype, string_view nomDuContig,
                       int from, string_view chemin, char formatDuChemin = 'W',
                       bool savePaths = true, int noLigne = -1);
    /**
     * @brief Permet d'afficher la base de données
     *
     * @param pred le préfixe des lignes
     * @return string
     */
    string str(string pred = "");
};
/**
 * @brief Classe permetant la navigation entre les differents chemins des individus
 *
 * Une des trois grandes bases de données de #DataBase, représentant les chemins (Walks et Paths) du fichier
 * 
 * Elle est construite via plusieurs couches de classes imbriquées :
 * <ul>
 * <li>#WPDataBase est une modélisation d'un dictionnaire associant un individu à l'ensemble des chemins de ses haplotypes (#BddHaplotype)</li>
 * <li>#BddHaplotype est une modélisation d'un dictionnaire associant un numéro d'haplotype à l'ensemble des chemins de ses contigues (#BddContig)</li>
 * <li>#BddContig est une modélisation d'un dictionnaire associant un contig à l'ensemble des portions de chemin de ce contig (#ListeDeContigs)</li>
 * <li>#ListeDeContigs est une modélisation d'une liste des chemins qui ensemble forment un contig (#PortionDeContig)</li>
 * <li>#PortionDeContig est une modélisation d'un chemin dans le graphe, représentant une longue chaine associée à un morceau de contig (#ListeDeSommetsOrientesAvecPosition)</li>
 * <li>#ListeDeSommetsOrientesAvecPosition est la liste exacte des sommets d'un chemin du graphe (#SommetOrienteAvecPosition)</li>
 * <li>#SommetOrienteAvecPosition est la modélisation d'un sommet, enregistrant sa position dans le chemin</li>
 * </ul>
 *
 */
class WPDataBase {
    map<string, BddHaplotype> dt{};

  public:
    /**
     * @brief Renvoie la base de données (utile pour iterer par exemple)
     *
     * @return map<string, BddHaplotype >
     */
    map<string, BddHaplotype> getData();
    /**
     * @brief Retourne vrai si l'individu existe
     *
     * @param nomIndividu
     * @return true
     * @return false
     */
    bool estDefini(const string &nomIndividu);
    /**
     * @brief Retourne vrai si l'individu et l'haplotype existent
     *
     * @param nomIndividu
     * @param idHaplotype
     * @return true
     * @return false
     */
    bool estDefini(const string &nomIndividu, int idHaplotype);
    /**
     * @brief Retourne vrai si l'individu et l'haplotype et le contig existent
     *
     * @param nomIndividu
     * @param idHaplotype
     * @param nomDuContig
     * @return true
     * @return false
     */
    bool estDefini(const string &nomIndividu, int idHaplotype,
                   const string &nomDuContig);
    /**
     * @brief Retourne vrai si l'individu et l'haplotype et le contig existent
     * et sont définis à cette position
     *
     * @param nomIndividu
     * @param idHaplotype
     * @param nomDuContig
     * @param position
     * @return true
     * @return false
     */
    bool estDefini(const string &nomIndividu, int idHaplotype,
                   const string &nomDuContig, int position);
    /**
     * @brief Extrait le sommet à une position précise d'un chemin
     *
     * @param nomIndividu Le nom de l'individu
     * @param idHaplotype Le numéro de l'haplotype
     * @param nomDuContig Le nom du contig
     * @param position La position à extraire
     * @return SommetOriente
     */
    SommetOriente extrairePosition(const string &nomIndividu, int idHaplotype,
                                   const string &nomDuContig, int position);
    /**
     * @brief Permet d'ajouter un contig à la base de données
     *
     * @param db La base de données générale
     * @param nomIndividu Le nom de l'individu
     * @param idHaplotype Le numéro de l'haplotype
     * @param nomDuContig Le nom du contig
     * @param from La position de début du contig
     * @param chemin Le chemin en string, au format >noSom<noSom<noSom... ou
     * noSom+,noSom-,noSom-...
     * @param formatDuChemin 'P' ou 'W' : indique le format du chemin
     * @param savePaths indique si le chemin doit etre enregistré, ou juste le
     * pointeur vers la ligne
     * @param noLigne numéro de la ligne a afficher en cas d'erreur
     */
    void ajouterContig(DataBase *db, string_view nomIndividu, int idHaplotype,
                       string_view nomDuContig, int from, string_view chemin,
                       char formatDuChemin = 'W', bool savePaths = true,
                       int noLigne = -1);
    /**
     * @brief Permet d'afficher la base de données
     *
     * @param pred le préfixe des lignes
     * @return string
     */
    string str(string pred = "");
};
#endif