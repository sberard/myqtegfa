#include "WPDataBase.h"
#include "class/DataBase/DataBase.h"

map<string, BddHaplotype> WPDataBase::getData() { return dt; };

bool WPDataBase::estDefini(const string &nomIndividu) {
    return dt.find(nomIndividu) != dt.end();
};

bool WPDataBase::estDefini(const string &nomIndividu, int idHaplotype) {
    return estDefini(nomIndividu) && dt[nomIndividu].estDefini(idHaplotype);
};

bool WPDataBase::estDefini(const string &nomIndividu, int idHaplotype,
                           const string &nomDuContig) {
    return estDefini(nomIndividu) &&
           dt[nomIndividu].estDefini(idHaplotype, nomDuContig);
};

bool WPDataBase::estDefini(const string &nomIndividu, int idHaplotype,
                           const string &nomDuContig, int position) {
    return estDefini(nomIndividu) &&
           dt[nomIndividu].estDefini(idHaplotype, nomDuContig, position);
};

SommetOriente WPDataBase::extrairePosition(const string &nomIndividu,
                                            int idHaplotype,
                                            const string &nomDuContig,
                                            int position) {
    const string ind(nomIndividu);
    return dt[ind].extrairePosition(idHaplotype, nomDuContig, position);
};

void WPDataBase::ajouterContig(DataBase *db,
                               string_view nomIndividu, int idHaplotype,
                               string_view nomDuContig, int from,
                               string_view chemin, char formatDuChemin,
                               bool savePaths, int noLigne) {
    const string ind(nomIndividu);
    if (!estDefini(ind)) {
        dt[ind] = BddHaplotype();
    }
    dt[ind].ajouterContig(db, idHaplotype, nomDuContig, from, chemin,
                              formatDuChemin, savePaths, noLigne);
    return;
};

string WPDataBase::str(string pred) {
    string str = string("{");
    map<string, BddHaplotype *>::iterator it;

    for (auto [key, value] : dt) {
        str += key;
        str += ":\n";
        str += pred;
        str += "    ";
        str += value.str(pred + string("    "));
        str += "\n";
        str += pred;
    }
    str += "}";
    return str;
};