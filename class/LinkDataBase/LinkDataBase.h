/**
 * @file LinkDataBase.h
 * @author Nael BB
 * @brief Classe servant au stockage des liens
 */
#ifndef CLASS_LISTE_ADJACENCE_
#define CLASS_LISTE_ADJACENCE_

#include <cstring>
#include <map>
#include <utility>
#include <vector>
using namespace std;

typedef pair<int, char> SommetOriente;
typedef vector<SommetOriente > ListeDeSommetsOrientes;
/**
 * @brief Classe enregistrant les liens et permetant le parcours (vers l'avant uniquement) du graphe
 *
 * Une des trois grandes bases de données de #DataBase, représentant les liens du fichier
 * 
 * Elle est construite sous forme de listes d'adjacences.
 *
 */
class LinkDataBase {
  public :
    map<SommetOriente, ListeDeSommetsOrientes > dt = {};
    /**
     * @brief Renvoie l'ensemble des sommets accesibles en un lien depuis un sommet (revoie la liste d'adjacence du sommet)
     * 
     * @param s sommet
     * @param errorIfNotExists si vrai une erreur est affichée en cas de non-existance du sommet
     * @return ListeDeSommetsOrientes 
     */
    ListeDeSommetsOrientes getAccesible(const SommetOriente & s,
                                         bool errorIfNotExists = false);
    /**
     * @brief Renvoie vrai si le noeud as une liste d'adjacence
     * 
     * @param s 
     * @return true 
     * @return false 
     */
    bool existNode(const SommetOriente & s);
    /**
     * @brief Renvoie vrai si le lien existe
     * 
     * @param start 
     * @param end 
     * @return true 
     * @return false 
     */
    bool existLink(const SommetOriente & start, const SommetOriente &end);
    /**
     * @brief Ajoute un lien 
     * 
     * @param idA id du sommet de début
     * @param sensA sens du sommet de début
     * @param idB id du sommet de fin
     * @param sensB sens du sommet de fin
     * @param errorIfStartNotExists si vrai, une erreur est affichée si le début n'est pas défini
     */
    void addLink(const int& idA, const char & sensA, const int & idB, const char & sensB,
                           bool errorIfStartNotExists = false);
    /**
     * @brief Ajoute un sommet avec une liste vide
     * 
     * @param s 
     * @param errorIfExists si vrai, une erreur est affichée si le sommet existe
     */
    void addNode(SommetOriente s, bool errorIfExists = false);
};

void printSommet(const SommetOriente s);
void printListeSommet(const ListeDeSommetsOrientes l);
void printDictAdj(const LinkDataBase g);

#endif
