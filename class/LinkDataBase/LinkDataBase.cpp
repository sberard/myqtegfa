#include "LinkDataBase.h"
#include <algorithm>
#include <iostream>

void printSommet(const SommetOriente s) {
    cout << "(" << s.first << "," << s.second << ")";
};

void printListeSommet(const ListeDeSommetsOrientes l) {
    SommetOriente s;
    cout << "[";
    for (int i = 0; i < (int)l.size(); i++) {
        cout << " ";
        s = l[i];
        printSommet(s);
    }
    cout << " ]";
}

void printDictAdj(const LinkDataBase *g) {
    for (auto const &[k, v] : g->dt) {
        printSommet(k);
        cout << ":";
        printListeSommet(v);
        cout << endl;
    }
}

ListeDeSommetsOrientes LinkDataBase::getAccesible(const SommetOriente & start,
                                                   bool errorIfNotExists) {
    ListeDeSommetsOrientes l{};
    if (this->existNode(start)) {
        l.insert(l.end(), dt[start].begin(), dt[start].end());
    } else {
        if (errorIfNotExists) {
            cerr << "Warning : Sommet innaccessible, liste vide renvoyée "
                    "(DictionnaireAdjacence::getAccesible)"
                 << " / (ajoutez false pour désactiver ce warning)" << endl;
        }
    }
    return l;
};

bool LinkDataBase::existNode(const SommetOriente & s) {
    return dt.find(s) != dt.end();
};

bool LinkDataBase::existLink(const SommetOriente & start, const SommetOriente &end) {
    return dt.find(start) != dt.end() &&
           find(dt[start].begin(), dt[start].end(), end) !=
               (dt[start]).end();
}

void LinkDataBase::addLink(const int& idA, const char & sensA, const int & idB, const char & sensB,
                           bool errorIfStartNotExists) {
    SommetOriente start{idA,sensA};
    SommetOriente end{idB,sensB};
    if (existNode(start)) {
        dt[start].push_back(end);
    } else if (errorIfStartNotExists) {
        cerr << "Warning : tentative d'ajouter un lien partant d'un sommet non "
                "initialisé dans le graphe : ";
        printSommet(start);
        cout << " / (ajoutez false pour désactiver ce warning)" << endl;
    } else {
        ListeDeSommetsOrientes l{};
        l.push_back(end);
        dt[start] = l;
    }
    return;
};

void LinkDataBase::addNode(SommetOriente s, bool errorIfExists) {
    if (existNode(s)) {
        if (errorIfExists) {
            cerr << "Warning : tentative d'ajouter un sommet qui existe déjà "
                    "dans le "
                    "graphe : ";
            printSommet(s);
            cout << " / (ajoutez false pour désactiver ce warning)" << endl;
        }
    } else {
        ListeDeSommetsOrientes l{};
        dt[s] = l;
    }
}
