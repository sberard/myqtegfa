#include "DataBase.h"
#include "class/LinkDataBase/LinkDataBase.h"

SommetOriente inverser(const SommetOriente &origine) {
    SommetOriente s{origine.first, (origine.second == '+' ? '-' : '+')};
    return s;
};

ListeDeSommetsOrientes
DataBase::getSubDirectedGraphRange(const ListeDeSommetsOrientes &sommets,
                                   long n,
                                   bool separerSens) { // Distance en liens
    ListeDeSommetsOrientes ret{}; // Definition des variables
    queue<pair<SommetOriente, int>> file{};
    for (SommetOriente s : sommets) { // Ajout des sommets de base dans la file
        if (find(ret.begin(), ret.end(), s) == ret.end()) {
            if(n>0) {file.push(pair<SommetOriente, int>(s, 0));}
            ret.push_back(s);

            if (!separerSens) {
                if(n>0) {file.push(pair<SommetOriente, int>(inverser(s), 0));}
                ret.push_back(inverser(s));
            }
        }
    }

    pair<SommetOriente, int> s;
    while (!file.empty()) { // Tant que la file n'est pas vide :
        s = file.front();   // On défile un élément de la file
        for (SommetOriente v : this->gL().getAccesible(
                 s.first)) { // Pour chaque voisin de ce sommet
            if (find(ret.begin(), ret.end(), v) ==
                ret.end()) {      // Si on ne l'a pas déjà vu
                ret.push_back(v); // On l'ajoute
                if (!separerSens) {
                    ret.push_back(inverser(v));
                }
                if (s.second + 1 <
                    n) { // Et si ses voisins sont a distance correcte
                    file.push(pair<SommetOriente, int>(
                        v, s.second + 1)); // On les ajoutera
                    if (!separerSens) {
                        file.push(pair<SommetOriente, int>(inverser(v),
                                                           s.second + 1));
                    }
                }
            }
        }
        file.pop();
    }
    return ret;
};

ListeDeSommetsOrientes
DataBase::getSubDirectedGraphDist(const ListeDeSommetsOrientes &sommets, int n,
                                  bool separerSens) { // Distance en chaines
    // Initialisation des listes acc et reste
    ListeDeSommetsOrientes acc{};
    map<SommetOriente, long> reste{};
    // On ajoute les sommets à acc et a reste dans tous les cas
    for (SommetOriente s : sommets) {
        if (find(acc.begin(), acc.end(), s) == acc.end()) {
            acc.push_back(s);
            if (!separerSens) {
                acc.push_back(inverser(s));
            }
            (reste)[s] = 0;
            if (!separerSens) {
                (reste)[inverser(s)] = 0;
            }
        }
    }
    long longS;
    // Tant qu'il reste des sommets dont tous les voisins sont accesibles
    while (!reste.empty()) {
        // On prends le sommet s de distance minimale dans reste et on le
        // supprime de reste
        pair<SommetOriente, long> sommetDistanceMinimale = *min_element(
            reste.begin(), reste.end(),
            [](const auto &l, const auto &r) { return l.second < r.second; });
        reste.erase(sommetDistanceMinimale.first);
        // Pour chacun de ses voisins v:
        for (SommetOriente v :
             this->gL().getAccesible(sommetDistanceMinimale.first)) {
            longS = sommetDistanceMinimale.second +
                    this->gS().getSommet(v.first)->getLongueur();
            if (find(acc.begin(), acc.end(), v) ==
                acc.end()) { // S'il n'est pas dans acc : on l'ajoute a reste
                             // et a acc avec comme longueur long(s)+long(v)
                if (longS < n) {
                    (reste)[v] = longS;
                    if (!separerSens) {
                        (reste)[inverser(v)] = longS;
                    }
                }
                acc.push_back(v);
                if (!separerSens) {
                    acc.push_back(inverser(v));
                }
            }
        }
    }
    // On renvoie acc
    return acc;
};
