#include "DataBase.h"
SegmentDataBase & DataBase::getSommets() { return sommets; };
LinkDataBase & DataBase::getLiens() { return liens; };
WPDataBase & DataBase::getChemins() { return chemins; };
vector<pair<long, string>> DataBase::getErrors() { return errors; };
void DataBase::addError(string err) {
    this->errors.push_back(pair<long, string>(-1, err));
};
void DataBase::addError(long line, string err) {
    this->errors.push_back(pair<long, string>(line, err));
};
void DataBase::vider() {
    lastInd.clear();
    lastHap = -1;
    lastCont.clear();
    sommets = SegmentDataBase();
    liens = LinkDataBase();
    chemins = WPDataBase();
    nbLignes = 0;
    header.clear();
    errors = vector<pair<long, string>>();
    degresIn = map<long, long>();
    degresOut = map<long, long>();
    degresTot = map<long, long>();
    tailles = map<long, long>();
    passages = map<long, long>();
    nomFichier = QString("Aucun fichier analysé");
    checkErrors = false;
    pathOrWalks = 'P';
    nbLignes = 0;
    nbLiens = 0;
    nbPW = 0;
    selections.clear();
    selectionToView = "";
};
bool DataBase::sommetsVide() {
    return degresIn.empty() && degresOut.empty() && degresTot.empty() &&
           tailles.empty() && passages.empty();
};
