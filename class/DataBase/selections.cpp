#include "DataBase.h"
#include "class/LinkDataBase/LinkDataBase.h"
#include "librairies/warning/warning.h"
#include <algorithm>
#include <iterator>
#include <vector>
void DataBase::addSelection(const string & nomSelection){
    this->selections[nomSelection] = {};
};
void DataBase::addSelection(const string & nomSelection, const vector<SommetOriente> & choisis){
    this->selections[nomSelection] = choisis;
};
void DataBase::deleteSelection(const string & nomSelection){
    this->selections.erase(nomSelection);
};
void DataBase::interSelections(const vector<string> & in, const string & out){
    if(in.size() == 0){
        this->selections[out] = vector<SommetOriente>();
        return;
    }
    vector<SommetOriente> temp = selections[in[0]] ;
    vector<SommetOriente> temp2{} ;
    for(string select : in){
        set_intersection(temp.cbegin(), temp.cend(), selections[select].cbegin(), selections[select].cend(), back_inserter(temp2));
        temp = temp2;
        temp2.clear();
    }
    this->selections[out] = temp;
};
void DataBase::unionSelections(const vector<string> & in, const string & out){
    vector<SommetOriente> temp{}, temp2{};
    for(string select : in){
        set_union(temp.cbegin(), temp.cend(), selections[select].cbegin(), selections[select].cend(), back_inserter(temp2));
        temp = temp2;
        temp2.clear();
    }
    this->selections[out] = temp;
};
const vector<SommetOriente> & DataBase::getSelection(const string & nomSelection){
    return this->selections[nomSelection];
};
void DataBase::exportSelection(const string & nomSelection, const string & nomFichier){//TODO faire l'import export
    ofstream fileSEL(nomFichier);
    if (!fileSEL.is_open()) {
        warning("Impossible d'ouvrir ou creer le fichier de sortie");
        return;
    }
    fileSEL << nomSelection << endl;
    for(SommetOriente i : selections[nomSelection]){
        fileSEL <<  gS().getNameSommet(i.first) << i.second;
    }
    fileSEL.close();
    
};

void DataBase::importSelection(const string & nomFichier){
    ifstream fileSEL(nomFichier);
    if (!fileSEL.is_open()) {
        warning("Impossible d'ouvrir le fichier à importer");
        return;
    }
    string nomSelection;
    if(! getline(fileSEL,nomSelection)){
        warning("Erreur dans la lecture du nom de la séléction ! Le fichier doit comporter plus d'une ligne (Nom de séléction)");
    };
    string line;
    vector<SommetOriente> selection{};
    if(! getline(fileSEL,line)){
        addSelection(nomSelection);
    }else{
        auto s = line.begin();
        for(auto it = line.begin(); it< line.end(); it++){
            if(*it == '+' || *it == '-'){
                selection.push_back(SommetOriente( gS().getIdSommet(string_view(line.data() + (s - line.begin()), it - s)), *it));
                s = it+1;
            }
        }
    }
    addSelection(nomSelection,selection);
    
    fileSEL.close();
};
map<string,vector<SommetOriente>> DataBase::getSelections(){
    return this->selections;
}