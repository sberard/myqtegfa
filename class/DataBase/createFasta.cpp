#include "DataBase.h"
#include "librairies/StringExtention/stringExtention.h"
#include "librairies/warning/warning.h"
#define NOMBRE_COLUMNS 60
void DataBase::createFasta(string nomFichier, string individu) {
    ofstream fileFASTA(nomFichier);
    if (!fileFASTA.is_open()) {
        warning("Impossible d'ouvrir ou creer le fichier fasta");
        return;
    }
    if (!gC().estDefini(individu)) {
        warning("Individu " + individu + " innexistant");
        return;
    }
    string numIndividu = individu.substr(5);
    string textOfSom;
    short column = 0;
    int reste = 0;
    for (auto [numHap, bddcontig] : gC().getData()[individu].getData()) {
        for (auto [nomContig, listecontig] : bddcontig.getData()) {
            fileFASTA << ">lcl|" << numIndividu << "|" << nomContig << endl;
            column = 0;
            for (PortionDeContig contig : listecontig.getData()) {
                for (SommetOrienteAvecPosition som :
                     contig.getChemin().getData()) {
                    textOfSom = this->gS()
                                    .getSommet(som.getNumeroSommet())
                                    ->getChaine();
                    if (som.getDirrection() == '-') {
                        textOfSom = reverseComplement(textOfSom);
                    }
                    reste = textOfSom.size();
                    //*Remplissage de la ligne déjà existante :
                    if (column != 0) {
                        if (reste < NOMBRE_COLUMNS - column) { // On peut vider
                                                               // text dans col
                            fileFASTA << textOfSom;
                            column += reste;
                            reste = 0;
                        } else { // On remplis la ligne jusqu'au bout
                            fileFASTA
                                << textOfSom.substr(0, NOMBRE_COLUMNS - column)
                                << endl;
                            textOfSom =
                                textOfSom.substr(NOMBRE_COLUMNS - column);
                            reste -= NOMBRE_COLUMNS - column;
                            column = 0;
                        }
                    }
                    //* remplissage de lignes
                    while (reste - NOMBRE_COLUMNS >= 0) {
                        fileFASTA << textOfSom.substr(0, NOMBRE_COLUMNS)
                                  << endl;
                        textOfSom = textOfSom.substr(NOMBRE_COLUMNS);
                        reste -= NOMBRE_COLUMNS;
                        column = 0;
                    }
                    //* remplissage de la derniere ligne
                    if (reste) {
                        fileFASTA << textOfSom;
                        column = reste;
                    }
                }
            }
            fileFASTA << endl;
        }
    }
    information(string("Construction du fichier ") + nomFichier +
                string(" terminé"));
    fileFASTA.close();
};
