/**
 * @file DataBase.h
 * @author Nael BB
 * @brief Classe principale de stockage des données, comprenant des fonctions de
 * parcours des informations du graphe de pangénome
 */
#ifndef PAGES_DATA_BASE_
#define PAGES_DATA_BASE_
#include "../LinkDataBase/LinkDataBase.h"
#include "../SegmentDataBase/SegmentDataBase.h"
#include "class/WPDataBase/WPDataBase.h"
#include <QString>

#define gS() getSommets()
#define gL() getLiens()
#define gC() getChemins()
#define gE() getErrors()
/**
 * @brief Inverse le sens d'un SommetOriente : - devient + et + devient -
 * @param origine
 * @return SommetOriente
 */
SommetOriente inverser(const SommetOriente &origine);
/**
 * @brief Classe principale servant à la gestion de la mémoire commune de
 * l'application, partagée automatiquement avec les pages
 *
 *
 * <U>Informations stockées :</U>
 * <ul>
 * <li>
  * Graphe de pangénome :
  *    <ul>
  *        <li>Sommets (Grace à #SegmentDataBase)</li>
  *        <li>Lien (Grace à #LinkDataBase)</li>
  *        <li>Chemins (Grace à #WPDataBase)</li>
  *        <li>Nombre de lignes, header, type de chemin...</li>
  *    </ul>
 * </li>
 * <li>
  * Erreurs trouvées :<br>
  * Si la case "enregistrer les erreurs" est cochée, alors il est possible
  * d'avoir accès aux erreurs qui seront automatiquement enregistrées dans la base
 * </li>
 * <li>
  * Parametres généraux de l'application :
  *     <ul>
  *         <li>Cases cochées à l'ouverture de l'application</li>
  *         <li>Calculs faits par les pages (distributions des degrés parexemple)</li>
  *     </ul>
 * </li>
 * <li>
  * Sélections :
  * @todo Créer une page réservée au tutoriel séléctions
 * </li>
 * </ul>
 */
class DataBase {
    SegmentDataBase sommets = SegmentDataBase();
    LinkDataBase liens = LinkDataBase();
    WPDataBase chemins = WPDataBase();
    vector<pair<long, string>> errors = vector<pair<long, string>>();

    map<string, vector<SommetOriente>> selections{};

  public:
    /**
     * @brief Permet à QPSelections et QPSelectionView de se passer les
     * informations quand à la sélection à voir
     *
     */
    string selectionToView;
    /**
     * @brief Stocke le dernier individu regardé
     */
    QString lastInd = QString();
    /**
     * @brief Stocke le dernier haplotype regardé
     */
    int lastHap = -1;
    /**
     * @brief Stocke le dernier contig regardé
     */
    QString lastCont = QString();
    /**
     * @brief Stocke le nom du fichier stocké dans cette base de données
     */
    QString nomFichier = QString();
    /**
     * @brief Paramètre "Les erreurs sont elles enregistrées"
     */
    bool checkErrors = false;
    /**
     * @brief Paramètre "Les chaines sont elles enregistrées"
     */
    bool getChaines = true;
    /**
     * @brief Paramètre "Les chemins sont ils enregistrées"
     */
    bool getPaths = true;
    /**
     * @brief Type de chemins du graphe. Reste 'P' dans le cas ou les deux sont
     * là
     */
    char pathOrWalks = 'P';
    /**
     * @brief Nombre de lignes du fichier
     */
    long nbLignes = 0;
    /**
     * @brief Nombre de liens du fichier
     */
    long nbLiens = 0;
    /**
     * @brief Nombre de paths/walks dans le fichier
     */
    long nbPW = 0;
    /**
     * @brief Distribution des degrés entrant calculés lors du premier accès à
     * QPSegmInf
     */
    map<long, long> degresIn{};
    /**
     * @brief Distribution des degrés sortant calculés lors du premier accès à
     * QPSegmInf
     */
    map<long, long> degresOut{};
    /**
     * @brief Distribution des degrés totaux calculés lors du premier accès à
     * QPSegmInf
     */
    map<long, long> degresTot{};
    /**
     * @brief Distribution des tailles entrant calculés lors du premier accès à
     * QPSegmInf
     */
    map<long, long> tailles{};
    /**
     * @brief Distribution des passages entrant calculés lors du premier accès à
     * QPSegmInf
     */
    map<long, long> passages{};
    /**
     * @brief Header du fichier
     */
    string header{""};
    /**
     * @brief Renvoie la base de données des sommets (Dans le code, remplacé par
     * la macro "gS()")
     *
     * @return SegmentDataBase&
     */
    SegmentDataBase &getSommets();
    /**
     * @brief Renvoie la base de données des liens (Dans le code, remplacé par
     * la macro "gL()")
     *
     * @return LinkDataBase&
     */
    LinkDataBase &getLiens();
    /**
     * @brief Renvoie la base de données des chemins (Dans le code, remplacé par
     * la macro "gC()")
     *
     * @return WPDataBase&
     */
    WPDataBase &getChemins();
    /**
     * @brief Renvoie la base de données des erreurs trouvées (Dans le code,
     * remplacé par la macro "gE()")
     *
     * @return vector<pair<long, string>>
     */
    vector<pair<long, string>> getErrors();
    /**
     * @brief Ajoute une erreur sans ligne dans la base de données (ligne -1)
     *
     * @param err
     */
    void addError(string err);
    /**
     * @brief Ajoute une erreur dans la base de données
     *
     * @param line
     * @param err
     */
    void addError(long line, string err);
    /**
     * @brief Retourne vrai si les distributions ne sont pas calculées
     *
     * @return true
     * @return false
     */
    bool sommetsVide();
    /**
     * @brief Vide la base de données
     * @bug Non fonctionnel : une grande partie des données n'est pas supprimée
     *
     */
    void vider();
    /**
     * @brief Retourne tous les sommets à distance inferieure ou égale à n dans
     * le graphe (distance en chaine) en distinguant ou non les sens
     *
     * @param sommets
     * @param n
     * @param separereSens
     * @return ListeDeSommetsOrientes
     */
    ListeDeSommetsOrientes
    getSubDirectedGraphDist(const ListeDeSommetsOrientes &sommets, int n,
                            bool separereSens);
    /**
     * @brief Retourne tous les sommets à distance inferieure ou égale à n dans
     * le graphe (distance en liens) en distinguant ou non les sens
     *
     * @param sommets
     * @param n
     * @param separereSens
     * @return ListeDeSommetsOrientes
     */
    ListeDeSommetsOrientes
    getSubDirectedGraphRange(const ListeDeSommetsOrientes &sommets, long n,
                             bool separereSens);
    /**
     * @brief Crée un fichier fasta du nom de nomFichier contenant tous les
     * contigs de individu
     *
     * @param nomFichier
     * @param individu
     */
    void createFasta(string nomFichier, string individu);

    /**
     * @brief Ajoute une séléction vide dans la base de données
     *
     * @param nomSelection
     */
    void addSelection(const string &nomSelection);
    /**
     * @brief Ajoute une séléction dans la base de données, contenant les
     * sommets choisis
     *
     * @param nomSelection
     * @param choisis
     */
    void addSelection(const string &nomSelection,
                      const vector<SommetOriente> &choisis);
    /**
     * @brief Supprime une séléction dans la base de données
     *
     * @param nomSelection
     */
    void deleteSelection(const string &nomSelection);
    /**
     * @brief Crée l'intersection de plusieurs séléctions
     *
     * @param in
     * @param out
     */
    void interSelections(const vector<string> &in, const string &out);
    /**
     * @brief Crée l'union de plusieurs séléctions
     *
     * @param in
     * @param out
     */
    void unionSelections(const vector<string> &in, const string &out);
    /**
     * @brief Exporte une séléction
     *
     * @param nomSelection
     * @param nomFichier
     */
    void exportSelection(const string &nomSelection, const string &nomFichier);
    /**
     * @brief Importe une séléction
     *
     * @param nomFichier
     */
    void importSelection(const string &nomFichier);
    /**
     * @brief Renvoie une séléction
     *
     * @param nomSelection
     * @return const vector<SommetOriente>&
     */
    const vector<SommetOriente> &getSelection(const string &nomSelection);
    /**
     * @brief Renvoie toutes les séléctions
     *
     */
    map<string, vector<SommetOriente>> getSelections();
};

#endif