#include "class/SegmentDataBase/SegmentDataBase.h"
#include <string>
#include <string_view>
#include <unordered_map>

InfosSommet::InfosSommet(string_view chaine) {
    this->longueur = chaine.size();
    this->chaine = string(chaine);
    this->dIn = 0;
    this->dOut = 0;
    this->passage = 0;
};
InfosSommet::InfosSommet(long longueurChaine) {
    this->longueur = longueurChaine;
    this->chaine = "";
    this->dIn = 0;
    this->dOut = 0;
    this->passage = 0;
};

int InfosSommet::getLongueur() const { return longueur; };

int InfosSommet::getDegresIn() const { return dIn; };
void InfosSommet::add1DegresIn() { dIn++; };

int InfosSommet::getDegresOut() const { return dOut; };
void InfosSommet::add1DegresOut() { dOut++; };
int InfosSommet::getDegresTot() const { return dIn + dOut; };
int InfosSommet::getPassage() const { return passage; };
void InfosSommet::add1Passage() { passage++; };
string InfosSommet::getChaine() const { return chaine; };
string InfosSommet::str() {
    stringstream stream;
    stream << "[" << getLongueur() << "]";
    string str;
    stream >> str;
    return str;
}

// SommetDataBase::

InfosSommet *SegmentDataBase::getSommet(int id) {
    if (id < (int)infos.size()) {
        return &infos[id];
    }
    return 0;
};

InfosSommet *SegmentDataBase::getSommet(const string& name) {
    return &infos[ids[name]];
};

InfosSommet *SegmentDataBase::getSommet(string *name) {
    return getSommet(*name);
};
InfosSommet *SegmentDataBase::getSommet(const string_view& name) {
    return getSommet(string(name));
};

int SegmentDataBase::getIdSommet(const string& name) { return ids[name]; };
int SegmentDataBase::getIdSommet(const string_view& name) { return ids[string(name)]; };
string SegmentDataBase::getNameSommet(int id) { return names[id]; };

int SegmentDataBase::addSommet(const string_view& nomSommet, const string_view& chaine) {
    InfosSommet s{chaine};
    infos.push_back(s);
    ids[string(nomSommet)] = nextId;
    names.push_back(string(nomSommet));
    nextId+=1;
    return nextId-1;
};
int SegmentDataBase::addSommet(const string_view& nomSommet, long longueurChaine) {
    InfosSommet s{longueurChaine};
    infos.push_back(s);
    ids[string(nomSommet)] = nextId;
    names.push_back(string(nomSommet));
    nextId+=1;
    return nextId-1;
};

vector<InfosSommet> SegmentDataBase::getInfos() { return this->infos; }
unordered_map<string, int> SegmentDataBase::getIds() { return this->ids; }

bool SegmentDataBase::exists(const string& name) {
    return ids.find(name) != ids.end();
};

bool SegmentDataBase::exists(int id) { return id < nextId; };

int SegmentDataBase::size() { return nextId; };

string SegmentDataBase::str() {
    string str = string("[ ");
    for (auto [key, value] : ids) {
        str += "(n=";
        str += key;
        str += ":i=";
        str += to_string(value);
        str += ")";
        str += infos[value].str();
        str += " ";
    }
    str += "]";
    return str;
};
