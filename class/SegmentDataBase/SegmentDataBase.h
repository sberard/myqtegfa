/**
 * @file SegmentDataBase.h
 * @author Nael BB
 * @brief Classes servant au stockage des sommets
 */
#ifndef SOMMET_DATA_BASE_
#define SOMMET_DATA_BASE_
#include <bits/stdc++.h>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

using namespace std;
/**
 * @brief Classe représentant les informations utiles sur un sommet, calculé pendant les parcours.
 * 
 * Les informations calculées sont : longueur, degrés entrant, degrés sortant, et chaine si l'option de garder les chaines est cochée (sinon remplacée par "")
 * 
 */
class InfosSommet {
    int longueur;
    int dIn;
    int dOut;
    int passage;
    string chaine;

  public:
    /**
     * @brief Construit un InfosSommet en stockant la chaine
     * 
     * @param chaine 
     */
    InfosSommet(string_view chaine);
    /**
     * @brief Construit un InfosSommet sans stocker la chaine
     * 
     * @param longueurChaine 
     */
    InfosSommet(long longueurChaine);
    /**
     * @brief Renvoie la longueur de la chaine du sommet
     * 
     * @return int 
     */
    int getLongueur() const;
    /**
     * @brief Renvoie le degrés entrant du sommet
     * 
     * @return int 
     */
    int getDegresIn() const;
    /**
     * @brief Ajoute 1 au degres entrant du sommet
     * 
     */
    void add1DegresIn();
    /**
     * @brief Renvoie le degrés sortant du sommet
     * 
     * @return int 
     */
    int getDegresOut() const;
    /**
     * @brief Ajoute 1 au degres sortant du sommet
     * 
     */
    void add1DegresOut();
    /**
     * @brief Renvoie le degrés total du sommet, ie la somme des degres
     * 
     * @return int 
     */
    int getDegresTot() const;
    /**
     * @brief Renvoie le nombre de fois où ce sommet est traversé
     * 
     */
    int getPassage() const;
    /**
     * @brief Ajoute 1 au passage du sommet
     * 
     */
    void add1Passage();
    /**
     * @brief Renvoie la chaine du sommet, "" si elle n'est pas définie
     * 
     * @return string 
     */
    string getChaine() const;
    /**
     * @brief Transforme le sommet en chaine de caracteres
     * 
     * @return string 
     */
    string str();
};
/**
 * @brief Classe permetant la collecte d'informations et l'ajout d'identifiant aux sommets du graphe
 *
 * Une des trois grandes bases de données de #DataBase, représentant les sommets du fichier
 * 
 * Elle est construite comme une liste de #InfosSommet, associé à une map permétant de passer de nom à id, l'autre permetant de passer de id à nom
 *
 * Chaque sommet se voit en effet attribué un identifiant, correspondant à l'ordre d'apparition des sommets dans le fichier (commencant par 0)
 *
 */
class SegmentDataBase {
    int nextId = 0;
    vector<InfosSommet > infos{};
    unordered_map<string, int> ids{};
    vector<string> names{};
  public:
  /**
   * @brief Retourne la liste des #InfosSommet (utile pour iterer)
   * 
   * @return vector<InfosSommet > 
   */
    vector<InfosSommet > getInfos();
    /**
     * @brief Retourne le nom du sommet identifié par id
     * 
     * @param id 
     * @return string 
     */
    string getNameSommet(int id);
    /**
     * @brief Retourne la map nom->id
     * 
     * @return unordered_map<string, int> 
     */
    unordered_map<string, int> getIds();
    /**
     * @brief Retourne les informations du sommet identifié par id
     * 
     * @param id 
     * @return InfosSommet* 
     */
    InfosSommet *getSommet(int id);
    /**
     * @brief Retourne les informations du sommet de nom name
     * 
     * @param name 
     * @return InfosSommet* 
     */
    InfosSommet *getSommet(string *name);
    /**
     * @brief Retourne les informations du sommet de nom name
     * 
     * @param name 
     * @return InfosSommet* 
     */
    InfosSommet *getSommet(const string& name);
    /**
     * @brief Retourne les informations du sommet de nom name
     * 
     * @param name 
     * @return InfosSommet* 
     */
    InfosSommet *getSommet(const string_view& name);
    /**
     * @brief Retourne l'identifiant du sommet de nom name
     * 
     * @param name 
     * @return int 
     */
    int getIdSommet(const string& name);
    /**
     * @brief Retourne l'identifiant du sommet de nom name
     * 
     * @param name 
     * @return int 
     */
    int getIdSommet(const string_view& name);
    /**
     * @brief Ajoute un sommet en enregistrant sa chaine
     * 
     * @param nomSommet 
     * @param chaine 
     * @return int 
     */
    int addSommet(const string_view& nomSommet, const string_view& chaine);
    /**
     * @brief Ajoute un sommet sans enregistrer sa chaine
     * 
     * @param nomSommet 
     * @param tailleChaine 
     * @return int 
     */
    int addSommet(const string_view& nomSommet, long tailleChaine);
    /**
     * @brief Retourne vrai si un sommet de ce nom existe
     * 
     * @param name 
     * @return true 
     * @return false 
     */
    bool exists(const string& name);
    /**
     * @brief Retourne vrai si un sommet de cet identifiant existe
     * 
     * @param id 
     * @return true 
     * @return false 
     */
    bool exists(int id);
    /**
     * @brief retourne le nombre de sommets enregistré
     * 
     * @return int 
     */
    int size();
    /**
     * @brief Permet d'afficher les sommets et leurs informations
     * 
     * @return string 
     */
    string str();
};
#endif
