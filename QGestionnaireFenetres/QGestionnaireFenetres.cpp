#include "QGestionnaireFenetres.h"
#include "QPages/QPChooseFile/QPChooseFile.h"
#include "QPages/QPErrInf/QPErrInf.h"
#include "QPages/QPGeneralInfos/QPGeneralInfos.h"
#include "QPages/QPIndInf/QPIndInf.h"
#include "QPages/QPMenu/QPMenu.h"
#include "QPages/QPCreateSelection/QPCreateSelection.h"
#include "QPages/QPSegmInf/QPSegmInf.h"
#include "QPages/QPSelections/QPSelections.h"
#include "QPages/QPViewSelection/QPViewSelection.h"
#include "librairies/warning/warning.h"

IQPages *QGestionnaireFenetres::getNewPage(PageId pageId) {
    IQPages *page;
    switch (pageId) {
    case PageId::ChooseFile: {
        page = (IQPages *)new QPChooseFile();
        page->setWindowTitle("Choix du fichier");
        break;
    }
    case PageId::Menu: {
        page = (IQPages *)new QPMenu();
        page->setWindowTitle("Menu Principal");
        break;
    }
    case PageId::GeneralInfos: {
        page = (IQPages *)new QPGeneralInfos();
        page->setWindowTitle("Informations générales");
        break;
    }
    case PageId::SegmInf: {
        page = (IQPages *)new QPSegmInf();
        page->setWindowTitle("Sommets");
        break;
    }
    case PageId::ErrInf: {
        page = (IQPages *)new QPErrInf();
        page->setWindowTitle("Erreurs");
        break;
    }
    case PageId::IndInf: {
        page = (IQPages *)new QPIndInf();
        page->setWindowTitle("Individus");
        break;
    }
    case PageId::CreateSelection: {
        page = (IQPages *)new QPCreateSelection();
        page->setWindowTitle("Créer une séléction");
        break;
    }
    case PageId::Selections: {
        page = (IQPages *)new QPSelections();
        page->setWindowTitle("Séléctions de noeuds");
        break;
    }
    case PageId::ViewSelection: {
        page = (IQPages *)new QPViewSelection();
        page->setWindowTitle("Visionneur de sélections");
        break;
    }
    default: {
        page = 0;
        warning("Page non prise en charge");
    }
    }
    if (page) {
        page->setDataBase(this->db);
    }
    return page;
};

void QGestionnaireFenetres::connecter(IQPages *page) {
    connect(page, SIGNAL(goForward(int, PageId)), this,
            SLOT(openPageInWindow(int, PageId)));
    connect(page, SIGNAL(goBack(int)), this, SLOT(returnInWindow(int)));
    connect(page, SIGNAL(openNew(int, PageId)), this, SLOT(openNewWindow(int, PageId)));
    connect(page, SIGNAL(closeMe(int)), this, SLOT(closeWindow(int)));
    connect(page, SIGNAL(closeEverythingButMe(int)), this,
            SLOT(closeAllWindowsExcept(int)));
    connect(page, SIGNAL(reloadPred(int)), this, SLOT(reloadPred(int)));
}

bool QGestionnaireFenetres::exists(int window) {
    return !(this->app.find(window) == this->app.end());
}

void QGestionnaireFenetres::openPageInWindow(int window, PageId pageId) {
    if (exists(window)) {
        IQPages *page = this->getNewPage(pageId);
        if (!page) {
            return;
        }
        page->setDataBase(db);
        connecter(page);
        page->setWindow(window);
        app[window].back()->hide();
        app[window].push_back(page);
        page->goHere(MODE_RESTART);
        //balise(string("Changement de page"));
    } else {
        warning(string("Fenetre ") + to_string(window) +
                string(" innexistante"));
        return;
    }
};
void QGestionnaireFenetres::returnInWindow(int window) {
    if (exists(window)) {
        app[window].back()->hide();
        app[window].pop_back();
        if (app[window].size() > 0) {
            app[window].back()->goHere(MODE_CONTINUE);
            if(window ==0 && app[window].size() == 1){
                closeAllWindowsExcept(window);
            } 
        } else {
            IQPages *page = this->getNewPage(PAGE_PAR_DEFAUT);
            page->setDataBase(db);
            if (!page) {
                return;
            }
            connecter(page);
            page->setWindow(window);
            app[window].push_back(page);
            page->goHere(MODE_RESTART);
        }
        //balise(string("Retour en ariere"));
    } else {
        warning(string("Fenetre ") + to_string(window) +
                string(" innexistante"));
        return;
    }
};
int QGestionnaireFenetres::openNewWindow(int pred, PageId pageId) {
    IQPages *page = this->getNewPage(pageId);
    if (!page) {
        return nextWindow - 1;
    }
    this->pred[nextWindow]=pred;
    app[nextWindow] = vector<IQPages *>();
    page->setDataBase(db);
    connecter(page);
    page->setWindow(nextWindow);
    app[nextWindow].push_back(page);
    page->goHere(MODE_RESTART);
    //balise(string("Ouverture de page"));
    return nextWindow++;
};
void QGestionnaireFenetres::closeWindow(int window) {
    if (exists(window)) {
        while (app[window].size() != 0) {
            app[window].back()->close();
            app[window].pop_back();
        }
        pred.erase(window);
    } else {
        warning(string("Fenetre ") + to_string(window) +
                string(" innexistante"));
        return;
    }
};
void QGestionnaireFenetres::closeAllWindowsExcept(int window) {
    for (map<int, vector<IQPages *>>::iterator iter = app.begin();
         iter != app.end(); ++iter) {
        if (iter->first != window) {
            closeWindow(iter->first);
        }
    }
};
void QGestionnaireFenetres::reloadAll() {
    for (map<int, vector<IQPages *>>::iterator iter = app.begin();
         iter != app.end(); ++iter) {
        reloadWindow(iter->first);
    }
}
void QGestionnaireFenetres::reloadWindow(int window) {
    app[window].back()->goHere(MODE_CONTINUE);
};
void QGestionnaireFenetres::showWindow(int window) {
    app[window].back()->goHere(MODE_RESTART);
};

void QGestionnaireFenetres::reloadPred(int window){
    reloadWindow(pred[window]);
};
void QGestionnaireFenetres::closePred(int window){
    closeWindow(pred[window]);
};
int QGestionnaireFenetres::getPred(int window){
    return pred[window];
};