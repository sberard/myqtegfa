/**
 * @file QGestionnaireFenetres.h
 * @author Nael BB
 * @brief Classe principale qui s'occupe de gerer les différentes fenêtres et la
 * navigation dans l'application, en fonctionnement avec #IQPages
 *
 */
#ifndef GESTIONNAIRE_FENETRES_QT_
#define GESTIONNAIRE_FENETRES_QT_
#include "../QPages/IQPages/IQPages.h"
#include "../QPages/PageId.h"
#include <QWidget>

using namespace std;
#include <map>

// Page ouverte lors d'un retour en arière dans une fenêtre dont la pile est
// vide
#define PAGE_PAR_DEFAUT PageId::Menu

/**
 * @brief Classe de gestion des fenêtres de l'application
 *
 *
 * Fonctionnement :
 * Une application est découpée en plusieurs fenêtres. Chaque fenêtre est
 * associée automatiquement à un identifiant, une fenêtre précédente, et à un
 * vevcteur de pages qui agis comme une pile De là, il est possible d'ouvrir des
 * nouvelles fenếtres, d'ouvrir une nouvelle page dans une fenêtre, de revenir
 * en arière dans une fenêtre, ext. Ces actions correspondent au slots de la
 * classe.
 *
 * Les pages de QGestionnaireFenetres sont des instances de IQPages, l'ouverture
 * d'une page se fait en utilisant le constructeur de la classe d'une page.
 *
 * Ajouter une page :
 * <ol>
 * <li> Créer une classe modélisant le comportement de la page souhaitée,
 * héritant #IQPages<br> Exemple : Création de la page QPViderTexte qui permet
 * de vider récursivement tous les fichiers texte d'un répertoire choisi
 * </li>
 * <li> Ajouter une constante correspondant au nom de la page dans l'énumération
 * #PageId<br> Exemple : Ajout de "ViderTexte" dans l'énumération PageId
 * </li>
 * <li> Ajouter un cas en plus dans le switch de la methode
 * QGestionnaireFenetres::getNewPage (fichier QGestionnaireFenetres).<br>
 * Exemple : Ajouter dans QGestionnaireFenetres.cpp
 * @code
 * case PageId::ViderTexte: {
 *       page = (IQPages *)new QPViderTexte();
 *       page->setWindowTitle("Vider les textes dans un répertoire");
 *       break;
 *   }
 * @endcode
 * </li>
 * <li> Connecter cette page au reste de votre application<br>
 * Exemple : Ajouter dans une autre page le code
 * @code
 * connect(boutonAllerViderTexte, &QPushButton::clicked, [=](){emit
 * goForward(myWindow, PageId::ViderTexte);})
 * @endcode
 * </li>
 * </ol>
 */
class QGestionnaireFenetres : public QWidget {
    Q_OBJECT
    DataBase *db = new DataBase();
    int nextWindow = 0;
    map<int, vector<IQPages *>> app{};
    map<int, int> pred{};
    IQPages *getNewPage(PageId pageId);
    void connecter(IQPages *);

  public:
    /**
     * @brief retourne vrai si une fenêtre existe dans l'application
     *
     * @param window Identifiant de la fenêtre
     * @return true
     * @return false
     */
    bool exists(int window);
    /**
     * @brief Renvoie l'identifiant du prédécesseur de window
     *
     * @param window Identifiant de la fenêtre
     * @return int
     */
    int getPred(int window);
  public slots:
    /**
     * @brief Ouvre une page dans une fenêtre déjà existante de type donnée en
     * paramètre
     *
     * Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Page actuelle de la fenêtre window<td>appel à hide()
     * <tr><td>Nouvelle page de type pageId<td>appel à goHere(MODE_RESTART);
     * <tr><td>Fenêtre window<td>La page change pour le type pageId. L'ancienne
     * page est stockée dans la pile <tr><td>Autres fenêtres / pages<td>Aucun
     * changement
     * </table>
     *
     * @param window Identifiant de la fenêtre
     * @param pageId Type de page souhaitée
     */
    void openPageInWindow(int window, PageId pageId);
    /**
     * @brief Retour en arière dans la fenêtre donnée (en supprimant l'actuelle
     * page affichée)
     *
     * Comportement sur les pages/fenêtres si la pile de window permet de
     * revenir en arière : <table> <tr><th>Page<th>Comportement <tr><td>Page
     * actuelle de la fenêtre window<td>détruite <tr><td>Page sous la page
     * actuelle dans la pile de la fenêtre window<td>appel à
     * goHere(MODE_CONTINUE); <tr><td>Fenêtre window<td>La page change et
     * redeviens celle qu'elle était avant. La page actuelle est détruite
     * <tr><td>Autres fenêtres / pages<td>Aucun changement
     * </table>
     *
     * Comportement sur les pages/fenêtres si la pile de window ne permet pas de
     * revenir en arière : <table> <tr><th>Page<th>Comportement <tr><td>Page
     * actuelle de la fenêtre window<td>détruite <tr><td>Nouvelle fenêtre de
     * type PAGE_PAR_DEFAUT<td>appel à goHere(MODE_RESTART);
     * <tr><td>Fenêtre window<td>La pile contient une fenêtre du type
     * PAGE_PAR_DEFAUT. La page actuelle est détruite <tr><td>Autres fenêtres /
     * pages<td>Aucun changement
     * </table>
     * PAGE_PAR_DEFAUT est une constante définie dans QGestionnaireFenetres.h
     * @param window Identifiant de la fenêtre
     */
    void returnInWindow(int window);
    /**
     * @brief Ouvre une nouvelle fenêtre dans l'application
     *
     *Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Page/fenetre identifiée par pred<td>Aucun changement
     * <tr><td>Nouvelle page<td>appel à goHere(MODE_RESTART);
     * <tr><td>Nouvelle page<td>à appel à goHere(MODE_RESTART);
     * <tr><td>Fenêtre window<td>La page change et redeviens celle qu'elle était
     *avant. La page actuelle est détruite <tr><td>Autres fenêtres /
     *pages<td>Aucun changement
     * </table>
     *
     * @param pred Identifiant de la fenêtre
     * @param pageId Type de la page à ouvrir dans la nouvelle fenêtre
     * @return int L'identifiant de la nouvelle fenêtre
     */
    int openNewWindow(int pred, PageId pageId);
    /**
     * @brief Ferme completement la fenêtre window
     *
     * Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Page actuelle de window<td>Détruite
     * <tr><td>Pages sur la pile de la fenêtre window<td>détruites
     * <tr><td>Fenêtre window<td>détruite
     * <tr><td>Autres fenêtres / pages<td>Aucun changement
     * </table>
     * @param window
     */
    void closeWindow(int window);
    /**
     * @brief Ferme completement toutes les fenêtres qui ne sont pas
     *
     * Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Fenêtre window<td>Aucun changement
     * <tr><td>Autres pages actuelles<td>Détruites
     * <tr><td>Pages dans les piles des autres fenêtres<td>Détruites
     * <tr><td>Autres fenêtres<td>Détruites
     * </table>
     * @param window Identifiant de la fenêtre
     */
    void closeAllWindowsExcept(int window);
    /**
     * @brief Recharge toutes les fenêtres
     *
     * Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Page actuelles de toutes les fenêtres<td>appel à
     * goHere(MODE_CONTINUE); <tr><td>Pages dans les piles des fenêtres<td>Aucun
     * changement <tr><td>Fenêtres<td>Aucun changement
     * </table>
     *
     */
    void reloadAll();
    /**
     * @brief Recharge la fenetre prédécésseur de window
     * Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Page actuelle du prédécésseur de window<td>appel à
     * goHere(MODE_CONTINUE); <tr><td>Page actuelle de window<td>Aucun
     * changement <tr><td>Autres fenêtres / pages<td>Aucun changement
     * </table>
     * @param window
     */
    void reloadPred(int window);
    /**
     * @brief Ferme le prédécesseur de la fenêtre window
     *
     * Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Page actuelle du prédécésseur de window<td>Détruite
     * <tr><td>Pages dans la pile du prédécésseur de window<td>Détruite
     * <tr><td>Fenêtre prédécesseur de window<td>Détruite
     * <tr><td>Page actuelle de window<td>Aucun changement
     * <tr><td>Autres fenêtres / pages<td>Aucun changement
     * </table>
     * @param window Identifiant de la fenêtre
     */
    void closePred(int window);
    /**
     * @brief Recharge la fenêtre donnée en paramètre
     *
     * Comportement sur les pages/fenêtres :
     * <table>
     * <tr><th>Page<th>Comportement
     * <tr><td>Page actuelle de window<td>appel à goHere(MODE_CONTINUE);
     * <tr><td>Pages dans la pile de window<td>Aucun changement
     * <tr><td>Autres fenêtres / pages<td>Aucun changement
     * </table>
     *
     * @param window Identifiant de la fenêtre
     */
    void reloadWindow(int window);
    /**
     * @brief Affiche la fenêtre sélécionnée
     * @param window Identifiant de la fenêtre
     */
    void showWindow(int window);
};
#endif