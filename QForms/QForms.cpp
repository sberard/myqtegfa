#include "QForms.h"
#include "QFileSelect/QFileSelect.h"

bool QForms::exists(const QString &nomWidget) {
    return !(this->form.find(nomWidget) == this->form.end());
}

QWidget *QForms::getWidget(const QString &nomWidget) {
    if (!exists(nomWidget)) {
        qDebug().nospace() << "Warning : Le widget " << nomWidget
                           << " n'existe pas dans ce formulaire";
    } else {
        return this->form[nomWidget].first;
    }
    return 0;
}

void QForms::addLabel(const QString &nomWidget, const QString &contents,
                      QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QLabel *toAdd = new QLabel(contents, parent);
        showable->addWidget(toAdd);
        this->form[nomWidget] = pair((QWidget *)toAdd, OptionWidget::Label);
    }
};
void QForms::addConfirmButton(const QString &nomWidget,
                              const QString &validationText, QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QPushButton *toAdd = new QPushButton(validationText, parent);
        showable->addWidget(toAdd);
        connect(toAdd, SIGNAL(clicked()), this, SLOT(disable()));
        connect(toAdd, SIGNAL(clicked()), this, SIGNAL(endForm()));
        this->form[nomWidget] = pair((QWidget *)toAdd, OptionWidget::Confirm);
    }
};
void QForms::addFileSelect(const QString &nomWidget,
                           const QString &restrictions,
                           const QString &texteBouton,
                           const QString &texteSiVide, QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QFileSelect *toAdd =
            new QFileSelect(restrictions, texteBouton, texteSiVide, parent);
        showable->addWidget(toAdd);
        this->form[nomWidget] =
            pair((QWidget *)toAdd, OptionWidget::FileSelect);
    }
};
void QForms::addLineEdit(const QString &nomWidget, const QString &placeHolder,
                         int maxLength, const QString &contents,
                         QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QLineEdit *toAdd = new QLineEdit(contents, parent);
        toAdd->setPlaceholderText(placeHolder);
        if (maxLength > 0) {
            toAdd->setMaxLength(maxLength);
        }
        showable->addWidget(toAdd);
        this->form[nomWidget] = pair((QWidget *)toAdd, OptionWidget::LineEdit);
    }
};
void QForms::addCheckBox(const QString &nomWidget, const QString &contents,
                         bool isTristate, QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QCheckBox *toAdd = new QCheckBox(contents, parent);
        toAdd->setTristate(isTristate);
        showable->addWidget(toAdd);
        this->form[nomWidget] = pair((QWidget *)toAdd, OptionWidget::CheckBox);
    }
};
void QForms::addSlider(const QString &nomWidget, int min, int max, int pas,
                       Qt::Orientation orientation, QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QSlider *toAdd = new QSlider(orientation, parent);
        if (min < max) {
            toAdd->setRange(min, max);
        } else {
            qDebug().nospace() << "Warning : Le minimum est plus grand que le "
                                  "maximum sur le Slider "
                               << nomWidget;
        }
        if (pas > 0) {
            toAdd->setPageStep(pas);
            toAdd->setSingleStep(pas);
        } else {
            qDebug().nospace() << "Warning : Le pas donné au Slider "
                               << nomWidget << " est négatif";
        }
        showable->addWidget(toAdd);
        this->form[nomWidget] = pair((QWidget *)toAdd, OptionWidget::Slider);
    }
};
void QForms::addSpinBox(const QString &nomWidget, const QString &prefix,
                        const QString &suffix, int min, int max, int pas,
                        QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QSpinBox *toAdd = new QSpinBox(parent);
        if (min > max) {
            toAdd->setRange(min, max);
        } else if (min < max) {
            qDebug().nospace() << "Warning : Le minimum est plus grand que le "
                                  "maximum sur la SpinBox "
                               << nomWidget;
        }
        if (pas > 0) {
            toAdd->setSingleStep(pas);
        } else {
            qDebug().nospace() << "Warning : Le pas donné au Slider "
                               << nomWidget << " est négatif";
        }
        toAdd->setPrefix(prefix);
        toAdd->setSuffix(suffix);
        showable->addWidget(toAdd);
        this->form[nomWidget] = pair((QWidget *)toAdd, OptionWidget::SpinBox);
    }
};
void QForms::addComboBox(const QString &nomWidget, QWidget *parent) {
    if (exists(nomWidget)) {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " déjà existant dans ce formulaire";
    } else {
        QComboBox *toAdd = new QComboBox(parent);
        showable->addWidget(toAdd);
        this->form[nomWidget] = pair((QWidget *)toAdd, OptionWidget::ComboBox);
    }
};
void QForms::addBoxInComboBox(const QString &nomWidget, const QString &value) {
    if (exists(nomWidget)) {
        pair<QWidget *, OptionWidget> p = this->form[nomWidget];
        if (p.second == OptionWidget::ComboBox) {
            ((QComboBox *)p.first)->addItem(value);
        } else {
            qDebug().nospace() << "Warning : Le widget " << nomWidget
                               << " n'est pas une comboBox";
        }
    } else {
        qDebug().nospace() << "Warning : Widget " << nomWidget
                           << " n'existe pas dans ce formulaire";
    }
};
int QForms::getIntForm(const QString &nomWidget) {
    if (exists(nomWidget)) {
        pair<QWidget *, OptionWidget> p = this->form[nomWidget];
        switch (p.second) {
        case OptionWidget::CheckBox:
            switch (((QCheckBox *)p.first)->checkState()) {
            case Qt::Unchecked:
                return 0;
            case Qt::PartiallyChecked:
                return 1;
            case Qt::Checked:
                return ((int)((QCheckBox *)p.first)->isTristate()) + 1;
            }
            break;
        case OptionWidget::Slider:
            return ((QSlider *)p.first)->value();
            break;
        case OptionWidget::SpinBox:
            return ((QSpinBox *)p.first)->value();
            break;
        case OptionWidget::ComboBox:
            return ((QComboBox *)p.first)->currentIndex();
            break;
        default:
            qDebug().nospace() << "Warning : Le widget " << nomWidget
                               << " ne possede pas de retour entier !";
        }
    } else {
        qDebug().nospace() << "Warning : Le widget " << nomWidget
                           << " n'existe pas dans ce formulaire";
    }
    return 0;
};
QString QForms::getStringForm(const QString &nomWidget) {
    if (exists(nomWidget)) {
        pair<QWidget *, OptionWidget> p = this->form[nomWidget];
        switch (p.second) {
        case OptionWidget::FileSelect: {
            QString str = ((QFileSelect *)p.first)->getFileSelect();
            return str;
            break;
        }
        case OptionWidget::LineEdit:
            return ((QLineEdit *)p.first)->text();
            break;
        case OptionWidget::ComboBox:
            return ((QComboBox *)p.first)->currentText();
            break;
        default:
            qDebug().nospace() << "Warning : Le widget " << nomWidget
                               << " ne possede pas de retour string !";
        }
    } else {
        qDebug().nospace() << "Warning : Le widget " << nomWidget
                           << " n'existe pas dans ce formulaire";
    }
    return "";
};
void QForms::enable() { setEnabled(true); };
void QForms::disable() { setEnabled(false); };
void QForms::setEnabled(bool val) {
    for (map<QString, pair<QWidget *, OptionWidget>>::iterator it =
             this->form.begin();
         it != this->form.end(); it++) {
        it->second.first->setEnabled(val);
    }
};