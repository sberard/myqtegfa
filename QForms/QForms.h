/**
 * @file QForms.h
 * @author Nael BB
 * @brief Widget permetant de créer des formulaires personalisables avec Qt
 */
#ifndef FORMS_QT_
#define FORMS_QT_

#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QProgressBar>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QString>
#include <QWidget>
#include <QtDebug>

#include <QGridLayout>
#include <map>
#include <utility>
using namespace std;
/**
 * @brief Enumération comprenant les différents types de widgets qui peuvent
 * être ajoutés à une instance de #QForms
 *
 */
enum class OptionWidget {
    Label,
    Confirm,
    FileSelect,
    LineEdit,
    CheckBox,
    Slider,
    SpinBox,
    ComboBox
};
/**
 * @brief Widget permetant de créer des formulaires personalisables avec Qt
 * 
 */
class QForms : public QWidget {
    Q_OBJECT
    QVBoxLayout *showable = new QVBoxLayout(this);
    map<QString, pair<QWidget *, OptionWidget>> form =  map<QString, pair<QWidget *, OptionWidget>>();

  public:
    /**
     * @brief Vérifie si un identifiant de widget existe dans le formulaire
     *
     * @param nomWidget Identifiant
     * @return true
     * @return false
     */
    bool exists(const QString &nomWidget);
    /**
     * @brief Ajoute un QLabel au formulaire
     *
     * @param nomWidget Identifiant
     * @param contents Texte du label
     * @param parent
     */
    void addLabel(const QString &nomWidget,
                  const QString &contents = "Choisir ici : ",
                  QWidget *parent = 0);
    /**
     * @brief Ajoute un QPushButton de confirmation au formulaire (point de
     * sortie)
     *
     * @param nomWidget Identifiant
     * @param validationText Texte du bouton
     * @param parent
     */
    void addConfirmButton(const QString &nomWidget,
                          const QString &validationText = " Confirmer ",
                          QWidget *parent = 0);
    /**
     * @brief Ajoute un #QFileSelect au formulaire
     *
     * @param nomWidget Identifiant
     * @param restrictions Réstrictions appliquées au sélecteur : quels
     * fichiers peuvent être séléctionnés
     * @param texteBouton Texte présent sur le bouton qui active la demande de
     * choix d'un fichier
     * @param texteSiVide Texte affiché quand aucun fichier n'est choisi
     * @param parent
     */
    void addFileSelect(
        const QString &nomWidget, const QString &restrictions = "*.txt",
        const QString &texteBouton = " Select a file ",
        const QString &texteSiVide = " No file selected ",
        QWidget *parent = 0);
    /**
     * @brief Ajoute un QLineEdit au formulaire
     * 
     * @param nomWidget Identifiant
     * @param placeHolder PlaceHolder de la ligne
     * @param maxLength Taille maximum de la ligne
     * @param contents Contenu d'origine de la ligne
     * @param parent 
     */
    void addLineEdit(const QString &nomWidget,
                     const QString &placeHolder = "Ecrivez ici",
                     int maxLength = 0, const QString &contents = "",
                     QWidget *parent = 0);
    /**
     * @brief Ajoute une QCheckBox au formulaire
     * 
     * @param nomWidget Identifiant
     * @param contents Label à coté du bouton
     * @param isTristate True pour que le bouton ait un état intermédiaire
     * @param parent 
     */
    void addCheckBox(const QString &nomWidget,
                     const QString &contents = "Option",
                     bool isTristate = false, QWidget *parent = 0);
    /**
     * @brief Ajoute un QSlider au formulaire
     * 
     * @param nomWidget Identifiant
     * @param min Minimum du slider
     * @param max Maximum du slider
     * @param pas Pas du slider (avec les flèches, le pas n'est pas en hard sur le slider)
     * @param orientation Orientation du slider
     * @param parent 
     */
    void addSlider(const QString &nomWidget, int min = 0, int max = 100,
                   int pas = 1, Qt::Orientation orientation = Qt::Horizontal,
                   QWidget *parent = 0);
    /**
     * @brief Ajoute un QSpinBox au formulaire
     * 
     * @param nomWidget Identifiant
     * @param prefix Préfixe de la spinbox (ex : "Niveau ")
     * @param suffix Suffixe de la spinbox (ex : "€")
     * @param min Valeur minimum
     * @param max Valeur maximum (si égale à valeur minimum, la valeur par défaut est laissée)
     * @param pas Pas de la spinbox
     * @param parent 
     */
    void addSpinBox(const QString &nomWidget, const QString &prefix = "",
                    const QString &suffix = "", int min = 0, int max = 0,
                    int pas = 1, QWidget *parent = 0);
    /**
     * @brief Ajoute une QComboBox vide au formulaire
     * 
     * @param nomWidget Identifiant
     * @param parent 
     */
    void addComboBox(const QString &nomWidget, QWidget *parent = 0);
    /**
     * @brief Ajoute un item à une QComboBox 
     * 
     * @param nomWidget Identifiant de la comboBox
     * @param value Item à ajouter
     */
    void addBoxInComboBox(const QString &nomWidget,
                          const QString &value = " Option ");
    /**
     * @brief Renvoie le QWidget avec l'identifiant demandé en parametre.
     * 
     * Attention aux erreurs de segmentation : si vous essayez de cast le widget renvoyé, faites le dans le bon type
     * 0 est renvoyé si l'identifiant n'existe pas
     * 
     * @param nomWidget Identifiant
     * @return QWidget* 
     */
    QWidget *getWidget(const QString &nomWidget);
    /**
     * @brief Renvoie la valeur entière contenue dans le widget 
     *
     * <table>
     * <tr><th>Type du widget<th>Valeur renvoyée
     * <tr><td>QCheckBox (not tristate)<td> 0 (unchecked) ou 1 (checked)
     * <tr><td>QCheckBox (tristate)<td> 0 (unchecked) ou 1 (partially checked) ou 2 (checked)
     * <tr><td>QSlider<td>Valeur séléctionnée
     * <tr><td>QSpinBox<td>Valeur séléctionnée
     * <tr><td>QComboBox<td>Index du choix effectué
     * <tr><td>Autre<td>0 (Erreur signalée)
     * <tr><td>Identifiant inconnu<td>0 (Erreur signalée)
     * </table>
     * @param nomWidget Identifiant
     * @return int 
     */
    int getIntForm(const QString &nomWidget);
    /**
     * @brief Renvoie la chaine de caractères contenue dans le widget 
     *
     * <table>
     * <tr><th>Type du widget<th>Valeur renvoyée
     * <tr><td>QFileSelect<td>Nom du fichier séléctionné
     * <tr><td>QLineEdit<td>Texte écrit
     * <tr><td>QComboBox<td>Texte du choix éffectué
     * <tr><td>Autre<td>"" (Erreur signalée)
     * <tr><td>Identifiant inconnu<td>"" (Erreur signalée)
     * </table>
     * @param nomWidget Identifiant
     * @return QString 
     */
    QString getStringForm(const QString &nomWidget);
    /**
     * @brief Permet d'activer ou désactiver le formulaire
     *
     * Utilisé par #QForms::enable et #QForms::disable
     * 
     * @param val 
     */
    void setEnabled(bool val);

  public slots:
  /**
   * @brief Rends le formulaire modifiable
   * 
   */
    void enable();
    /**
     * @brief Empêche la modification du formulaire. 
     * 
     * Note : automatiquement appelé quand un bouton de confirmation est cliqué. La ligne <br>
     * 
     */
    void disable();

  signals:
  /**
   * @brief Signal envoyé quand un bouton de confirmation est cliqué
   * 
   */
    void endForm();
};
#endif