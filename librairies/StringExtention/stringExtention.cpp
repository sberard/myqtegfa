#include "stringExtention.h"
#include "librairies/warning/warning.h"
#include <string>
#include <string_view>


int getLenTillNextChevron(const string_view & str, int pos) {
    int i = pos;
    while (i < (int)str.size() && str[i] != '<' && str[i] != '>') {
        i++;
    }
    return i - pos;
}


const string reverseComplement(const string in) {
    string ret{};
    for (char c : in) {
        switch (c) {
        case 'A':
            ret+= 'T';
            break;
        case 'C':
            ret+= 'G';
            break;
        case 'G':
            ret+= 'C';
            break;
        case 'T':
            ret+= 'A';
            break;
        case 'a':
            ret+= 't';
            break;
        case 'c':
            ret+= 'g';
            break;
        case 'g':
            ret+= 'c';
            break;
        case 't':
            ret+= 'a';
            break;
        case 'N':
            ret+= 'N';
            break;
        case 'n':
            ret+= 'n';
            break;
        default:
            ret += c;
            warning("Erreur : caractère \'" + to_string(c)+ "\' présent dans un code génétique !");
            break;
        }
    }
    return ret;
};


vector<string_view> split(const string &s, const char delimiter){
    vector<string_view> ret{};
    const auto start = s.begin();
    auto lastEnd = start;
    for(auto pos = start; pos != s.end(); pos++){
        if(*pos==delimiter){
            ret.push_back(string_view(s.data() + (lastEnd - start), pos - lastEnd));
            lastEnd = pos+1;
        }   
    }
    ret.push_back(string_view(s.data() + (lastEnd - start), s.end() - lastEnd));
    return ret;
}

vector<string_view> split(const string_view &s, const char delimiter){
    vector<string_view> ret{};
    const auto start = s.begin();
    auto lastEnd = start;
    for(auto pos = start; pos != s.end(); pos++){
        if(*pos==delimiter){
            ret.push_back(string_view(s.data() + (lastEnd - start), pos - lastEnd));
            lastEnd = pos+1;
        }   
    }
    ret.push_back(string_view(s.data() + (lastEnd - start), s.end() - lastEnd));
    return ret;
}

QString formatedFloatToQString(float f) {
    QString number;
    int i;
    if (f - int(f) != 0) {
        number = QString::number(f, 'f', 2);
        i = number.lastIndexOf('.');
    } else {
        number = QString::number((int)f);
        i = number.size();
    }
    number.truncate(i + 3);
    if (i > 0) {
        i -= 3;
        while (i > 0) {
            number.insert(i, ',');
            i -= 3;
        }
    }
    return number;
}