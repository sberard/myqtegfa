/**
 * \file stringExtention.h
 * \brief Fonctions utilitaires touchant aux chaines de caractères
 * \author Nael BB
 */

#ifndef STRING_EXTENTION_
#define STRING_EXTENTION_

#include <QString>
#include <string>
#include <string_view>
#include <vector>
using namespace std;

/**
 * @brief Renvoie le reverseComplement de la chaine entrée en paramètres
 *
 * La chaine est parcourue à l'envers, chaque A deviens T ; chaque C deviens G
 * Les N restent tel quel
 * Les autres caractères ne sont pas suprimés, mais font afficher une erreur
 * Insensible à la case
 * 
 * @param monstring 
 * @return string* 
 */
const string reverseComplement(const string monstring);

/**
 * @brief Renvoie la longueur du text en partant de str[pos] jusqu'au prochain
 * chevron (< ou >)
 *
 * @param str chaine à regarder
 * @param pos position d'où commencer la recherche
 * @return int
 */
int getLenTillNextChevron(const string_view &str, int pos = 0);

/**
 * @brief Découpe une chaine de caractères (string) selon un délimiteur, sans la copier
 * 
 * @param s chaine en entrée
 * @param delimiter délimiteur de la chaine
 * @return vector<string_view> 
 */
vector<string_view> split(const string &s, const char delimiter);
/**
 * @brief Découpe une chaine de caractères (string_view) selon un délimiteur, sans la copier
 * 
 * @param s chaine en entrée
 * @param delimiter délimiteur de la chaine
 * @return vector<string_view> 
 */
vector<string_view> split(const string_view &s, const char delimiter);

/**
 * @brief Transforme un flotant en QString, formaté correctement
 *
 * Exemple : 230892.2344 devient 230,892.23
 * @param f le nombre à formater
 * @return QString 
 */
QString formatedFloatToQString(float i);

#endif