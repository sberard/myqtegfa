/**
 * \file warning.h
 * \brief Fonctions d'affichage standardisé des erreurs et du débug
 * \author Nael BB
 */
#ifndef WARNING_LIB
#define WARNING_LIB
#include <QString>
using namespace std;
/**
 * @brief Affiche un warning associé à une ligne dans cerr
 * 
 * @param noLigne 
 * @param warning 
 */
void warningLine(int noLigne, const string &warning);
/**
 * @brief Affiche un warning général dans cerr
 * 
 * @param warning 
 */
void warning(const string &warning);
/**
 * @brief Affiche une information dans cout
 * 
 * @param warning 
 */
void information(const string &warning);
/**
 * @brief Affiche une balise dans cout (à partir d'un entier)
 * 
 * @param val 
 */
void balise(int val);
/**
 * @brief Affiche une balise dans cout (à partir d'une string)
 * 
 * @param val 
 */
void balise(const string &val);
/**
 * @brief Affiche une balise dans cout (à partir d'une QString)
 * 
 * @param val 
 */
void balise(const QString& val);
#endif