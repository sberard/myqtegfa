#include "warning.h"
#include "librairies/StringExtention/stringExtention.h"
#include <iostream>
void warningLine(int noLigne, const string &warning) {
    cerr << "Erreur ligne " << "\x1B[33m" << noLigne << "\x1B[0m : " << warning
         << "." << endl;
}
void warning(const string &warning) {
    cerr << "\x1B[33mErreur\x1B[0m : " << warning << "." << endl;
}
void information(const string &warning) {
    cout << "\x1B[32mInformation\x1B[0m : " << warning << "." << endl;
}
void balise(int val) {
    cout << "\x1B[34mBalise << \x1B[0m" << formatedFloatToQString(val).toStdString() << "\x1B[34m >>\x1B[0m" << endl;
}
void balise(const string &val) {
    cout << "\x1B[34mBalise\x1B[0m : " << val << " !" << endl;
}
void balise(const QString& val) {
    cout << "\x1B[34mBalise\x1B[0m : " << val.toStdString() << " !" << endl;
}