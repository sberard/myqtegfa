/**
 * \file statsExtention.h
 * \brief Fonction getStats, servant à récuperer des statistiques sur un échantillon
 * \author Nael BB
 */
#ifndef STATS_EXTENTION_
#define STATS_EXTENTION_
#include <map>
#include <string>
#include <tgmath.h>
using namespace std;

/**
 * @brief Récupère des statistiques sur la répartition donnée en entrée
 * 
 * Dictionnaire retourné: 
 * <table><tr><th>Clé<th>Valeur
 * <tr><td>"sampleSize"<td> effectif total
 * <tr><td>"avg"<td> moyenne
 * <tr><td>"median"<td> médiane
 * <tr><td>"min"<td> valeur minimum
 * <tr><td>"max"<td> valeur maximum
 * <tr><td>"25%"<td> premier quartile
 * <tr><td>"75%"<td> troisième quartile
 * <tr><td>"stdDev"<td> écart type
 * </table>
 *
 * @param donnees de la forme {valeur : effectif}
 * @return map<string, float>
 */
const map<string, float> getStats(const map<long, long> &donnees);

#endif