#include "statsExtention.h"
const map<string, float> getStats(const map<long, long> &donnees) {
    map<string, float> ret{};
    if (donnees.size() == 0) {
        ret["sampleSize"] = 0;
        ret["avg"] = 0;
        ret["median"] = 0;
        ret["min"] = 0;
        ret["max"] = 0;
        ret["25%"] = 0;
        ret["75%"] = 0;
        ret["stdDev"] = 0;
        return ret;
    }
    float sum = 0;
    float size = 0;
    bool mF = false;
    for (pair<long, float> val : donnees) {
        if (!mF) {
            ret["min"] = val.first;
            mF = true;
        }
        sum += val.first * val.second;
        size += val.second;
    }
    ret["sampleSize"] = (float)size;
    ret["avg"] = (sum / ((float)(size)));
    float pos = 0;
    float sum2 = 0;
    int p = 0;
    int last = 0;
    for (pair<long, float> val : donnees) {
        switch (p) {
        case 0: {
            if (pos - 0.5 == (size - 1) / ((float)4)) {
                p++;
                ret["25%"] = (last + ((float)val.first)) / 2;
            } else if (pos > (size - 1) / ((float)4)) {
                p++;
                ret["25%"] = last;
            }
            [[fallthrough]];
        }
        case 1: {
            if (pos - 0.5 == (size - 1) / ((float)2)) {
                p++;
                ret["median"] = (last + ((float)val.first)) / 2;
            } else if (pos > (size - 1) / ((float)2)) {
                p++;
                ret["median"] = last;
            }
            [[fallthrough]];
        }
        case 2: {
            if (pos - 0.5 == 3 * (size - 1) / ((float)4)) {
                p++;
                ret["75%"] = (last + ((float)val.first)) / 2;
            } else if (pos > 3 * (size - 1) / ((float)4)) {
                p++;
                ret["75%"] = last;
            }
        }
        }
        sum2 += val.first * val.first * val.second;
        pos += val.second;
        last = val.first;
    }
    ret["max"] = last;
    switch (p) {
    case 0: {
        ret["25%"] = last;
        [[fallthrough]];
    }
    case 1: {
        ret["median"] = last;
        [[fallthrough]];
    }
    case 2: {
        ret["75%"] = last;
    }
    }
    ret["stdDev"] =
        sqrt((sum2 / ((float)size)) - pow((sum / ((float)(size))), 2));
    return ret;
};