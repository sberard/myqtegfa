#include <QApplication>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "class/DataBase/DataBase.cpp"
#include "QGestionnaireFenetres/QGestionnaireFenetres.h"
using namespace std; 

int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    QGestionnaireFenetres *monApp = new QGestionnaireFenetres();
    monApp->resize(120000,12000);
    monApp->openNewWindow(0,PageId::ChooseFile);

    return app.exec();
}
