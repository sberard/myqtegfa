clear;
export QT_SELECT=qt5;
unset GTK_PATH;
qmake -project &&
echo "QT += widgets" >> $(basename $(pwd)).pro &&
echo "QT_QPA_PLATFORM=wayland" >> $(basename $(pwd)).pro &&
qmake &&
make -j$(nproc) &&(
echo "---START---";
./$(basename $(pwd)))