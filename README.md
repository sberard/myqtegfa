# MyQteGfa - Documentation
## Table des matières
## I. Introduction
Cette patie introductive explique l'utilisation générale de MyQteGfa
### 1. Objectif
L'objectif principal de cette application est de parcourir un fichier gfa (représentant un graphe de pangénome)
donné en entrée, et d'en extraire des informations. Ces informations sont diverses et variées, allant de la 
distribution des degrés des sommets du graphe (III.3.b) à la géneration de sous-graphes de pangénomes (V.5).
L'utilisation se décompose en deux phases principales, la première servant à parser le fichier pour stocker les
données dans des structures iunternes au logiciel, la deuxième à en extraire les informations souhaitées.
### 2. Démarrer le logiciel
### 3. Utilisations
## II. Ouvrir un fichier gfa
### 1. Choisir un fichier
### 2. Paramètres
### 3. Règles sur le fichier
## III. Récuperer des informations
### 1. Lignes du fichier
### 2. Erreurs trouvées
### 3. Sommets
#### a. Informations quantitatives
#### b. Distributions
#### c. Comparaisons
## IV. Génération de fasta
### 1. Conditions
### 2. Génerer le fichier
## V. Génération de sous-graphes (dot)
### 1. Sélections
### 2. Créer des sélections
### 3. Importer une sélection
### 4. Manipuler des sélections
### 5. Générer un fichier
#### a. Paramètres
#### b. Observer les fichiers
## VI. TODO List - Améliorations envisageables