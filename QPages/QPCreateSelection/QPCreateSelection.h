/**
 * @file QPCreateSelection.h
 * @author Nael BB
 * @brief QPage permetant de creer une sélection de sommets
 */
#ifndef CRE_SELECTION_APP_QT_
#define CRE_SELECTION_APP_QT_
#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include "class/LinkDataBase/LinkDataBase.h"
#include <QGridLayout>

#define COND_EQUAL 0
#define COND_LOWER_EQUAL 1
#define COND_GREATER_EQUAL 2
#define COND_LOWER 3
#define COND_GREATER 4
#define COND_DIFFERENT 5
#define COND_DIVISIBLE 6
#define COND_NOT_DIVISIBLE 7

#define CARAC_LENGTH 0
#define CARAC_PASS 1
#define CARAC_DEGIN 2
#define CARAC_DEGOUT 3
#define CARAC_DEGTOT 4
/**
 * @brief QPage permetant de creer une sélection de sommets
 *
 */
class QPCreateSelection : public IQPages {
    Q_OBJECT
    ListeDeSommetsOrientes choisis{};

  public:
    /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPCreateSelection(QWidget *parent = 0);
    /**
     * @brief Prépare le QComboBox des individus
     *
     */
    void loadPos();
    /**
     * @brief Ajoute un segment à la sélection si ce n'est ni une erreur, ni un
     * pointeur, ni un initialisateur (#ListeDeSommetsOrientesAvecPosition), et
     * qu'il n'est pas déjà ajouté
     *
     * @param som
     */
    void addOneSegment(const SommetOriente &som);
  public slots:
    void goHere(int mode = MODE_RESTART);
    /**
     * @brief Appelé quand l'individu change, pour recharger les haplotyopes
     * 
     */
    void changeInd();
    /**
     * @brief Appelé quand l'haplotyope change, pour recharger les contigs
     * 
     */
    void changeHap();
    /**
     * @brief Appelé quand le contig change, pour recharger les morceaux présents
     * 
     */
    void changeCont();
    /**
     * @brief Doit être appelé à chaque fois qu'un sommet est ajouté à choisis, pour modifier dynamiquement le nombre de sommets choisis affiché
     * 
     */
    void changeNbChoisis();
    /**
     * @brief Methode d'ajout de sommet à une sélection : ajoute un contig entier
     * 
     */
    void addPosContig();
    /**
     * @brief Methode d'ajout de sommet à une sélection : ajoute une position dans un contig
     * 
     */
    void addPos();
    /**
     * @brief Methode d'ajout de sommet à une sélection : ajoute un sommet selon son 
     * 
     */
    void addName();
    /**
     * @brief Methode d'ajout de sommet à une sélection : les sommets dont la caracteristique donnée en paramètres est minimale
     * 
     *<table>
     *<tr><th>Caracteristique<th>Valeur passée
     *<tr><td>Taille des sommets<td>0
     *<tr><td>Nombre de passages<td>1
     *<tr><td>Degrés entrant<td>2
     *<tr><td>Degrés sortant<td>3
     *<tr><td>Degrés total<td>4
     *</table>
     *
     * @param caracteristique 
     */
    void addPropMin(int caracteristique);
    /**
     * @brief Methode d'ajout de sommet à une sélection : les sommets dont la caracteristique donnée en paramètres est maximale
     * 
     *<table>
     *<tr><th>Caracteristique<th>Valeur passée
     *<tr><td>Taille des sommets<td>0
     *<tr><td>Nombre de passages<td>1
     *<tr><td>Degrés entrant<td>2
     *<tr><td>Degrés sortant<td>3
     *<tr><td>Degrés total<td>4
     *</table>
     *
     * @param caracteristique 
     */
    void addPropMax(int caracteristique);
    /**
     * @brief Methode d'ajout de sommet à une sélection : les sommets dont la caracteristique donnée en paramètres vérifie la condition sélectionnée
     * 
     *<table>
     *<tr><th>Caracteristique<th>Valeur passée
     *<tr><td>Taille des sommets<td>0
     *<tr><td>Nombre de passages<td>1
     *<tr><td>Degrés entrant<td>2
     *<tr><td>Degrés sortant<td>3
     *<tr><td>Degrés total<td>4
     *</table>
     *
     * @param caracteristique 
     */
    void addPropCond(int caracteristique);

  signals:
    void goBack(int window);
};
#endif