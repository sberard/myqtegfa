#include "QPCreateSelection.h"
#include "qcombobox.h"
#include "qlabel.h"
#include <cstdlib>

void QPCreateSelection::loadPos() {
    QComboBox *ind = ((QComboBox *)(this->getWidgetWithName("posInd")));
    for (auto [key, value] : db-> gC().getData()) {
        ind->addItem(QString::fromStdString(key));
    }
}
void QPCreateSelection::changeInd() {
    string indSelect = ((QComboBox *)(this->getWidgetWithName("posInd")))
                           ->currentText()
                           .toStdString();
    QComboBox *hap = ((QComboBox *)(this->getWidgetWithName("posHap")));
    hap->clear();
    if (db-> gC().estDefini(indSelect)) {
        for (auto [key, value] :
             db-> gC().getData()[indSelect].getData()) {
            hap->addItem(QString::number(key));
        }
    }
}
void QPCreateSelection::changeHap() {
    string indSelect = ((QComboBox *)(this->getWidgetWithName("posInd")))
                           ->currentText()
                           .toStdString();
    string hapSelect = ((QComboBox *)(this->getWidgetWithName("posHap")))
                           ->currentText()
                           .toStdString();
    QComboBox *cont = ((QComboBox *)(this->getWidgetWithName("posCont")));
    cont->clear();
    if (hapSelect != "" && db-> gC().estDefini(indSelect, stoi(hapSelect))) {
        for (auto [key, value] : db-> gC().getData()[indSelect]
                                          .getData()[stoi(hapSelect)]
                                       .getData()) {
            cont->addItem(QString::fromStdString(key));
        }
    }
}

void QPCreateSelection::changeCont() {
    string indSelect = ((QComboBox *)(this->getWidgetWithName("posInd")))
                           ->currentText()
                           .toStdString();
    string hapSelect = ((QComboBox *)(this->getWidgetWithName("posHap")))
                           ->currentText()
                           .toStdString();
    string contSelect = ((QComboBox *)(this->getWidgetWithName("posCont")))
                            ->currentText()
                            .toStdString();
    QLabel *txt = ((QLabel *)(this->getWidgetWithName("posLab")));
    txt->setText("Position dans le chemin parmi :");
    if (hapSelect != "" &&
        db-> gC().estDefini(indSelect, stoi(hapSelect), contSelect)) {
        for (auto cnt : db-> gC().getData()[indSelect]
                                    .getData()[stoi(hapSelect)]
                                 .getData()[contSelect]
                              .getData()) {
            txt->setText(txt->text() + "\n]" +
                         QString::number(cnt.getStart()) + "," +
                         QString::number(cnt.getEnd()) + "]");
        }
    }
}
