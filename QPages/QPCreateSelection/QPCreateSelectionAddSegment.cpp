#include "QPCreateSelection.h"
#include "class/LinkDataBase/LinkDataBase.h"
#include "class/SegmentDataBase/SegmentDataBase.h"
#include "class/WPDataBase/WPDataBase.h"
#include "librairies/warning/warning.h"
#include "qchar.h"
#include "qcombobox.h"
#include "qlabel.h"
#include "qlineedit.h"
#include "qspinbox.h"
#include <climits>

void QPCreateSelection::addOneSegment(const SommetOriente &som) {
    if (std::find(choisis.begin(), choisis.end(), som) == choisis.end()) {
        if (som.second == '+' || som.second == '-') {
            choisis.push_back(som);
        }
    }
};

void QPCreateSelection::changeNbChoisis() {
    ((QLabel *)(this->getWidgetWithName("nbAdd")))
        ->setText(QString::number(choisis.size()));
}

void QPCreateSelection::addPosContig() {
    string indSelect = ((QComboBox *)(this->getWidgetWithName("posInd")))
                           ->currentText()
                           .toStdString();
    string hapSelect = ((QComboBox *)(this->getWidgetWithName("posHap")))
                           ->currentText()
                           .toStdString();
    string contSelect = ((QComboBox *)(this->getWidgetWithName("posCont")))
                            ->currentText()
                            .toStdString();
    if (hapSelect != "" &&
        db-> gC().estDefini(indSelect, stoi(hapSelect), contSelect)) {
        for (auto lsom :db-> gC().getData()[indSelect]
                                    .getData()[stoi(hapSelect)]
                                  .getData()[contSelect]
                               .getData()) {
            for (SommetOrienteAvecPosition som :
                 lsom.getChemin().getData()) {
                addOneSegment(som.extraireSommet());
            }
        }
    }
    changeNbChoisis();
};

void QPCreateSelection::addPos() {
    string indSelect = ((QComboBox *)(this->getWidgetWithName("posInd")))
                           ->currentText()
                           .toStdString();
    string hapSelect = ((QComboBox *)(this->getWidgetWithName("posHap")))
                           ->currentText()
                           .toStdString();
    string contSelect = ((QComboBox *)(this->getWidgetWithName("posCont")))
                            ->currentText()
                            .toStdString();
    int posSelect = ((QSpinBox *)(this->getWidgetWithName("posSom")))->value();
    if (hapSelect != "" && db-> gC().estDefini(indSelect, stoi(hapSelect),
                                               contSelect, posSelect)) {
        addOneSegment(db-> gC().extrairePosition(indSelect, stoi(hapSelect),
                                                 contSelect, posSelect));
    }

    changeNbChoisis();
};

void QPCreateSelection::addName() {
    int idSom =
        db-> gS().getIdSommet(((QLineEdit *)(this->getWidgetWithName("nomSom")))
                                  ->text()
                                  .toStdString());
    int sensSom =
        ((QComboBox *)(this->getWidgetWithName("sensSom")))->currentIndex();
    if (sensSom == 0) {
        addOneSegment(SommetOriente(idSom, '+'));
        addOneSegment(SommetOriente(idSom, '-'));
    } else if (sensSom == 1) {
        addOneSegment(SommetOriente(idSom, '+'));
    } else if (sensSom == 2) {
        addOneSegment(SommetOriente(idSom, '-'));
    }
    changeNbChoisis();
};

void QPCreateSelection::addPropMin(int caracteristique) {
    int (InfosSommet::*_chooseCarac)( )const{};
    switch (caracteristique){
        case CARAC_LENGTH : {_chooseCarac = &InfosSommet::getLongueur;break;}
        case CARAC_PASS : {_chooseCarac = &InfosSommet::getPassage;break;}
        case CARAC_DEGIN : {_chooseCarac = &InfosSommet::getDegresIn;break;}
        case CARAC_DEGOUT : {_chooseCarac = &InfosSommet::getDegresOut;break;}
        case CARAC_DEGTOT : {_chooseCarac = &InfosSommet::getDegresTot;break;}
    }
    int tailleMin = INT_MAX;
    vector<int> argsMinId{};
    const vector<InfosSommet> ls = db-> gS().getInfos();
    for (int i = 0; i < (int)ls.size(); i++) {
        if ((ls[i].*_chooseCarac)() < tailleMin) {
            tailleMin = (ls[i].*_chooseCarac)();
            argsMinId.clear();
        }
        if ((ls[i].*_chooseCarac)() == tailleMin) {
            argsMinId.push_back(i);
        }
    }
    for (int id : argsMinId) {
        addOneSegment(SommetOriente(id, '-'));
        addOneSegment(SommetOriente(id, '+'));
    }
    changeNbChoisis();
};
void QPCreateSelection::addPropMax(int caracteristique) {
    int (InfosSommet::*_chooseCarac)( )const{};
    switch (caracteristique){
        case CARAC_LENGTH : {_chooseCarac = &InfosSommet::getLongueur;break;}
        case CARAC_PASS : {_chooseCarac = &InfosSommet::getPassage;break;}
        case CARAC_DEGIN : {_chooseCarac = &InfosSommet::getDegresIn;break;}
        case CARAC_DEGOUT : {_chooseCarac = &InfosSommet::getDegresOut;break;}
        case CARAC_DEGTOT : {_chooseCarac = &InfosSommet::getDegresTot;break;}
    }
    int tailleMax = 0;
    vector<int> argsMaxId{};
    const vector<InfosSommet> ls = db-> gS().getInfos();
    for (int i = 0; i < (int)ls.size(); i++) {
        if ((ls[i].*_chooseCarac)() > tailleMax) {
            tailleMax = (ls[i].*_chooseCarac)();
            argsMaxId.clear();
        }
        if ((ls[i].*_chooseCarac)() == tailleMax) {
            argsMaxId.push_back(i);
        }
    }

    for (int id : argsMaxId) {
        addOneSegment(SommetOriente(id, '-'));
        addOneSegment(SommetOriente(id, '+'));
    }

    changeNbChoisis();
};
void QPCreateSelection::addPropCond(int caracteristique) {
    int (InfosSommet::*_chooseCarac)( )const{};
    switch (caracteristique){
        case CARAC_LENGTH : {_chooseCarac = &InfosSommet::getLongueur;break;}
        case CARAC_PASS : {_chooseCarac = &InfosSommet::getPassage;break;}
        case CARAC_DEGIN : {_chooseCarac = &InfosSommet::getDegresIn;break;}
        case CARAC_DEGOUT : {_chooseCarac = &InfosSommet::getDegresOut;break;}
        case CARAC_DEGTOT : {_chooseCarac = &InfosSommet::getDegresTot;break;}
    }
    QStringList liCmp = {"taille", "passage", "degin", "degout",
                                 "degtot"};
    int valeur = ((QSpinBox *)(this->getWidgetWithName(liCmp[caracteristique] +"Val")))->value();
    int condition =
        ((QComboBox *)(this->getWidgetWithName(liCmp[caracteristique] +"Prop")))->currentIndex();
    if(valeur == 0 && condition >= 6){
        warning("Tentative de division par 0");
        return;
    }
    bool accept;
    int traite;
    const vector<InfosSommet> ls = db-> gS().getInfos();
    for (int i = 0; i < (int)ls.size(); i++) {
        traite = (ls[i].*_chooseCarac)();
        accept = false;
        switch (condition) {
        case COND_EQUAL: {
            accept = (traite == valeur);
            break;
        }
        case COND_LOWER_EQUAL: {
            accept = (traite <= valeur);
            break;
        }
        case COND_GREATER_EQUAL: {
            accept = (traite >= valeur);
            break;
        }
        case COND_LOWER: {
            accept = (traite < valeur);
            break;
        }
        case COND_GREATER: {
            accept = (traite > valeur);
            break;
        }
        case COND_DIFFERENT: {
            accept = (traite != valeur);
            break;
        }
        case COND_DIVISIBLE: {
            accept = (traite % valeur == 0);
            break;
        }
        case COND_NOT_DIVISIBLE: {
            accept = (traite % valeur != 0);
            break;
        }
        }
        if(accept){
            addOneSegment(SommetOriente(i, '-'));
            addOneSegment(SommetOriente(i, '+'));
        }
    };
    changeNbChoisis();
};
