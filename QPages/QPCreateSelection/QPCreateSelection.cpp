#include "QPCreateSelection.h"
#include "QPages/PageId.h"
#include "librairies/warning/warning.h"
#include "qboxlayout.h"
#include "qcombobox.h"
#include "qgridlayout.h"
#include "qlabel.h"
#include "qlineedit.h"
#include "qlist.h"
#include "qpushbutton.h"
#include "qspinbox.h"
#include "qwidget.h"
#include <QPushButton>
#include <unistd.h>

QPCreateSelection::QPCreateSelection(QWidget *parent) {
    QVBoxLayout *page = new QVBoxLayout(this);
    { //*header
        QHBoxLayout *header = new QHBoxLayout();
        {
            QPushButton *ret = new QPushButton("Annuler", parent);
            header->addWidget(ret);
            connect(ret, &QPushButton::clicked,
                        [=]() {emit closeMe(myWindow); });

            QLabel *nom = new QLabel("---");
            addNameToWidget("nom", nom);
            header->addWidget(nom);

            
        }
        page->addLayout(header);
    }
    { //*Main
        QHBoxLayout *main = new QHBoxLayout();
        QVBoxLayout *mainCol1 = new QVBoxLayout();
        QVBoxLayout *mainCol2 = new QVBoxLayout();
        { // Ligne 1 : positionGrid dans un chemin
            QWidget *posGeneralWidget = new QWidget();
            addNameToWidget("positionGrid", posGeneralWidget);
            QVBoxLayout *positionContent = new QVBoxLayout(posGeneralWidget);
            positionContent->addWidget(
                new QLabel("<b>Ajouter des sommets selon leur "
                           "position dans les chemins</b>"));
            {
                QGridLayout *positionGrid = new QGridLayout();

                // 0,0 / 0,1
                positionGrid->addWidget(new QLabel("Individu :"), 0, 0);
                positionGrid->addWidget(new QLabel("Haplotype :"), 0, 1);

                // 1,0
                QComboBox *ind = new QComboBox();
                addNameToWidget("posInd", ind);
                connect(ind, &QComboBox::currentTextChanged,
                        [=](const QString &) { changeInd(); });
                positionGrid->addWidget(ind, 1, 0);

                // 1,1
                QComboBox *hap = new QComboBox();
                addNameToWidget("posHap", hap);
                connect(hap, &QComboBox::currentTextChanged,
                        [=](const QString &) { changeHap(); });

                positionGrid->addWidget(hap, 1, 1);

                // 2,0
                positionGrid->addWidget(new QLabel("Contig :"), 2, 0);

                // 3,0
                QComboBox *cnt = new QComboBox();
                connect(cnt, &QComboBox::currentTextChanged,
                        [=](const QString &) { changeCont(); });

                addNameToWidget("posCont", cnt);
                positionGrid->addWidget(cnt, 3, 0);

                // 3,1
                QPushButton *addCnt =
                    new QPushButton("Ajouter tous les sommets du contig");
                connect(addCnt, &QPushButton::clicked,
                        [=]() { addPosContig(); });

                positionGrid->addWidget(addCnt, 3, 1);

                // 4,0
                QLabel *posLab = new QLabel("Position dans le chemin :");
                addNameToWidget("posLab", posLab);
                positionGrid->addWidget(posLab, 4, 0);

                // 5,0
                QSpinBox *pos = new QSpinBox();
                pos->setRange(0, INT_MAX);
                addNameToWidget("posSom", pos);
                positionGrid->addWidget(pos, 5, 0);

                // 5,1
                QPushButton *addPos =
                    new QPushButton("Ajouter le sommet à cette position");
                connect(addPos, &QPushButton::clicked,
                        [=]() { this->addPos(); });
                positionGrid->addWidget(addPos, 5, 1);

                // 6,0
                positionGrid->addWidget(new QLabel("Jusqu'à"), 6, 0);

                // 7,0
                QSpinBox *posEnd = new QSpinBox();
                posEnd->setRange(0, INT_MAX);
                addNameToWidget("posSomEnd", posEnd);
                positionGrid->addWidget(posEnd, 7, 0);

                // 7,1
                QPushButton *addPosEnd = new QPushButton(
                    "Ajouter des sommets jusqu'à cette position");
                positionGrid->addWidget(addPosEnd, 7, 1);
                QFrame *f2 = new QFrame();
                f2->setFrameShape(QFrame::HLine);
                positionContent->addLayout(positionGrid);
                positionContent->addWidget(f2);
            }
            mainCol2->addWidget(posGeneralWidget);
        }
        { // NomSommets
            QVBoxLayout *nomContent = new QVBoxLayout();
            {
                nomContent->addWidget(
                    new QLabel("<b>Ajouter des sommet selon leur "
                               "nom dans le fichier gfa</b>"));
                QGridLayout *nomGrid = new QGridLayout();
                {
                    // 0,0
                    nomGrid->addWidget(
                        new QLabel("Entrez le nom et le sens du sommet :"), 0,
                        0);

                    // 1,0
                    QLineEdit *nomSom = new QLineEdit();
                    addNameToWidget("nomSom", nomSom);
                    nomGrid->addWidget(nomSom, 1, 0);
                    nomSom->setReadOnly(false);

                    // 1,1
                    QComboBox *sensSom = new QComboBox();
                    sensSom->addItem("+ et -");
                    sensSom->addItem("+");
                    sensSom->addItem("-");
                    addNameToWidget("sensSom", sensSom);
                    nomGrid->addWidget(sensSom, 1, 1);

                    // 1,2
                    QPushButton *addNom =
                        new QPushButton("Ajouter ce sommet", parent);
                    connect(addNom, &QPushButton::clicked,
                            [=]() { this->addName(); });

                    nomGrid->addWidget(addNom, 1, 2);
                }

                nomContent->addLayout(nomGrid);
                QFrame *f1 = new QFrame();
                f1->setFrameShape(QFrame::HLine);
                nomContent->addWidget(f1);
            }
            mainCol1->addLayout(nomContent);
        }
        {
            QStringList comparateurs = {
                "Égal à",        "Inferieur ou égale à", "Superieur ou égale à",
                "Inferieur à",   "Superieur à",          "Différent de",
                "Divisible par", "Non divisible par"};
            QStringList liCmp = {"taille", "passage", "degin", "degout",
                                 "degtot"};
            QString a = "<b>Ajouter le(s) sommet(s) de <i>";
            QString b = "</i></b> :";
            QStringList licmpReel = {"Taille", "Passage", "Degrés Entrant",
                                     "Degrés Sortant", "Degrés Total"};
            vector<QVBoxLayout *> col = {mainCol2, mainCol2, mainCol1, mainCol1,
                                         mainCol1};

            for (int i = 0; i < liCmp.size(); i++) {
                QVBoxLayout *caracContent = new QVBoxLayout();
                caracContent->addWidget(new QLabel(a + licmpReel[i] + b));
                {
                    QGridLayout *caracGrid = new QGridLayout();
                    {
                        // 0,0
                        QPushButton *bm = new QPushButton("Minimum");
                        connect(bm, &QPushButton::clicked,
                                [=]() { this->addPropMin(i); });
                        caracGrid->addWidget(bm, 0, 0);

                        // 0,1
                        QPushButton *bM = new QPushButton("Maximum");
                        connect(bM, &QPushButton::clicked,
                                [=]() { this->addPropMax(i); });
                        caracGrid->addWidget(bM, 0, 1);

                        // 1,0
                        QComboBox *prop = new QComboBox();
                        prop->addItems(comparateurs);
                        addNameToWidget(liCmp[i] + "Prop", prop);
                        caracGrid->addWidget(prop, 1, 0);

                        // 1,1
                        QSpinBox *val = new QSpinBox();
                        val->setRange(0, INT_MAX);
                        addNameToWidget(liCmp[i] + "Val", val);
                        caracGrid->addWidget(val, 1, 1);

                        // 1,2
                        QPushButton *bC = new QPushButton("Ajouter");
                        connect(bC, &QPushButton::clicked,
                                [=]() {this->addPropCond(i); });

                        caracGrid->addWidget(bC, 2, 0);
                    }
                    caracContent->addLayout(caracGrid);
                }
                ((QVBoxLayout *)(col[i]))->addLayout(caracContent);
            }
        }

        main->addLayout(mainCol1);
        QFrame *fc = new QFrame();
        fc->setFrameShape(QFrame::VLine);
        main->addWidget(fc);
        main->addLayout(mainCol2);
        page->addLayout(main);
    }
    { //*Footer
        QVBoxLayout *footer = new QVBoxLayout();
        QFrame *fb = new QFrame();
        fb->setFrameShape(QFrame::HLine);
        footer->addWidget(fb);
        {
            QHBoxLayout *footerNb = new QHBoxLayout();
            footerNb->addWidget(new QLabel("<b>Nombre de sommets dans la séléction</b>"));
            QLabel *nbAdd = new QLabel("0"); 
            addNameToWidget("nbAdd", nbAdd);
            footerNb->addWidget(nbAdd);
            QPushButton * nbReset = new QPushButton("Recommencer");
            connect(nbReset, &QPushButton::clicked, [=](){
                this->choisis.clear();
                changeNbChoisis();
            });
            footerNb->addWidget(nbReset);
            footer->addLayout(footerNb);
        }
        footer->addWidget(
            new QLabel("<b>Choisir un nom pour la séléction</b>"));
        {
            QHBoxLayout *footerName = new QHBoxLayout();
            QLineEdit *nomSelection = new QLineEdit();
            nomSelection->setPlaceholderText(
                "Entrez le nom de la séléction ici");
            nomSelection->setText("Ma séléction");
            addNameToWidget("nomSelection", nomSelection);
            footerName->addWidget(nomSelection);

            QPushButton *valider = new QPushButton("Enregistrer la sélection");
            connect(valider, &QPushButton::clicked, [=]() {
                db->addSelection(
                    ((QLineEdit *)(getWidgetWithName("nomSelection")))
                        ->text()
                        .toStdString(),
                    this->choisis);
                emit reloadPred(myWindow);
                emit this->closeMe(myWindow);
            });
            footerName->addWidget(valider);
            footer->addLayout(footerName);
        }
        page->addLayout(footer);
    }
}
void QPCreateSelection::goHere(int mode) {
    this->show();
    ((QLabel *)(this->getWidgetWithName("nom")))->setText(db->nomFichier);
    if (mode == MODE_RESTART) {
        if (db->getPaths) {
            this->loadPos();
            this->getWidgetWithName("positionGrid")->show();

        } else {
            this->getWidgetWithName("positionGrid")->hide();
        }
    }
}