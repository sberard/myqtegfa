#include "QPIndInf.h"
#include "QForms/QForms.h"
#include "QPages/PageId.h"
#include "librairies/StringExtention/stringExtention.h"
#include "qboxlayout.h"
#include "qchar.h"
#include "qcombobox.h"
#include "qlabel.h"
#include "qlineedit.h"
#include "qwidget.h"
#include <QLabel>
#include <QLineEdit>
#include <QTableWidget>
#include <filesystem>
QPIndInf::QPIndInf(QWidget *parent) {
    QVBoxLayout *page = new QVBoxLayout(this);

    //*Header
    {
        QHBoxLayout *header = new QHBoxLayout();
        // Bouton de retour en ariere
        {
            QPushButton *ret = new QPushButton("Retour", parent);
            header->addWidget(ret);
            connect(ret, &QPushButton::clicked, [=](){emit goBack(myWindow);});
        }
        // Titre
        {
            QLabel *nom = new QLabel("<U>Ici le nom du fichier</U>");
            header->addWidget(nom);
            addNameToWidget("nom", nom);
        }
        page->addLayout(header);
    }
    //*ChooseInd
    {
        QHBoxLayout *nbI = new QHBoxLayout();
        {
            nbI->addWidget(new QLabel("Nombre d'individus"));
            this->addNameToWidget("nbInd", new QLabel("---"));
            nbI->addWidget(this->getWidgetWithName("nbInd"));
        }
        page->addLayout(nbI);
    }
    //*ChooseInd
    {
        page->addWidget(new QLabel("<b>Veuillez choisir un individu ci-dessous</b>"));
        QComboBox *cb = new QComboBox(this);
        addNameToWidget("actInd", cb);
        connect(cb, SIGNAL(currentTextChanged(const QString &)), this,
                SLOT(whenSelectInd(const QString &)));
        page->addWidget(cb);
    }
    //*Informations
    {
        QWidget *mainPage = new QWidget(this);
        addNameToWidget("contents", mainPage);
        QVBoxLayout *contents = new QVBoxLayout(mainPage);
        // NbHap
        {
            QHBoxLayout *haps = new QHBoxLayout();
            {
                haps->addWidget(new QLabel("Nombre d'haplotypes : "));
                QLabel *hapsVal = new QLabel("");
                addNameToWidget("nbHaps", hapsVal);
                haps->addWidget(hapsVal);
            }

            contents->addLayout(haps);
        }
        // NbContig
        {
            QVBoxLayout *conts = new QVBoxLayout();
            {
                conts->addWidget(
                    new QLabel("Nombre de contigues par haplotype : "));
                QLabel *nbContig = new QLabel("", this);
                addNameToWidget("nbContig", nbContig);
                conts->addWidget(nbContig);
            }
            contents->addLayout(conts);
        }
        page->addWidget(mainPage);
    }
    //*Regénérer FASTA
    {
        QForms *fasta = new QForms();
        addNameToWidget("formFasta", fasta);
        fasta->addLineEdit("nom", "Entrez le nom du fasta ici");
        fasta->addConfirmButton("ok", "Reconstruire le .fasta");
        connect(fasta, SIGNAL(endForm()), this, SLOT(downloadFasta()));
        page->addWidget(fasta);
    }
    //*Regénérer tous les FASTA
    {
        QForms *Afasta = new QForms();
        addNameToWidget("allFasta", Afasta);
        Afasta->addConfirmButton("ok", "Reconstruire tous les .fasta");
        connect(Afasta, SIGNAL(endForm()), this, SLOT(downloadAllFasta()));
        page->addWidget(Afasta);
    }
};
void QPIndInf::setDataBase(DataBase *dataBase) {
    this->db = dataBase;
    this->actCont = db->lastCont;
    this->actHap = db->lastHap;
    this->actInd = db->lastInd;
};
void QPIndInf::goHere(int mode) {
    switch (mode) {
    case MODE_CONTINUE: {
        this->show();
        if (actInd == QString()) {
            getWidgetWithName("contents")->hide();
        }
        break;
    }
    case MODE_RESTART: {
        ((QForms *)(getWidgetWithName("allFasta")))->enable();
        ((QLabel *)(this->getWidgetWithName("nom")))->setText(db->nomFichier);
        ((QLabel *)(this->getWidgetWithName("nbInd")))->setText(QString::number(this->db-> gC().getData().size()));
        this->show();
        this->actCont = db->lastCont;
        this->actHap = db->lastHap;
        this->actInd = db->lastInd;
        QComboBox *selectInd = (QComboBox *)getWidgetWithName("actInd");
        selectInd->clear();
        QLineEdit *t = new QLineEdit();
        t->setPlaceholderText("Choisir un individu");
        selectInd->setLineEdit(t);
        for (auto [nomInd, indDb] : db-> gC().getData()) {
            selectInd->addItem(QString::fromStdString(nomInd));
        }
        if (actInd != QString()) {
            selectInd->setCurrentText(actInd);
            whenSelectInd(actInd);
        } else {
            getWidgetWithName("contents")->hide();
            getWidgetWithName("formFasta")->hide();
        }
        if (db->getChaines && db->getPaths) {
            getWidgetWithName("allFasta")->show();
        } else {
            getWidgetWithName("allFasta")->show();
        }
        break;
    }
    }
};

void QPIndInf::whenSelectInd(const QString &nameInd) {
    this->actInd = nameInd;
    db->lastInd = nameInd;
    getWidgetWithName("contents")->show();
    ((QLabel *)(this->getWidgetWithName("nbHaps")))
        ->setText(formatedFloatToQString(
            (db-> gC().getData())[nameInd.toStdString()]
                .getData()
                .size()));
    QLabel *nbByHap = (QLabel *)(this->getWidgetWithName("nbContig"));
    QString txt = "";
    for (auto [key, val] :
         db-> gC().getData()[nameInd.toStdString()].getData()) {
        txt += QString::number(key);
        txt += " : ";
        txt += QString::number(val.getData().size());
        txt += "<br>";
    }
    nbByHap->setText(txt);
    if (db->getChaines && db->getPaths) {
        getWidgetWithName("formFasta")->show();
        ((QLineEdit *)(((QForms *)(getWidgetWithName("formFasta")))
                           ->getWidget("nom")))
            ->setText(QString::fromStdString("./out/") + nameInd +
                      QString::fromStdString(".fasta"));
    } else {
        getWidgetWithName("formFasta")->hide();
    }
};

void QPIndInf::downloadFasta() {
    db->createFasta(((QForms *)(getWidgetWithName("formFasta")))
                        ->getStringForm("nom")
                        .toStdString(),
                    this->actInd.toStdString());
    ((QForms *)(getWidgetWithName("formFasta")))->enable();
};
void QPIndInf::downloadAllFasta() {
    string basename = string(
        split(split(db->nomFichier.toStdString(),
                    '/')[split(db->nomFichier.toStdString(), '/').size() - 1],
              '.')[0]);
    if (filesystem::create_directories("./out/" + basename)) {
        for (auto [key, val] : db-> gC().getData()) {
            db->createFasta("./out/" + basename + "/" + key + ".fasta", key);
        }
    }
};
