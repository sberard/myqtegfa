/**
 * @file QPIndInf.h
 * @author Nael BB
 * @brief QPage permetant de séléctionner un individu pour en afficher des
 * informations ou en extraire les fasta
 */
#ifndef IND_INFOS_QT_
#define IND_INFOS_QT_
#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include "qchar.h"
#include <QComboBox>
#include <QGridLayout>
#include <QPushButton>
#include <QtDebug>
/**
 * @brief QPage permetant de séléctionner un individu pour en afficher des
 * informations ou en extraire les fasta
 *
 */
class QPIndInf : public IQPages {
    Q_OBJECT
    QString actInd = QString();
    int actHap = -1;
    QString actCont = QString();

  public:
    /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPIndInf(QWidget *parent = 0);
    /**
     * @brief Calcule et affiche les informations
     *
     */
    void recalcul();
    /**
     * @brief Relie la database (Spécialisation : récupération des ind/cont/hap en plus)
     *
     * @param dataBase 
     */
    void setDataBase(DataBase * dataBase);
  public slots:
    /**
     * @brief Quand un individu est séléctionné, ce slot affiche les
     * informations sur cet individu
     *
     * @param nameInd
     */
    void whenSelectInd(const QString &nameInd);
    void goHere(int mode = MODE_RESTART);
    /**
     * @brief Télécharge le fasta lié à un individu
     *
     */
    void downloadFasta();
    /**
     * @brief Télécharge tous les fasta
     *
     */
    void downloadAllFasta();
  signals:
    void goBack(int window);
};
#endif