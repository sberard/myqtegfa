#include "IQPages.h"
void IQPages::setWindow(int window){this->myWindow = window;};
void IQPages::setDataBase(DataBase *dataBase) { this->db = dataBase; };
void IQPages::addNameToWidget(QString nom, QWidget *widget) {
    this->widgets[nom] = widget;
};
QWidget *IQPages::getWidgetWithName(QString nom) { return this->widgets[nom]; };
void IQPages::eraseWidgetWithName(QString nom){this->widgets.erase(nom);};