/**
 * @file IQPages.h
 * @author Nael BB
 * @brief Classe pensée comme une inteface permétant de modéliser une page, en
 * fonctionnement avec QGestionnaireFenetres
 */
#ifndef I_PAGES_QT_
#define I_PAGES_QT_
#include "../../class/DataBase/DataBase.h"
#include "../PageId.h"
#include <QWidget>
/**
 * @brief Classe pensée comme une inteface permétant de modéliser une page, en
 * fonctionnement avec #QGestionnaireFenetres
 *
 * Fonctionnement :
 * IQPages est une interface permétant d'implémenter des pages dans
 * l'application, et de leur donner accès à tout le contenu de l'application
 * tout en laissant leur controle réstreint Une classe héritée IQPages possèdera
 * donc des methodes et signaux déjà implémentées permétant de <ul><li>Se gérer
 * elle même, via des methodes appelées à la création, lors de l'ouverture et au
 * rechargement de la page</li> <li>Gérer les widgets qui lui sont associés, en
 * ayant à disposition un système d'atribution de nom aux widgets pour pouvoir
 * les utiliser dans l'intégralité de la page</li> <li>Gérer la navigation dans
 * l'application, ouvrir de nouvelles fenêtres, en fermer, revenir en arière
 * ext.</li> <li>Gérér les données de l'application, avec un acces direct à la
 * base de données DataBase (accès transmis par QGestionnaireFenetres)</li></ul>
 *
 * La page as toujours accès à son identifiant de fenêtre, utilisé pour les
 * signaux de la classe
 *
 * Créer un nouveau type de pages :
 * <ol><li>Créer une classe qui hérite de IQPages</li><li>Définir dans le constructeur les choses statiques, les widgets toujours présents sur la page par exemple</li>
 * <li>Définir la methode goHere avec si besoin une diférentiation selon le paramètre, qui définit la facon dont la page s'affiche (Ne pas oublier de mettre this->show() !)</li>
 * <li>Connecter la page au reste de l'application, en émétant les signaux necessaires aux bons moments, en connectant des widgets à ces signaux par exemple</li>
 * <li>Définir des slots d'affichage dynamiques si besoin, qui peuvent être connectés à des widgets</li>
 * <li>Si besoin, utiliser les methodes addNameToWidget et getWidgetWithName (pour par exemple récuperer des informations d'un widget ailleurs que lors de sa création)</li>
 * <li>Connecter le reste de l'application à la page (voir tutoriel sur QGestionnaireFenetres)</li></ol>
 */
class IQPages : public QWidget {
    Q_OBJECT
    map<QString, QWidget *> widgets = map<QString, QWidget *>();
  protected:
    DataBase *db = 0;
    int myWindow;
  public:
    void setWindow(int window);
    /**
     * @brief Permet de donner accès la base de données générale de
     * l'application à la page
     *
     * Cette fonction est appelée automatiquement par QGestionnaireFenetres
     * @param dataBase
     */
    void setDataBase(DataBase *dataBase);
  public slots:
    /**
     * @brief Fonction appelée lors de l'ouverture/rechargement de la page.
     *
     * Lors de l'ouverture, le paramètre est MODE_RESTART. Lors d'un
     * rechargement, la paramètre est MODE_CONTINUE
     *
     * @param mode
     */
    virtual void goHere(int mode = MODE_RESTART) = 0;
    /**
     * @brief Permet d'ajouter une étiquette à un QWidget
     *
     * Attention, ne fonctionne pas avec un QLayout<br>
     * Pour récuperer le widget, voir IQPages::getWidgetWithName<br>
     *
     * Si l'étiquette existait déjà, l'ancien widget est écrasé
     *
     * @param nom Nom du widget
     * @param widget Widget à ajouter
     */
    void addNameToWidget(QString nom, QWidget *widget);
    /**
     * @brief Permet de récupérer un widget étiqueté
     *
     * Attention : Aucune protéction en cas d'absence d'existance du widget !<br>
     * Attention : La valeur de retour est un QWidget*, vous pouvez donc
     * modifier le widget, mais il faudra surement le cast en type correct.<br>
     * Prenez garde aux erreurs de segmentation
     *
     * @param nom
     * @return QWidget*
     */
    QWidget *getWidgetWithName(QString nom);
    /**
     * @brief Permet de supprimer un widget étiqueté
     *
     * Attention : Aucune protéction en cas d'absence d'existance du widget !<br>
     *
     * @param nom
     * @return QWidget*
     */
    void eraseWidgetWithName(QString nom);

  signals:
    /**
     * @brief Permet d'ouvrir une page de type PageId dans la fenêtre donnée en
     * paramètre
     *
     * Note : Le plus souvent, le premier paramètre est myWindow<br>
     * Signal connecté à QGestionnaireFenetres::openPageInWindow si la page est crée via QGestionnaireFenetres
     *
     * @param window Identifiant de fenêtre
     * @param pageId Type de page
     */
    void goForward(int window, PageId pageId);
    /**
     * @brief Permet de retourner en arière dans la fenêtre donnée en paramètre
     *
     * Note : Le plus souvent, le premier paramètre est myWindow<br>
     * Signal connecté à QGestionnaireFenetres::returnInWindow si la page est crée via QGestionnaireFenetres
     *
     * @param window Identifiant de fenêtre
     */
    void goBack(int window);
    /**
     * @brief Permet d'ouvrir une fenêtre fille de window de type PageId
     *
     * Note : Le plus souvent, le premier paramètre est myWindow<br>
     * Signal connecté à QGestionnaireFenetres::returnInWindow si la page est
     * crée via QGestionnaireFenetres
     *
     * @param window Identifiant de fenêtre
     * @param pageId Type de page
     */
    void openNew(int window, PageId pageId);
    /**
     * @brief Permet de fermer une fenêtre
     *
     * Note : Le plus souvent, le premier paramètre est myWindow<br>
     * Signal connecté à QGestionnaireFenetres::closeWindow si la page est crée
     * via QGestionnaireFenetres
     *
     * @param window Identifiant de fenêtre
     */
    void closeMe(int window);
    /**
     * @brief Permet de fermer toutes les fenêtres sauf une
     *
     * Note : Le plus souvent, le premier paramètre est myWindow<br>
     * Signal connecté à QGestionnaireFenetres::closeAllWindowsExcept si la page
     * est crée via QGestionnaireFenetres
     *
     * @param window Identifiant de fenêtre
     */
    void closeEverythingButMe(int window);
    /**
     * @brief Permet de recharger le predecesseur d'une fenêtre
     *
     * Note : Le plus souvent, le premier paramètre est myWindow<br>
     * Signal connecté à QGestionnaireFenetres::closeAllWindowsExcept si la page
     * est crée via QGestionnaireFenetres
     *
     * @param window Identifiant de fenêtre
     */
    void reloadPred(int myWindow);
};
#endif