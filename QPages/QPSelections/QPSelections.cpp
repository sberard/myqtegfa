#include "QPSelections.h"
#include "QPages/PageId.h"
#include "qboxlayout.h"
#include "qchar.h"
#include "qcheckbox.h"
#include "qframe.h"
#include "qgridlayout.h"
#include "qlabel.h"
#include "qlineedit.h"
#include "qnamespace.h"
#include "qpushbutton.h"
#include "qradiobutton.h"
#include "qspinbox.h"
#include <QFileDialog>
#include <climits>
#include <QRadioButton>
QPSelections::QPSelections(QWidget *parent) {
    QVBoxLayout *page = new QVBoxLayout(this);
    { //*header
        QHBoxLayout *header = new QHBoxLayout();
        {
            QPushButton *ret = new QPushButton("Annuler", parent);
            header->addWidget(ret);
            connect(ret, &QPushButton::clicked,
                    [=]() { emit goBack(myWindow); });

            QLabel *nom = new QLabel("---");
            addNameToWidget("nom", nom);
            header->addWidget(nom);
        }
        page->addLayout(header);
    }
    { //*Main
        QVBoxLayout *main = new QVBoxLayout();
        QPushButton *importSelection = new QPushButton("Importer des séléctions");
            connect(importSelection, &QPushButton::clicked, [=]() {
                for (QString nameFile : QFileDialog::getOpenFileNames(
                         this, "Choisir des sélections", "/home", "*.txt")) {
                    db->importSelection(nameFile.toStdString());
                };
                goHere(MODE_CONTINUE);
            });
        QPushButton *addSelection = new QPushButton("Créer une séléction");
        connect(addSelection, &QPushButton::clicked,
                [=]() { openNew(myWindow, PageId::CreateSelection); });
        selectionsLayout = new QGridLayout();
        QCheckBox *allSelect = new QCheckBox("Tous");
        connect(allSelect, &QCheckBox::stateChanged, [=](int newState) {
            if (newState == 0) {
                for (auto [sel, selValue] : db->getSelections()) {
                    ((QCheckBox *)(getWidgetWithName(
                         QString("selection") + QString::fromStdString(sel) +
                         QString("Check"))))
                        ->setCheckState(Qt::Unchecked);
                }
            }
            if (newState == 2) {
                for (auto [sel, selValue] : db->getSelections()) {
                    ((QCheckBox *)(getWidgetWithName(
                         QString("selection") + QString::fromStdString(sel) +
                         QString("Check"))))
                        ->setCheckState(Qt::Checked);
                }
            }
        });
        selectionsLayout->addWidget(allSelect, 0, 0);
        selectionsLayout->addWidget(new QLabel("Nom de la séléction"), 0, 1);
        selectionsLayout->addWidget(new QLabel("Taille"), 0, 2);
        main->addWidget(importSelection);
        main->addWidget(addSelection);
        main->addLayout(selectionsLayout);
        page->addLayout(main);
    }
    { //*Footer
        QHBoxLayout *footer = new QHBoxLayout();
        { // Gestion des sélections

            
            QVBoxLayout *selectedGestion = new QVBoxLayout();
            QLabel *selectedLabel = new QLabel(
                "<b>Créer une nouvelle séléction</b><br>Nom de la sélection :");
            QLineEdit *selectedName = new QLineEdit("nvl_selection");
            addNameToWidget("nomSelected", selectedName);
            QPushButton *selectedUnion = new QPushButton("Créer l'union");
            connect(selectedUnion, &QPushButton::clicked,
                    [=]() { unionSelectedSelections(); });
            QPushButton *selectedInter =
                new QPushButton("Créer l'intersection");
            connect(selectedInter, &QPushButton::clicked,
                    [=]() { intersectionSelectedSelections(); });
            QPushButton *selectedExport =
                new QPushButton("Exporter ces séléctions");
            connect(selectedExport, &QPushButton::clicked, [=]() {
                for (string i : selected) {
                    db->exportSelection(i, string("./out/selection") + i +
                                               string(".txt"));
                }
            });
            QPushButton *selectedDelete =
                new QPushButton("Supprimer ces séléctions");
            connect(selectedDelete, &QPushButton::clicked,
                    [=]() { destroySelectedSelections(); });
            selectedGestion->addWidget(selectedLabel);
            selectedGestion->addWidget(selectedName);
            selectedGestion->addWidget(selectedUnion);
            selectedGestion->addWidget(selectedInter);
            selectedGestion->addWidget(selectedExport);
            selectedGestion->addWidget(selectedDelete);
            footer->addLayout(selectedGestion);
        }
        QFrame *fb = new QFrame();
        fb->setFrameShape(QFrame::VLine);
        footer->addWidget(fb);
        { // Enregistrement du sous-graphe
            QVBoxLayout *download = new QVBoxLayout();
            QLabel *dowLabel = new QLabel(
                "<b>Extraire un sous graphe autour des séléctions</b><br>Nom du fichier :");
            QLineEdit *dowName = new QLineEdit("nvl_extraction");
            addNameToWidget("dowName", dowName);
            QLabel *dowLabel2 = new QLabel(
                "Distance autour des sommets :");
            QSpinBox * dowDist = new QSpinBox();
            dowDist->setRange(0, INT_MAX);
            addNameToWidget("dowDist", dowDist);
            QLabel *dowLabel3 = new QLabel(
                "Type de distance :");
            QRadioButton * dowBR = new QRadioButton("Distance en sommets");
            QRadioButton * dowBD = new QRadioButton("Distance en chaines");
            addNameToWidget("dowBSommets", dowBR);
            dowBR->setChecked(true);
            dowBD->setChecked(false);
            connect(dowBR, &QPushButton::clicked,[=](){if(dowBR->isChecked()){dowBD->setChecked(false);}});
            connect(dowBD, &QPushButton::clicked,[=](){if(dowBD->isChecked()){dowBR->setChecked(false);}});
            QCheckBox * dowSens = new QCheckBox("Distinguer les sommets + et -");
            addNameToWidget("dowSens", dowSens);
            QCheckBox * dowAffName = new QCheckBox("Afficher les noms des sommets");
            addNameToWidget("dowAffName", dowAffName);
            QCheckBox * dowAffSize = new QCheckBox("Afficher les tailles des sommets");
            addNameToWidget("dowAffSize", dowAffSize);
            QCheckBox * dowAffPass = new QCheckBox("Afficher le passage des sommets");
            addNameToWidget("dowAffPass", dowAffPass);
            QCheckBox * dowAffDegin = new QCheckBox("Afficher les degrés entrant des sommets");
            addNameToWidget("dowAffDegin", dowAffDegin);
            QCheckBox * dowAffDegout = new QCheckBox("Afficher les degrés sortant des sommets");
            addNameToWidget("dowAffDegout", dowAffDegout);
            //QPushButton * dowGFA = new QPushButton("Extraire au format gfa");
            QPushButton * dowDOT = new QPushButton("Extraire au format dot");
            connect(dowDOT, SIGNAL(clicked()),this,SLOT(writeDot()));
            download->addWidget(dowLabel);
            download->addWidget(dowName);
            download->addWidget(dowLabel2);
            download->addWidget(dowDist);
            download->addWidget(dowLabel3);
            download->addWidget(dowBD);
            download->addWidget(dowBR);
            download->addWidget(dowSens);
            download->addWidget(dowAffName);
            download->addWidget(dowAffSize);
            download->addWidget(dowAffPass);
            download->addWidget(dowAffDegin);
            download->addWidget(dowAffDegout);
            //download->addWidget(dowGFA);
            download->addWidget(dowDOT);
            footer->addLayout(download);
        }
        page->addLayout(footer);
    }
};
void QPSelections::goHere(int mode) {
    this->show();
    ((QLabel *)(this->getWidgetWithName("nom")))->setText(db->nomFichier);
    clearSelectionLayout();

    int row = 1;
    for (auto [nomSelection, selection] : db->getSelections()) {
        string nomS2 = nomSelection;
        QCheckBox *check = new QCheckBox();
        if (std::find(selected.begin(), selected.end(), nomSelection) !=
            selected.end()) {
            check->setCheckState(Qt::Checked);
        }
        connect(check, &QCheckBox::stateChanged, [=](int newState) {
            if (newState == 0) {
                removeSelect(nomS2);
            }
            if (newState == 2) {
                addSelect(nomS2);
            }
        });
        addNameToWidget(QString("selection") +
                            QString::fromStdString(nomSelection) +
                            QString("Check"),
                        check);
        QLabel *name = new QLabel(QString::fromStdString(nomSelection));
        addNameToWidget(QString("selection") +
                            QString::fromStdString(nomSelection) +
                            QString("Name"),
                        check);
        QLabel *size = new QLabel(QString::number(selection.size()));
        addNameToWidget(QString("selection") +
                            QString::fromStdString(nomSelection) +
                            QString("Size"),
                        check);
        QPushButton *view = new QPushButton("Voir");
        connect(view, &QPushButton::clicked, [=]() {
            db->selectionToView = nomS2;
            openNew(myWindow, PageId::ViewSelection);
        });
        addNameToWidget(QString("selection") +
                            QString::fromStdString(nomSelection) +
                            QString("View"),
                        check);
        QPushButton *del = new QPushButton("Supprimer");
        connect(del, &QPushButton::clicked, [=]() {
            removeSelect(nomS2);
            db->deleteSelection(nomS2);
            goHere(MODE_RESTART);
        });
        addNameToWidget(QString("selection") +
                            QString::fromStdString(nomSelection) +
                            QString("Delete"),
                        check);

        selectionsLayout->addWidget(check, row, 0);
        selectionsLayout->addWidget(name, row, 1);
        selectionsLayout->addWidget(size, row, 2);
        selectionsLayout->addWidget(view, row, 3);
        selectionsLayout->addWidget(del, row, 4);

        row++;
    }
};

void QPSelections::clearSelectionLayout() {
    int i = 1;
    while (selectionsLayout->itemAtPosition(i, 0) != 0) {
        QString nomS =
            ((QLabel *)(selectionsLayout->itemAtPosition(i, 1)->widget()))
                ->text();
        eraseWidgetWithName("selection" + nomS + "Check");
        eraseWidgetWithName("selection" + nomS + "Name");
        eraseWidgetWithName("selection" + nomS + "Size");
        eraseWidgetWithName("selection" + nomS + "View");
        eraseWidgetWithName("selection" + nomS + "Delete");
        for (int j = 0; j < selectionsLayout->columnCount(); j++) {
            delete selectionsLayout->itemAtPosition(i, j)->widget();
        }
        i++;
    }
};

void QPSelections::destroySelectedSelections() {
    for (string nomSelection : selected) {
        db->deleteSelection(nomSelection);
    }
    selected.clear();
    goHere(MODE_CONTINUE);
};
void QPSelections::unionSelectedSelections() {
    QString nomSelected =
        ((QLineEdit *)(getWidgetWithName("nomSelected")))->text();
    db->unionSelections(selected, nomSelected.toStdString());
    goHere(MODE_CONTINUE);
};
void QPSelections::intersectionSelectedSelections() {
    QString nomSelected =
        ((QLineEdit *)(getWidgetWithName("nomSelected")))->text();
    db->interSelections(selected, nomSelected.toStdString());
    goHere(MODE_CONTINUE);
};

void QPSelections::addSelect(const string &nom) {
    if (std::find(selected.begin(), selected.end(), nom) == selected.end()) {
        selected.push_back(nom);
    }
    cout << endl;
};
void QPSelections::removeSelect(const string &nom) {
    if (std::find(selected.begin(), selected.end(), nom) != selected.end()) {
        selected.erase(std::find(selected.begin(), selected.end(), nom));
    }
    cout << endl;
};