#include "QPages/QPSelections/QPSelections.h"
#include "class/LinkDataBase/LinkDataBase.h"
#include "class/SegmentDataBase/SegmentDataBase.h"
#include "librairies/StringExtention/stringExtention.h"
#include "librairies/warning/warning.h"
#include "qcheckbox.h"
#include "qlineedit.h"
#include "qradiobutton.h"
#include "qspinbox.h"
#include <fstream>
#include <string>

string QPSelections::label(int id, bool affName, bool affSize, bool affPass,
                           bool affDegin, bool affDegout, bool affSens,
                           char sens) {
    InfosSommet *s = db->gS().getSommet(id);
    string ret{};
    if (affName) {
        ret += db->gS().getNameSommet(id);
    }
    if (affName && affSens) {
        ret += sens;
    }
    if (affSize || affPass || affDegin || affDegout) {
        ret += "[";
        if (affSize) {
            ret +=
                "s=" + formatedFloatToQString(s->getLongueur()).toStdString();
        }
        if (affPass) {
            if (affSize) {
                ret += ", ";
            }
            ret += "p=" + to_string(s->getPassage());
        }
        if (affDegin) {
            if (affSize || affPass) {
                ret += ", ";
            }
            ret += "in=" + to_string(s->getDegresIn());
        }
        if (affDegout) {
            if (affSize || affPass || affDegin) {
                ret += ", ";
            }
            ret += "out=" + to_string(s->getDegresOut());
        }

        ret += "]";
    }
    return ret;
}

void QPSelections::writeDot() {
    // 1. Ouvrir le fichier
    ofstream fileDOT(
        "./out/" +
        ((QLineEdit *)(getWidgetWithName("dowName")))->text().toStdString() +
        ".dot");
    if (!fileDOT.is_open()) {
        warning("Impossible d'ouvrir ou creer le fichier dot");
        return;
    }
    // 2. Prendre l'union des sélections choisies
    vector<SommetOriente> unionS{}, finalS{};
    for (string select : selected) {
        set_union(unionS.cbegin(), unionS.cend(),
                  db->getSelection(select).cbegin(),
                  db->getSelection(select).cend(), back_inserter(finalS));
        unionS = finalS;
        finalS.clear();
    }
    // 3. Prendre les sommets à bonne distance
    int distance = ((QSpinBox *)(getWidgetWithName("dowDist")))->value();
    bool typeOfDistanceSommets =
        ((QCheckBox *)(getWidgetWithName("dowBSommets")))->isChecked();
    bool separerSens =
        ((QRadioButton *)(getWidgetWithName("dowSens")))->isChecked();
    if (typeOfDistanceSommets) {
        cout << distance << endl;
        finalS = db->getSubDirectedGraphRange(unionS, distance, separerSens);

    } else {
        finalS = db->getSubDirectedGraphDist(unionS, distance, separerSens);
    }
    // 4. Regroupper les sommets choisis par identifiant si précisé
    vector<int> finalSWithoutSens{};
    if (!separerSens) {
        for (SommetOriente sommet : finalS) {
            if (std::find(finalSWithoutSens.begin(), finalSWithoutSens.end(),
                          sommet.first) == finalSWithoutSens.end()) {
                finalSWithoutSens.push_back(sommet.first);
            }
        }
    }
    // 4. Ecrire le fichier
    string plus{}, minus{};
    bool affName =
        ((QCheckBox *)(getWidgetWithName("dowAffName")))->isChecked();
    bool affSize =
        ((QCheckBox *)(getWidgetWithName("dowAffSize")))->isChecked();
    bool affPass =
        ((QCheckBox *)(getWidgetWithName("dowAffPass")))->isChecked();
    bool affDegin =
        ((QCheckBox *)(getWidgetWithName("dowAffDegin")))->isChecked();
    bool affDegout =
        ((QCheckBox *)(getWidgetWithName("dowAffDegout")))->isChecked();

    fileDOT << "digraph{" << endl;
    if (!separerSens) {
        for (int idSommet : finalSWithoutSens) {
            // Ajouter sommet
            fileDOT << "    " << to_string(idSommet) << " [label=\""
                    << label(idSommet, affName, affSize, affPass, affDegin,
                             affDegout)
                    << "\""
                    << (((std::find(unionS.begin(), unionS.end(),
                                    SommetOriente(idSommet, '+')) !=
                          unionS.end()) ||
                         (std::find(unionS.begin(), unionS.end(),
                                    SommetOriente(idSommet, '-')) !=
                          unionS.end()))
                            ? "shape=\"diamond\";color=\"gold\""
                            : "")
                    << "];" << endl;
            // Ajouter voisins
            for (SommetOriente voisin :
                 db->gL().getAccesible(SommetOriente(idSommet, '+'))) {
                if ((std::find(finalSWithoutSens.begin(),
                               finalSWithoutSens.end(),
                               voisin.first) != finalSWithoutSens.end())) {
                    if (voisin.second == '+') {
                        plus += " " + to_string(voisin.first);
                    } else {
                        minus += " " + to_string(voisin.first);
                    }
                }
            }
            if (plus != "") {
                fileDOT << "    " << idSommet << " -> {" << plus
                        << "} [color=\"red;0.5:red\"];" << endl;
            }
            if (minus != "") {
                fileDOT << "    " << idSommet << " -> {" << minus
                        << "} [color=\"red;0.5:blue\"];" << endl;
            }
            plus.clear();
            minus.clear();
            for (SommetOriente voisin :
                 db->gL().getAccesible(SommetOriente(idSommet, '-'))) {
                if ((std::find(finalSWithoutSens.begin(),
                               finalSWithoutSens.end(),
                               voisin.first) != finalSWithoutSens.end())) {
                    if (voisin.second == '+') {
                        plus += " " + to_string(voisin.first);
                    } else {
                        minus += " " + to_string(voisin.first);
                    }
                }
            }
            if (plus != "") {
                fileDOT << "    " << idSommet << " -> {" << plus
                        << "} [color=\"blue;0.5:red\"];" << endl;
            }
            if (minus != "") {
                fileDOT << "    " << idSommet << " -> {" << minus
                        << "} [color=\"blue;0.5:blue\"];" << endl;
            }
            plus.clear();
            minus.clear();
        }
    } else {
        for (SommetOriente sommet : finalS) {
            //Ajouter sommet
            fileDOT << "    \"" << to_string(sommet.first) << sommet.second
                    << "\" [label=\""
                    << label(sommet.first, affName, affSize, affPass, affDegin,
                             affDegout, true, sommet.second)
                    << "\""
                    << (((std::find(unionS.begin(), unionS.end(), sommet) !=
                          unionS.end()))
                            ? "shape=\"diamond\";color=\"gold\""
                            : "")
                    << "];" << endl;
            //Ajouter voisins
            for (SommetOriente voisin : db->gL().getAccesible(sommet)) {
                if ((std::find(finalS.begin(), finalS.end(), voisin) !=
                     finalS.end())) {
                    if (voisin.second == '+') {
                        plus += " \"" + to_string(voisin.first) +
                                voisin.second + "\"";
                    } else {
                        minus += " \"" + to_string(voisin.first) +
                                 voisin.second + "\"";
                    }
                }
            }
            if (plus != "") {
                fileDOT << "    \"" << to_string(sommet.first) << sommet.second
                        << "\" -> {" << plus << "} [color=\""
                        << (sommet.second == '+' ? "red" : "blue")
                        << ";0.5:red\"];" << endl;
            }
            if (minus != "") {
                fileDOT << "    \"" << to_string(sommet.first) << sommet.second
                        << "\" -> {" << minus << "} [color=\""
                        << (sommet.second == '+' ? "red" : "blue")
                        << ";0.5:blue\"];" << endl;
            }
            plus.clear();
            minus.clear();
        }
    }
    fileDOT << "}";
}