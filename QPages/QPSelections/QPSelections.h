/**
 * @file QPSelections.h
 * @author Nael BB
 * @brief QPage de gestion des séléctions (manipulation, supression ...) et de création des sous-graphes
 */
#ifndef SELECTIONS_APP_QT_
#define SELECTIONS_APP_QT_
#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include "qgridlayout.h"
#include <QGridLayout>
/**
 * @brief QPage de gestion des séléctions (manipulation, supression ...) et de création des sous-graphes
 * 
 */
class QPSelections : public IQPages {
    Q_OBJECT
    vector<string> selected{};
    QGridLayout * selectionsLayout;

  public:
   /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPSelections(QWidget *parent = 0);
    /**
     * @brief Methode utilisée par QPSelections::writeDot pour générer les labels des sommets du graphe
     * 
     * @param id 
     * @param affName 
     * @param affSize 
     * @param affPass 
     * @param affDegin 
     * @param affDegout 
     * @param affSens 
     * @param sens 
     * @return string 
     */
    string label(int id, bool affName, bool affSize, bool affPass, bool affDegin, bool affDegout, bool affSens = false, char sens=' ');
  public slots:
    void goHere(int mode = MODE_RESTART);
    /**
     * @brief Supprime toutes les sélections séléctionnées
     * 
     */
    void destroySelectedSelections();
    /**
     * @brief Vide l'affichage des sélections (pour le reremplir)
     * 
     */
    void clearSelectionLayout();
    /**
     * @brief Crée une nouvelle sélection qui est l'union de celles choisies
     * 
     */
    void unionSelectedSelections();
    /**
     * @brief Crée une nouvelle sélection qui est l'intersection de celles choisies
     * 
     */
    void intersectionSelectedSelections();
    /**
     * @brief Ajoute une sélection dans celles cochées
     * 
     * @param nom 
     */
    void addSelect(const string & nom);
    /**
     * @brief Retire une sélection de celles choisies
     * 
     * @param nom 
     */
    void removeSelect(const string & nom);
    /**
     * @brief Transforme les sélections choisies en sous-graphe, en réspectant les parametres
     *
     * Attention, si un graphe vide est généré, c'est qu'aucune sélection ou que seulement des sélections vide sont choisies
     * 
     */
    void writeDot();

  signals:
    void goBack(int window);
    void openNew(int window, PageId type);
};
#endif