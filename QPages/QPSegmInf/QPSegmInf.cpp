#include "QPSegmInf.h"
#include "QForms/QForms.h"
#include "librairies/StatsExtention/statsExtention.h"
#include "librairies/StringExtention/stringExtention.h"
#include "librairies/warning/warning.h"
#include "qlabel.h"
#include "qpushbutton.h"

// cette fonction sert a extraire le minimum et maximum des dictionnaires de db
bool sort_compare_SegmInf(pair<long, long> i, pair<long, long> j) {
    return i.first < j.first;
}

QPSegmInf::QPSegmInf(QWidget *parent) {
    QVBoxLayout *page = new QVBoxLayout(this);
    //*Header
    {
        QHBoxLayout *header = new QHBoxLayout();
        // Bouton de retour en ariere
        {
            QPushButton *ret = new QPushButton("Retour", parent);
            header->addWidget(ret);
            connect(ret, &QPushButton::clicked,
                    [=]() { emit goBack(myWindow); });
        }
        // Titre
        {
            QLabel *nom = new QLabel("<U>Informations générales</U>");
            header->addWidget(nom);
            addNameToWidget("nom", nom);
        }
        page->addLayout(header);
    }
    //*Nombre de sommets
    {
        QHBoxLayout *nb = new QHBoxLayout();
        // Label nombre de sommets
        nb->addWidget(new QLabel("Nombre de sommets"));
        // Affichage nombre de sommets
        {
            this->addNameToWidget("nb", new QLabel("---"));
            nb->addWidget(this->getWidgetWithName("nb"));
        }
        page->addLayout(nb);
    }

    //*Taille des sommets
    {
        QHBoxLayout *taille = new QHBoxLayout();
        // Grille de tailles
        {
            QGridLayout *tailleGrid = new QGridLayout();
            // head
            tailleGrid->addWidget(new QLabel("<b>Taille (chaine)</b>"), 0, 0);
            // min
            {
                tailleGrid->addWidget(new QLabel("Minimum"), 1, 0);
                this->addNameToWidget("tailleMin", new QLabel("---"));
                tailleGrid->addWidget(this->getWidgetWithName("tailleMin"), 1,
                                      1);
            }
            // max
            {
                tailleGrid->addWidget(new QLabel("Maximum"), 2, 0);
                this->addNameToWidget("tailleMax", new QLabel("---"));
                tailleGrid->addWidget(this->getWidgetWithName("tailleMax"), 2,
                                      1);
            }
            // avg
            {
                tailleGrid->addWidget(new QLabel("Moyenne"), 3, 0);
                this->addNameToWidget("tailleAvg", new QLabel("---"));
                tailleGrid->addWidget(this->getWidgetWithName("tailleAvg"), 3,
                                      1);
            }
            // std
            {
                tailleGrid->addWidget(new QLabel("Ecart type"), 4, 0);
                this->addNameToWidget("tailleStd", new QLabel("---"));
                tailleGrid->addWidget(this->getWidgetWithName("tailleStd"), 4,
                                      1);
            }
            // 25%
            {
                tailleGrid->addWidget(new QLabel("Premier Quartile"), 5, 0);
                this->addNameToWidget("taille25", new QLabel("---"));
                tailleGrid->addWidget(this->getWidgetWithName("taille25"), 5,
                                      1);
            }
            // median
            {
                tailleGrid->addWidget(new QLabel("Mediane"), 6, 0);
                this->addNameToWidget("tailleMed", new QLabel("---"));
                tailleGrid->addWidget(this->getWidgetWithName("tailleMed"), 6,
                                      1);
            }
            // 75%
            {
                tailleGrid->addWidget(new QLabel("Troisieme quartile"), 7, 0);
                this->addNameToWidget("taille75", new QLabel("---"));
                tailleGrid->addWidget(this->getWidgetWithName("taille75"), 7,
                                      1);
            }
            taille->addLayout(tailleGrid);
        }
        // Formulaire de téléchargement :
        {
            QForms *tailleForm = new QForms();
            tailleForm->addLineEdit("nomFichier", "Nom du fichier", 0,
                                    "out/tailles.csv");
            tailleForm->addConfirmButton("ok", "Télécharger la répartition");
            this->addNameToWidget("tailleForm", tailleForm);
            connect(tailleForm, SIGNAL(endForm()), this,
                    SLOT(downloadTailles()));
            taille->addWidget(tailleForm);
        }
        page->addLayout(taille);
    }
    //*Passage des chemins dans les sommets
    {
        QHBoxLayout *passage = new QHBoxLayout();
        // Grille de passage
        {
            QGridLayout *passageGrid = new QGridLayout();
            // head
            {
                passageGrid->addWidget(new QLabel("<b>Nombre de passages</b>"),
                                       0, 0);
            }
            // min
            {
                passageGrid->addWidget(new QLabel("Minimum"), 1, 0);
                this->addNameToWidget("passageMin", new QLabel("---"));
                passageGrid->addWidget(this->getWidgetWithName("passageMin"), 1,
                                       1);
            }
            // max
            {
                passageGrid->addWidget(new QLabel("Maximum"), 2, 0);
                this->addNameToWidget("passageMax", new QLabel("---"));
                passageGrid->addWidget(this->getWidgetWithName("passageMax"), 2,
                                       1);
            }
            // avg
            {
                passageGrid->addWidget(new QLabel("Moyenne"), 3, 0);
                this->addNameToWidget("passageAvg", new QLabel("---"));
                passageGrid->addWidget(this->getWidgetWithName("passageAvg"), 3,
                                       1);
            }
            // std
            {
                passageGrid->addWidget(new QLabel("Ecart type"), 4, 0);
                this->addNameToWidget("passageStd", new QLabel("---"));
                passageGrid->addWidget(this->getWidgetWithName("passageStd"), 4,
                                       1);
            }
            // 25%
            {
                passageGrid->addWidget(new QLabel("Premier Quartile"), 5, 0);
                this->addNameToWidget("passage25", new QLabel("---"));
                passageGrid->addWidget(this->getWidgetWithName("passage25"), 5,
                                       1);
            }
            // median
            {
                passageGrid->addWidget(new QLabel("Mediane"), 6, 0);
                this->addNameToWidget("passageMed", new QLabel("---"));
                passageGrid->addWidget(this->getWidgetWithName("passageMed"), 6,
                                       1);
            }
            // 75%
            {
                passageGrid->addWidget(new QLabel("Troisieme quartile"), 7, 0);
                this->addNameToWidget("passage75", new QLabel("---"));
                passageGrid->addWidget(this->getWidgetWithName("passage75"), 7,
                                       1);
            }
            passage->addLayout(passageGrid);
        }
        // Formulaire de téléchargement :
        {
            QForms *passageForm = new QForms();
            passageForm->addLineEdit("nomFichier", "Nom du fichier", 0,
                                     "out/passages.csv");
            passageForm->addConfirmButton("ok", "Télécharger la répartition");
            this->addNameToWidget("passageForm", passageForm);
            connect(passageForm, SIGNAL(endForm()), this,
                    SLOT(downloadPassages()));
            passage->addWidget(passageForm);
        }
        page->addLayout(passage);
    }
    //*Degres des sommets
    {
        QHBoxLayout *degres = new QHBoxLayout();
        // Grille de degres
        {
            QGridLayout *degresGrid = new QGridLayout();
            // head
            {
                degresGrid->addWidget(new QLabel("<b>Degres des sommets</b>"),
                                      0, 0);
                degresGrid->addWidget(new QLabel("Entrant"), 0, 1);
                degresGrid->addWidget(new QLabel("Sortant"), 0, 2);
                degresGrid->addWidget(new QLabel("Total"), 0, 3);
            }
            // min
            {
                degresGrid->addWidget(new QLabel("Minimum"), 1, 0);
                this->addNameToWidget("degresMinIn", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMinIn"), 1,
                                      1);
                this->addNameToWidget("degresMinOut", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMinOut"),
                                      1, 2);
                this->addNameToWidget("degresMinTot", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMinTot"),
                                      1, 3);
            }
            // max
            {
                degresGrid->addWidget(new QLabel("Maximum"), 2, 0);
                this->addNameToWidget("degresMaxIn", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMaxIn"), 2,
                                      1);
                this->addNameToWidget("degresMaxOut", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMaxOut"),
                                      2, 2);
                this->addNameToWidget("degresMaxTot", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMaxTot"),
                                      2, 3);
            }
            // avg
            {
                degresGrid->addWidget(new QLabel("Moyenne"), 3, 0);
                this->addNameToWidget("degresAvgIn", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresAvgIn"), 3,
                                      1);
                this->addNameToWidget("degresAvgOut", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresAvgOut"),
                                      3, 2);
                this->addNameToWidget("degresAvgTot", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresAvgTot"),
                                      3, 3);
            }
            // Ecart type
            {
                degresGrid->addWidget(new QLabel("Ecart type"), 4, 0);
                this->addNameToWidget("degresStdIn", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresStdIn"), 4,
                                      1);
                this->addNameToWidget("degresStdOut", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresStdOut"),
                                      4, 2);
                this->addNameToWidget("degresStdTot", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresStdTot"),
                                      4, 3);
            }
            // 25%
            {
                degresGrid->addWidget(new QLabel("Premier quartile"), 5, 0);
                this->addNameToWidget("degres25In", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degres25In"), 5,
                                      1);
                this->addNameToWidget("degres25Out", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degres25Out"), 5,
                                      2);
                this->addNameToWidget("degres25Tot", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degres25Tot"), 5,
                                      3);
            }
            // Mediane
            {
                degresGrid->addWidget(new QLabel("Mediane"), 6, 0);
                this->addNameToWidget("degresMedIn", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMedIn"), 6,
                                      1);
                this->addNameToWidget("degresMedOut", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMedOut"),
                                      6, 2);
                this->addNameToWidget("degresMedTot", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degresMedTot"),
                                      6, 3);
            }
            // 75%
            {
                degresGrid->addWidget(new QLabel("Troisieme quartile"), 7, 0);
                this->addNameToWidget("degres75In", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degres75In"), 7,
                                      1);
                this->addNameToWidget("degres75Out", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degres75Out"), 7,
                                      2);
                this->addNameToWidget("degres75Tot", new QLabel("---"));
                degresGrid->addWidget(this->getWidgetWithName("degres75Tot"), 7,
                                      3);
            }
            degres->addLayout(degresGrid);
        }
        // Formulaire de téléchargement :
        {
            QForms *degresForm = new QForms();
            {
                degresForm->addLineEdit("nomFichier", "Nom du fichier", 0,
                                        "out/degres.csv");
                degresForm->addConfirmButton("ok",
                                             "Télécharger la répartition");
                this->addNameToWidget("degresForm", degresForm);
                connect(degresForm, SIGNAL(endForm()), this,
                        SLOT(downloadDegres()));
                degres->addWidget(degresForm);
            }
            page->addLayout(degres);
        }
    }
    // Tout telecharger :
    {
        QForms *infosForm = new QForms();
        {
            infosForm->addLineEdit("nomFichier", "Nom du fichier", 0,
                                    "out/infos.csv");
            infosForm->addConfirmButton("ok",
                                            "Télécharger les informations sur les sommets");
            this->addNameToWidget("infosForm", infosForm);
            connect(infosForm, SIGNAL(endForm()), this,
                    SLOT(downloadInfos()));
            page->addWidget(infosForm);
        }
        QPushButton *all = new QPushButton("Tout télécharger");
        {
            connect(all, &QPushButton::clicked, [=]() {
                downloadTailles();
                downloadPassages();
                downloadDegres();
                downloadInfos();
            });
        }
        page->addWidget(all);
    }
};

void QPSegmInf::goBack() { emit goBack(myWindow); }

void QPSegmInf::goHere(int mode) {
    ((QLabel *)(this->getWidgetWithName("nom")))->setText(db->nomFichier);
    if (mode == MODE_RESTART) { // Dans le cas ou doit restart
        // Initialisation des variables "sum"
        string basename = string(split(
            split(db->nomFichier.toStdString(),
                  '/')[split(db->nomFichier.toStdString(), '/').size() - 1],
            '.')[0]);
        ((QLineEdit *)(((QForms *)(this->getWidgetWithName("tailleForm")))
                           ->getWidget("nomFichier")))
            ->setText(
                QString::fromStdString("out/tailles" + basename + ".csv"));
        ((QLineEdit *)(((QForms *)(this->getWidgetWithName("passageForm")))
                           ->getWidget("nomFichier")))
            ->setText(
                QString::fromStdString("out/passages" + basename + ".csv"));
        ((QLineEdit *)(((QForms *)(this->getWidgetWithName("degresForm")))
                           ->getWidget("nomFichier")))
            ->setText(QString::fromStdString("out/degres" + basename + ".csv"));
        ((QLineEdit *)(((QForms *)(this->getWidgetWithName("infosForm")))
                           ->getWidget("nomFichier")))
            ->setText(QString::fromStdString("out/infos" + basename + ".csv"));
        int nbSom = 0;
        if (!db->sommetsVide()) { //*Si la db est remplie ; on calcule les
                                  // variables "sum" pour faire les moyennes
            information("Page déjà accédée, recalcul évité");
            nbSom = db->gS().size();
        } else { //*Si elle n'est pas remplie, les "sum" sont alors calculées
                 // pendant le remplissage
            if (db->gS().size() ==
                0) { // On vérifie : s'il n'y a pas de sommets, inutile de
                     // remplir, tout reste à l'initialisation nulle
                ((QLabel *)(this->getWidgetWithName("nb")))
                    ->setText(QString::number(0));
                this->show();
                return;
            } else { // S'il y a des sommets, on remplie la db

                for (InfosSommet som : db->gS().getInfos()) {
                    // Degres In
                    if (db->degresIn.find(som.getDegresIn()) ==
                        db->degresIn.end()) {
                        (db->degresIn)[som.getDegresIn()] = 1;
                    } else {
                        (db->degresIn)[som.getDegresIn()]++;
                    }
                    // Degres Out
                    if (db->degresOut.find(som.getDegresOut()) ==
                        db->degresOut.end()) {
                        (db->degresOut)[som.getDegresOut()] = 1;
                    } else {
                        (db->degresOut)[som.getDegresOut()]++;
                    }
                    // DegresTot
                    if (db->degresTot.find(som.getDegresTot()) ==
                        db->degresTot.end()) {
                        (db->degresTot)[som.getDegresTot()] = 1;
                    } else {
                        (db->degresTot)[som.getDegresTot()]++;
                    }
                    // Passage
                    if (db->passages.find(som.getPassage()) ==
                        db->passages.end()) {
                        (db->passages)[som.getPassage()] = 1;
                    } else {
                        (db->passages)[som.getPassage()]++;
                    }
                    // Tailles
                    if (db->tailles.find(som.getLongueur()) ==
                        db->tailles.end()) {
                        (db->tailles)[som.getLongueur()] = 1;
                    } else {
                        (db->tailles)[som.getLongueur()]++;
                    }

                    nbSom++;
                }
            } // fin du remplissage de la db, les variables sont alors
              // initialisées
        }
        // Arivé ici, la db est remplie, et les "sum" sont calculées
        map<string, float> infDI = getStats(db->degresIn);
        map<string, float> infDO = getStats(db->degresOut);
        map<string, float> infDT = getStats(db->degresTot);
        map<string, float> infTaille = getStats(db->tailles);
        map<string, float> infPass = getStats(db->passages);
        ((QLabel *)(this->getWidgetWithName("nb")))
            ->setText(formatedFloatToQString(nbSom));
        ((QLabel *)(this->getWidgetWithName("tailleMin")))
            ->setText(formatedFloatToQString(infTaille["min"]));
        ((QLabel *)(this->getWidgetWithName("tailleMax")))
            ->setText(formatedFloatToQString(infTaille["max"]));
        ((QLabel *)(this->getWidgetWithName("tailleAvg")))
            ->setText(formatedFloatToQString(infTaille["avg"]));
        ((QLabel *)(this->getWidgetWithName("tailleMed")))
            ->setText(formatedFloatToQString(infTaille["median"]));
        ((QLabel *)(this->getWidgetWithName("taille25")))
            ->setText(formatedFloatToQString(infTaille["25%"]));
        ((QLabel *)(this->getWidgetWithName("taille75")))
            ->setText(formatedFloatToQString(infTaille["75%"]));
        ((QLabel *)(this->getWidgetWithName("tailleStd")))
            ->setText(formatedFloatToQString(infTaille["stdDev"]));

        ((QLabel *)(this->getWidgetWithName("passageMin")))
            ->setText(formatedFloatToQString(infPass["min"]));
        ((QLabel *)(this->getWidgetWithName("passageMax")))
            ->setText(formatedFloatToQString(infPass["max"]));
        ((QLabel *)(this->getWidgetWithName("passageAvg")))
            ->setText(formatedFloatToQString(infPass["avg"]));
        ((QLabel *)(this->getWidgetWithName("passageMed")))
            ->setText(formatedFloatToQString(infPass["median"]));
        ((QLabel *)(this->getWidgetWithName("passage25")))
            ->setText(formatedFloatToQString(infPass["25%"]));
        ((QLabel *)(this->getWidgetWithName("passage75")))
            ->setText(formatedFloatToQString(infPass["75%"]));
        ((QLabel *)(this->getWidgetWithName("passageStd")))
            ->setText(formatedFloatToQString(infPass["stdDev"]));

        ((QLabel *)(this->getWidgetWithName("degresMinIn")))
            ->setText(formatedFloatToQString(infDI["min"]));
        ((QLabel *)(this->getWidgetWithName("degresMinOut")))
            ->setText(formatedFloatToQString(infDO["min"]));
        ((QLabel *)(this->getWidgetWithName("degresMinTot")))
            ->setText(formatedFloatToQString(infDT["min"]));

        ((QLabel *)(this->getWidgetWithName("degresMaxIn")))
            ->setText(formatedFloatToQString(infDI["max"]));
        ((QLabel *)(this->getWidgetWithName("degresMaxOut")))
            ->setText(formatedFloatToQString(infDO["max"]));
        ((QLabel *)(this->getWidgetWithName("degresMaxTot")))
            ->setText(formatedFloatToQString(infDT["max"]));

        ((QLabel *)(this->getWidgetWithName("degresAvgIn")))
            ->setText(formatedFloatToQString(infDI["avg"]));
        ((QLabel *)(this->getWidgetWithName("degresAvgOut")))
            ->setText(formatedFloatToQString(infDO["avg"]));
        ((QLabel *)(this->getWidgetWithName("degresAvgTot")))
            ->setText(formatedFloatToQString(infDT["avg"]));

        ((QLabel *)(this->getWidgetWithName("degresMedIn")))
            ->setText(formatedFloatToQString(infDI["median"]));
        ((QLabel *)(this->getWidgetWithName("degresMedOut")))
            ->setText(formatedFloatToQString(infDO["median"]));
        ((QLabel *)(this->getWidgetWithName("degresMedTot")))
            ->setText(formatedFloatToQString(infDT["median"]));

        ((QLabel *)(this->getWidgetWithName("degres75In")))
            ->setText(formatedFloatToQString(infDI["75%"]));
        ((QLabel *)(this->getWidgetWithName("degres75Out")))
            ->setText(formatedFloatToQString(infDO["75%"]));
        ((QLabel *)(this->getWidgetWithName("degres75Tot")))
            ->setText(formatedFloatToQString(infDT["75%"]));

        ((QLabel *)(this->getWidgetWithName("degres25In")))
            ->setText(formatedFloatToQString(infDI["25%"]));
        ((QLabel *)(this->getWidgetWithName("degres25Out")))
            ->setText(formatedFloatToQString(infDO["25%"]));
        ((QLabel *)(this->getWidgetWithName("degres25Tot")))
            ->setText(formatedFloatToQString(infDT["25%"]));

        ((QLabel *)(this->getWidgetWithName("degresStdIn")))
            ->setText(formatedFloatToQString(infDI["stdDev"]));
        ((QLabel *)(this->getWidgetWithName("degresStdOut")))
            ->setText(formatedFloatToQString(infDO["stdDev"]));
        ((QLabel *)(this->getWidgetWithName("degresStdTot")))
            ->setText(formatedFloatToQString(infDT["stdDev"]));
    }
    this->show();
}