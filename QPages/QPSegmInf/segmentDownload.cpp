#include "QForms/QForms.h"
#include "QPSegmInf.h"
#include "librairies/warning/warning.h"

void QPSegmInf::downloadDegres() {
    downloadDegres(((QForms *)(this->getWidgetWithName("degresForm")))
                       ->getStringForm("nomFichier"));
    ((QForms *)(this->getWidgetWithName("degresForm")))->enable();
};
void QPSegmInf::downloadTailles() {
    downloadTailles(((QForms *)(this->getWidgetWithName("tailleForm")))
                        ->getStringForm("nomFichier"));
    ((QForms *)(this->getWidgetWithName("tailleForm")))->enable();
};
void QPSegmInf::downloadPassages() {
    downloadPassages(((QForms *)(this->getWidgetWithName("passageForm")))
                         ->getStringForm("nomFichier"));
    ((QForms *)(this->getWidgetWithName("passageForm")))->enable();
};
void QPSegmInf::downloadInfos() {
    downloadInfos(((QForms *)(this->getWidgetWithName("infosForm")))
                         ->getStringForm("nomFichier"));
    ((QForms *)(this->getWidgetWithName("infosForm")))->enable();
};

void QPSegmInf::downloadDegres(QString nomFichier) {
    ofstream fileCSV(nomFichier.toStdString());
    if (!fileCSV.is_open()) {
        warning("Impossible d'ouvrir ou creer le fichier csv");
        return;
    }
    fileCSV << "Degres,NombreEntrant,NombreSortant,NombreTotal" << endl;
    for (int i = 0;
         i <=
         ((QLabel *)(this->getWidgetWithName("degresMaxTot")))->text().toInt();
         i++) {
        if (db->degresIn[i] != 0 || db->degresOut[i] != 0 ||
            (db->degresTot[i] != 0) ){
            fileCSV << i << "," << db->degresIn[i] << ","
                    << db->degresOut[i] << "," << db->degresTot[i]
                    << endl;
        }
    }
};
void QPSegmInf::downloadTailles(QString nomFichier) {
    ofstream fileCSV(nomFichier.toStdString());
    if (!fileCSV.is_open()) {
        warning("Impossible d'ouvrir ou creer le fichier csv");
        return;
    }
    fileCSV << "Taille,Nombre" << endl;
    for (auto const &[key, value] : db->tailles) {
        fileCSV << key << "," << value << endl;
    }
    fileCSV.close();
};
void QPSegmInf::downloadPassages(QString nomFichier) {
    ofstream fileCSV(nomFichier.toStdString());
    if (!fileCSV.is_open()) {
        warning("Impossible d'ouvrir ou creer le fichier csv");
        return;
    }
    fileCSV << "Passage,Nombre" << endl;
    for (auto const &[key, value] : db->passages) {
        fileCSV << key << "," << value << endl;
    }
    fileCSV.close();
};
void QPSegmInf::downloadInfos(QString nomFichier) {
    ofstream fileCSV(nomFichier.toStdString());
    if (!fileCSV.is_open()) {
        warning("Impossible d'ouvrir ou creer le fichier csv");
        return;
    }
    fileCSV << "NomSommet,Taille,Passage,DegresIn,DegresOut,DegresTotal" << endl;
    int i = 0;
    for (auto sommet : db->gS().getInfos()) {
        fileCSV << db->gS().getNameSommet(i) << "," << sommet.getLongueur() << "," << sommet.getPassage() << "," << sommet.getDegresIn() << "," << sommet.getDegresOut() << "," << sommet.getDegresTot() << endl;
        i++;
    }
    fileCSV.close();
};