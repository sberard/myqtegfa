/**
 * @file QPSegmInf.h
 * @author Nael BB
 * @brief QPage permetant d'afficher et télécharger des statistiques sur les segments
 */
#ifndef SEGM_INF_APP_QT_
#define SEGM_INF_APP_QT_
#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include <QGridLayout>

bool sort_compare_SegmInf(pair<long, long> i, pair<long, long> j);
/**
 * @brief QPage permetant d'afficher et télécharger des statistiques sur les segments
 * @todo Permetre de voir un histogramme de la répartition dirrectement depuis l'application
 */
class QPSegmInf : public IQPages {
    Q_OBJECT
  public:
  /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPSegmInf(QWidget *parent = 0);
    /**
     * @brief Télécharge la répartition des degrés en csv
     * 
     * @param nomFichier 
     */
    void downloadDegres(QString nomFichier);
    /**
     * @brief Télécharge la répartition des tailles en csv
     * 
     * @param nomFichier 
     */
    void downloadTailles(QString nomFichier);
    /**
     * @brief Télécharge la répartition des passages en csv
     * 
     * @param nomFichier 
     */
    void downloadPassages(QString nomFichier);
    /**
     * @brief Télécharge les informations sur les sommets en csv
     * 
     * @param nomFichier 
     */
    void downloadInfos(QString nomFichier);
  public slots:
    void goHere(int mode = MODE_RESTART);
    void goBack();
    /**
     * @brief Slot relié à la methode du meme nom
     * @todo transformer en utilisant des lambda-exp pour éviter les problemes de parametrages
     */
    void downloadDegres();
    /**
     * @brief Slot relié à la methode du meme nom
     * @todo transformer en utilisant des lambda-exp pour éviter les problemes de parametrages
     */
    void downloadTailles();
    /**
     * @brief Slot relié à la methode du meme nom
     * @todo transformer en utilisant des lambda-exp pour éviter les problemes de parametrages
     */
    void downloadPassages();
    /**
     * @brief Slot relié à la methode du meme nom
     * @todo transformer en utilisant des lambda-exp pour éviter les problemes de parametrages
     */
    void downloadInfos();
  signals:
    void goBack(int window);
};
#endif