#include "QPMenu.h"
#include "QPages/PageId.h"
#include "class/LinkDataBase/LinkDataBase.h"
#include "qlabel.h"
#include "qpushbutton.h"
QPMenu::QPMenu(QWidget *parent) {
    QVBoxLayout *page = new QVBoxLayout(this);
    QHBoxLayout *header = new QHBoxLayout();
    QGridLayout *contents = new QGridLayout();

    QPushButton *ret = new QPushButton("Retour", parent);
    header->addWidget(ret);
    connect(ret, &QPushButton::clicked, [=](){emit goBack(myWindow);});

    QLabel *nom = new QLabel("---");
    addNameToWidget("nom", nom);
    header->addWidget(nom);

    QPushButton *gi = new QPushButton(QString("Informations générales"));
    contents->addWidget(gi, 0, 0);
    connect(gi, &QPushButton::clicked, [=](){emit goForward(myWindow, PageId::GeneralInfos);});

    QPushButton *si = new QPushButton(QString("Informations sur les sommets"));
    contents->addWidget(si, 1, 0);
    connect(si, &QPushButton::clicked, [=](){emit goForward(myWindow, PageId::SegmInf);});

    QPushButton *ii = new QPushButton(QString("Informations sur les individus"));
    contents->addWidget(ii, 2, 0);
    connect(ii, &QPushButton::clicked, [=](){emit goForward(myWindow, PageId::IndInf);});

    QPushButton *sg = new QPushButton(QString("Générer un sous-graphe"));
    contents->addWidget(sg, 3, 0);
    connect(sg, &QPushButton::clicked, [=](){emit goForward(myWindow, PageId::Selections);});

    page->addLayout(header);
    page->addLayout(contents);
};

void QPMenu::goHere(int mode) {
    if (mode == MODE_RESTART) {
        ((QLabel *)(this->getWidgetWithName("nom")))->setText(db->nomFichier);
    }
    this->show();
};