/**
 * @file QPMenu.h
 * @author Nael BB
 * @brief QPage servant de menu principal une fois le fichier gfa analysé
 */
#ifndef MENU_APP_QT_
#define MENU_APP_QT_
#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include <QGridLayout>
/**
 * @brief QPage servant de menu principal une fois le fichier gfa analysé
 * 
 */
class QPMenu : public IQPages {
    Q_OBJECT
  public:
    /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPMenu(QWidget *parent = 0);
  public slots:
    void goHere(int mode = MODE_RESTART);
};
#endif