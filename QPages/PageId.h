/**
 * @file PageId.h
 * @author Nael BB
 * @brief Enumération des différentes pages dont des liens mènent vers elles, ou qui sont en cours de développement
 */
#ifndef ENUM_PAGE_ID_
#define ENUM_PAGE_ID_

#define MODE_RESTART 0
#define MODE_CONTINUE 1
/**
 * @brief Enumération des différentes pages dont des liens mènent vers elles, ou qui sont en cours de développement
 * 
 */
enum class PageId {
    ChooseFile,
    Menu,
    GeneralInfos,
    SegmInf,
    LinkInf,
    PWInf,
    ErrInf,
    IndInf,
    ContigInf,
    CreateSelection,
    Selections,
    ViewSelection
};
#endif