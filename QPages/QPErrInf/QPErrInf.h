/**
 * @file QPErrInf.h
 * @author Nael BB
 * @brief QPage permetant de visionner les erreurs
 */
#ifndef ERRORS_APP_QT_
#define ERRORS_APP_QT_

#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include <QGridLayout>
#include <QPlainTextEdit>

#define ERREURS_PAR_PAGES 3
/** 
* @brief QPage permetant de visionner les erreurs
*/
class QPErrInf : public IQPages {
    Q_OBJECT
    int position = 0;

  public:
  /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPErrInf(QWidget *parent = 0);
    /**
     * @brief Recharge la partie qui affiche les erreurs en cas de passage d'une page à l'autre
     * 
     */
    void reloadErrors();
  public slots:
  /**
   * @brief Passe aux #ERREURS_PAR_PAGES erreurs d'après
   * 
   */
    void nextErrors();
    /**
   * @brief Passe aux #ERREURS_PAR_PAGES erreurs d'avant
   * 
   */
    void backErrors();
    void goHere(int mode = MODE_RESTART);
};
#endif