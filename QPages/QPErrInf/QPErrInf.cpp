#include "QPErrInf.h"
#include "QPages/PageId.h"
#include "qlabel.h"
#include "qpushbutton.h"
QPErrInf::QPErrInf(QWidget *parent) {
    QVBoxLayout *page = new QVBoxLayout(this);

    //*Header
    QHBoxLayout *header = new QHBoxLayout();
    // Bouton de retour en ariere
    QPushButton *ret = new QPushButton("Retour", parent);
    header->addWidget(ret);
    connect(ret, &QPushButton::clicked, [=](){emit goBack(myWindow);});
    // Titre
    QLabel *nom = new QLabel("<U>Informations générales</U>");
    header->addWidget(nom);
    addNameToWidget("nom", nom);
    page->addLayout(header);
    //*Boite d'erreurs
    QPlainTextEdit *erreurs = new QPlainTextEdit(this);
    this->addNameToWidget("txt", erreurs);
    erreurs->setReadOnly(true);
    page->addWidget(erreurs);
    //*Boutons
    QHBoxLayout *boutons = new QHBoxLayout();
    // Arière
    QPushButton *arr = new QPushButton("<", parent);
    this->addNameToWidget("back", arr);
    boutons->addWidget(arr);
    connect(arr, &QPushButton::clicked, [=](){backErrors();});
    // Avant
    QPushButton *av = new QPushButton(">", parent);
    this->addNameToWidget("next", av);
    boutons->addWidget(av);
    connect(av, &QPushButton::clicked, [=](){nextErrors();});

    page->addLayout(boutons);
};
void QPErrInf::nextErrors() {
    position += 1;
    if (((long)(ERREURS_PAR_PAGES * position + ERREURS_PAR_PAGES)) >=
        (long)db-> gE().size()) {
        getWidgetWithName("next")->hide();
    }
    getWidgetWithName("back")->show();
    reloadErrors();
};
void QPErrInf::backErrors() {
    position -= 1;
    if (position == 0) {
        getWidgetWithName("back")->hide();
    }
    getWidgetWithName("next")->show();
    reloadErrors();
};
void QPErrInf::goHere(int mode) {
    ((QLabel *)(this->getWidgetWithName("nom")))->setText(db->nomFichier);
    if (mode == MODE_RESTART) {
        position = 0;
    }
    this->show();
    getWidgetWithName("back")->hide();
    if ((long)db-> gE().size() <= (long)ERREURS_PAR_PAGES) {
        getWidgetWithName("next")->hide();
    }
    reloadErrors();
};
void QPErrInf::reloadErrors() {
    QPlainTextEdit *label = (QPlainTextEdit *)this->getWidgetWithName("txt");
    label->clear();
    if (db-> gE().size()) {
        label->appendHtml(
            "Erreurs " + QString::number(position * ERREURS_PAR_PAGES + 1) +
            " à " +
            QString::number(
                min((long)(position * ERREURS_PAR_PAGES + ERREURS_PAR_PAGES),
                    (long)db-> gE().size())) +
            " sur " + QString::number(db-> gE().size()) + ":<hr>");
        for (int i = position * ERREURS_PAR_PAGES;
             i < min((long)(position * ERREURS_PAR_PAGES + ERREURS_PAR_PAGES),
                     (long)db-> gE().size());
             i++) {
            pair<long, string> error = db->gE()[i];
            label->appendHtml("");
            if (error.first != -1) {
                label->appendHtml("Ligne <span style=\" color:#ff0000;\">" +
                                  QString::number(error.first) + "</span> : " +
                                  QString::fromStdString(error.second));
            } else {
                label->appendHtml(QString::fromStdString(error.second));
            }
        }
    } else {
        label->appendHtml("Pas d'erreurs trouvées");
    }
}