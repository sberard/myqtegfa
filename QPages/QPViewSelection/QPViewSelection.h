/**
 * @file QPViewSelection.h
 * @author Nael BB
 * @brief QPage pour afficher le contenu d'une selection
 */

#ifndef SELECTION_VIEW_APP_QT_
#define SELECTION_VIEW_APP_QT_
#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include "class/LinkDataBase/LinkDataBase.h"
#include <QGridLayout>
/**
 * @brief QPage pour afficher le contenu d'une selection
 * @todo essayer d'accelerer l'affichage pour une grosse sélection
 */
class QPViewSelection : public IQPages {
    Q_OBJECT
    string nomSelection = "--";
    vector<SommetOriente> selection;
  public:
  /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPViewSelection(QWidget *parent = 0);
  public slots:
    void goHere(int mode = MODE_RESTART);

  signals:
    void closeMe(int window);
};
#endif