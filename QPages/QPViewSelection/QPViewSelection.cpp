#include "QPViewSelection.h"
#include "QPages/PageId.h"
#include "class/LinkDataBase/LinkDataBase.h"
#include "librairies/warning/warning.h"
#include "qchar.h"
#include "qlabel.h"
#include "qlineedit.h"
#include "qpushbutton.h"
#include "qtextedit.h"

QPViewSelection::QPViewSelection(QWidget *parent){
    QVBoxLayout *page = new QVBoxLayout(this);
    { //*header
        QHBoxLayout *header = new QHBoxLayout();
        {
            QLabel *nomF = new QLabel("---");
            addNameToWidget("nomF", nomF);
            header->addWidget(nomF);
            QLineEdit *nomS = new QLineEdit("---");
            addNameToWidget("nomS", nomS);
            connect(nomS, &QLineEdit::textChanged, [=](const QString & newValue){
                db->deleteSelection(nomSelection);
                db->addSelection(newValue.toStdString(), this->selection);
                nomSelection = newValue.toStdString();
                reloadPred(myWindow);
            });
            header->addWidget(nomS);
        }
        page->addLayout(header);
    }
    { //*Main
        QTextEdit * main = new QTextEdit();
        main->setReadOnly(true);
        addNameToWidget("main", main);
        page->addWidget(main);
        page->addWidget(new QLabel("Trier l'affichage"));
    }
    { //*Footer
        QHBoxLayout *footer = new QHBoxLayout();
        {
            
            QPushButton *sortIdP = new QPushButton("Nom sommets A-Z");
            connect(sortIdP, &QPushButton::clicked, [=](){
                sort(selection.begin(),selection.end(), [=](SommetOriente a, SommetOriente b){return db->gS().getNameSommet(a.first)>db->gS().getNameSommet(b.first);});
                goHere(MODE_CONTINUE);
            });
            footer->addWidget(sortIdP);
            QPushButton *sortIdM = new QPushButton("Nom sommets Z-A");
            connect(sortIdM, &QPushButton::clicked, [=](){
                sort(selection.begin(),selection.end(), [=](SommetOriente a, SommetOriente b){return db->gS().getNameSommet(a.first)<db->gS().getNameSommet(b.first);});
                goHere(MODE_CONTINUE);
            });
            footer->addWidget(sortIdM);
            QPushButton *sortSensP = new QPushButton("Inversions +-");
            connect(sortSensP, &QPushButton::clicked, [=](){
                sort(selection.begin(),selection.end(), [=](SommetOriente a, SommetOriente b){return (a.second !=b.second?a.second <b.second:a.first<b.first);});
                goHere(MODE_CONTINUE);
            });
            footer->addWidget(sortSensP);
            QPushButton *sortSensM = new QPushButton("Inversions -+");
            connect(sortSensM, &QPushButton::clicked, [=](){
                sort(selection.begin(),selection.end(), [=](SommetOriente a, SommetOriente b){return (a.second !=b.second?a.second >b.second:a.first<b.first);});
                goHere(MODE_CONTINUE);
            });
            footer->addWidget(sortSensM);
        }
        page->addLayout(footer);
    }
};
void QPViewSelection::goHere(int mode){
    if(mode == MODE_RESTART){
        nomSelection = db->selectionToView;
        db->selectionToView = "";
        selection = db->getSelection(nomSelection);
    }
    ((QLabel *)(this->getWidgetWithName("nomF")))->setText(db->nomFichier);
    ((QLineEdit *)(this->getWidgetWithName("nomS")))->setText(QString::fromStdString(nomSelection));
    this->show();
    QTextEdit * main = (QTextEdit*) getWidgetWithName("main");
    QString txt = "";

    for(SommetOriente k : selection){
        txt+= QString::fromStdString(db->gS().getNameSommet(k.first) + k.second + "<br>");
    }
    main->setHtml(txt);

};