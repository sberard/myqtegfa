#include "QPChooseFile.h"
#include "../../QForms/QForms.h"
#include "QFileSelect/QFileSelect.h"
#include "QPages/QPChooseFile/codeAnalyse/collecteFichier.h"
QPChooseFile::QPChooseFile(QWidget *parent) {
    this->form = new QForms();
    form->addFileSelect("file", "*.gfa", " Selectionner un fichier .gfa",
                        " Aucun fichier selectionné ", parent);
    form->addCheckBox("ifCheckErrors", "Detecter les erreurs", false, parent);
    form->addCheckBox("ifGetChaines", "Enregistrer les chaines des sommets", false, parent);
    form->addCheckBox("ifGetPaths", "Enregistrer les chemins du graphe", false, parent);
    form->addConfirmButton("end", "Lancer l'analyse du fichier", parent);

    connect(this->form, SIGNAL(endForm()), this, SLOT(startAnalyse()));

    QVBoxLayout *vbox = new QVBoxLayout(this);
    vbox->addWidget(this->form);
};

void QPChooseFile::goHere(int mode) {
    switch (mode) {
    case MODE_RESTART:{
        this->db->vider();

        ((QCheckBox *)(form->getWidget("ifCheckErrors")))
            ->setCheckState(Qt::Unchecked);
        ((QCheckBox *)(form->getWidget("ifGetChaines")))
            ->setCheckState(Qt::Checked);
        ((QCheckBox *)(form->getWidget("ifGetPaths")))
            ->setCheckState(Qt::Checked);
        ((QFileSelect *)(form->getWidget("file")))
            ->setFileSelect("");
        form->enable();
        this->show();
        break;
    }
    case MODE_CONTINUE: {
        this->db->vider();
        form->enable();
        this->show();
        break;
    }
    }
};

void QPChooseFile::startAnalyse() {
    if (form->getStringForm("file") != "") {
        remplissageBDD(this->db, this->form->getStringForm("file").toStdString(), this->form->getIntForm("ifCheckErrors"),
                       this->form->getIntForm("ifGetChaines"), this->form->getIntForm("ifGetPaths"));
        emit goForward(myWindow, PageId::Menu);
    } else {
        form->enable();
    }
};