/**
 * @file collecteFichier.h
 * @author Nael BB
 * @brief Contient les fonctions qui remplissent la base de données à partir du fichier gfa
 */
#ifndef COLLECTE_FICHIER_
#define COLLECTE_FICHIER_
#include <QCoreApplication>
#include <cstdlib>
#include <string>
#include <vector>
using namespace std;
#include "../../../class/DataBase/DataBase.h"
/**
 * @brief Remplis la base de données avec le fichier gfa donné en entrée. 
 * 
 * Utilise les fonctions #gestionnaireWalkPaths, #gestionnaireSegment et #gestionnaireLink
 * @param db 
 * @param nomFichier 
 * @param checkErrors 
 * @param getChaines 
 * @param getPaths 
 * @return int
 */
int remplissageBDD(DataBase *db, const string & nomFichier, bool checkErrors,
                   bool getChaines, bool savePaths);
/**
 * @brief Prends en entrée une ligne découpée représentant un chemin, et l'enregistre dans la base de données
 * 
 * @param db 
 * @param caracter 
 * @param lineCut 
 * @param noLigne 
 * @param checkErrors 
 * @param savePaths 
 */
void gestionnaireWalkPaths(DataBase *db, char caracter,
                           vector<string_view> lineCut, long noLigne,
                           bool checkErrors, bool savePaths);
/**
 * @brief Prends en entrée une ligne découpée représentant un sommet, et l'enregistre dans la base de données
 * 
 * @param db 
 * @param lineCut 
 * @param noLigne 
 * @param checkErrors 
 */
void gestionnaireSegment(DataBase *db, vector<string_view> lineCut, long noLigne,
                         bool checkErrors);
/**
 * @brief Prends en entrée une ligne découpée représentant un lien, et l'enregistre dans la base de donnée
 * 
 * @param db 
 * @param lineCut 
 * @param noLigne 
 * @param checkErrors 
 */
void gestionnaireLink(DataBase *db, vector<string_view> lineCut, long noLigne,
                      bool checkErrors);
#endif