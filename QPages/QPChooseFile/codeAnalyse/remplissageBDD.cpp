#include "collecteFichier.h"
#include "librairies/StringExtention/stringExtention.h"
#include "librairies/warning/warning.h"

int remplissageBDD(DataBase *db, const string &nomFichier, bool checkErrors,
                   bool getChaines, bool getPaths) {
    ifstream file(nomFichier);
    if (!file.is_open()) {
        cerr << "Erreur : Fichier \"" << nomFichier << "\" impossible à ouvrir"
             << endl;
        return 1;
    }
    db->nomFichier = QString::fromStdString(nomFichier);
    db->checkErrors = checkErrors;
    db->getChaines = getChaines;
    db->getPaths = getPaths;
    string line;
    //* Premier parcours : verification syntaxe premier db caractère, ajout des
    // sommets, comptage des sommets
    long noLigne = 0;
    long ligneHeader = 0;
    long compteLiens = 0;
    long comptePaths = 0;
    long compteWalks = 0;
    long nbSom = 0;
    vector<string_view> lineCut;
    information("Nombre de sommets " + to_string(nbSom));
    //*Deuxième parcours : ajout des sommets
    information("Sommets en cours");
    file.clear();
    file.seekg(0);
    noLigne = 0;
    while (getline(file, line)) {
        if (noLigne % 100000 == 0) {
            QCoreApplication::processEvents();
        }
        noLigne++;
        char caracter = line[0];
        if (checkErrors && (line.length() < 2 || line[1] != '	')) {
            warningLine(noLigne, "L'identifiant de ligne n'est pas reconnu");
            db->addError(noLigne, "L'identifiant de ligne n'est pas reconnu");
        }else{
            switch (caracter) {
            case 'L': {
                compteLiens++;
                break;
            }
            case 'S': {
                nbSom++;
                lineCut = split(line, '	');
                gestionnaireSegment(db, lineCut, noLigne, checkErrors);
                break;
            }
            case 'P': {
                comptePaths++;
                break;
            }
            case 'W': {
                compteWalks++;
                break;
            }
            case 'H': {
                if (!ligneHeader) {
                    ligneHeader = noLigne;
                    lineCut = split(line, '	');
                    if (lineCut.size() >= 2) {
                        db->header = string(lineCut[1]);
                    }
                } else {
                    if (checkErrors) {
                        warningLine(noLigne,
                                    "Attention, présence de plusieurs headers");
                        db->addError(
                            noLigne,
                            "Attention, présence de plusieurs headers");
                    }
                }
                break;
            }
            default: {
                if (checkErrors) {
                    warningLine(noLigne,
                                "L'identifiant de ligne n'est pas reconnu");
                    db->addError(noLigne,
                                 "L'identifiant de ligne n'est pas reconnu");
                }
                break;
            }
            }
        }
        lineCut.clear();
        line.clear();
    }
    if (comptePaths && compteWalks) {
        warning("Présence de walks et de paths en même temps");
        db->addError("Présence de walks et de paths en même temps");
    } else if (comptePaths) {
        db->pathOrWalks = 'P';
        db->nbPW = comptePaths;
    } else if (compteWalks) {
        db->pathOrWalks = 'W';
        db->nbPW = compteWalks;
    } else {
        warning("Ce fichier n'a pas de chemins");
        db->addError("Ce fichier n'a pas de chemins");
        db->nbPW = compteWalks+comptePaths;
    }
    //*Troisième parcours : ajout des liens et des chemins
    information("Liens et chemins en cours");
    file.clear();
    file.seekg(0);
    noLigne = 0;
    while (getline(file, line)) {
        if (noLigne % 100000 == 0) {
            QCoreApplication::processEvents();
        }
        noLigne++;
        char caracter = line[0];
        if (!(checkErrors && (line.length() < 2 || line[1] != '	'))) {
            switch (caracter) {
            case 'L': {
                lineCut = split(line, '	');
                gestionnaireLink(db, lineCut, noLigne, checkErrors);
                break;
            }
            case 'P':
            case 'W': {
                lineCut = split(line, '	');
                gestionnaireWalkPaths(db, caracter, lineCut, noLigne,
                                      checkErrors, getPaths);
                break;
            }
            }
        }
        line.clear();
        lineCut.clear();
    }
    db->nbLiens = compteLiens;
    db->nbLignes = noLigne;
    file.close();
    return 0;
}
