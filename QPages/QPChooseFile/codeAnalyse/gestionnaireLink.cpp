#include "collecteFichier.h"
#include "librairies/warning/warning.h"
void gestionnaireLink(DataBase *db, vector<string_view> lineCut, long noLigne,
                      bool checkErrors){
    //* db->gS() trié par noms
    // lineCut[0] = 'L'
    // lineCut[1] = nomStart
    // lineCut[2] = dirStart
    // lineCut[3] = nomEnd
    // lineCut[4] = dirEnd
    // facultative lineCut[5] = overlap
    if (checkErrors) {
        if (lineCut.size() != 6 && lineCut.size() != 5) {
            warningLine(noLigne, "Nombre d'arguments incorrect");
            db->addError(noLigne, "Nombre d'arguments incorrect");
            return;
        }
    }
    char sensA = lineCut[2][0];
    char sensB = lineCut[4][0];
    string nomA(lineCut[1]);
    string nomB(lineCut[3]);
    if(checkErrors){
        if (lineCut[2].size() != 1 || (sensA != '+' && sensA != '-')) {
            warningLine(noLigne, "Argument 3 incorrect, doit etre '+' ou '-'");
            db->addError(noLigne, "Argument 3 incorrect, doit etre '+' ou '-'");
            return;
        }
        if (lineCut[4].size() != 1 || (sensB != '+' && sensB != '-')) {
            warningLine(noLigne, "Argument 5 incorrect, doit etre '+' ou '-'");
            db->addError(noLigne, "Argument 5 incorrect, doit etre '+' ou '-'");
            return;
        }
    }
    InfosSommet *startDT = db->gS().getSommet(nomA);
    InfosSommet *endDT = db->gS().getSommet(nomB);
    if (startDT == 0) {
        warningLine(
            noLigne,
            "Argument 2 incorrect, doit etre un sommet déclaré plus haut");
        db->addError(
            noLigne,
            "Argument 2 incorrect, doit etre un sommet déclaré plus haut");
        return;
    }
    if (endDT == 0) {
        warningLine(
            noLigne,
            "Argument 4 incorrect, doit etre un sommet déclaré plus haut");
        db->addError(
            noLigne,
            "Argument 4 incorrect, doit etre un sommet déclaré plus haut");
        return;
    }
    endDT->add1DegresIn();
    startDT->add1DegresOut();
    db-> gL().addLink(db-> gS().getIdSommet(nomA), sensA, db-> gS().getIdSommet(nomB), sensB);
    return;
}
