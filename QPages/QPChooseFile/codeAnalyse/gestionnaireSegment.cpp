#include "collecteFichier.h"
#include "librairies/warning/warning.h"
void gestionnaireSegment(DataBase *db, vector<string_view> lineCut, long noLigne,
                         bool checkErrors) {
    // lineCut[0] = 'S'
    // lineCut[1] = nom
    // lineCut[2] = séquence
    if (checkErrors) {
        if (lineCut.size() < 3) {
            warningLine(noLigne, "Nombre d'arguments incorrect");
            db->addError(noLigne, "Nombre d'arguments incorrect");
            return;
        }
    }
    if (db->getChaines) {
        db->gS().addSommet(lineCut[1], lineCut[2]);
    } else {
        db->gS().addSommet(lineCut[1], lineCut[2].size());
    }
    return;
}
