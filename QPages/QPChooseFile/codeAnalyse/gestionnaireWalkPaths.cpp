#include "collecteFichier.h"
#include "librairies/StringExtention/stringExtention.h"
#include "librairies/warning/warning.h"
void gestionnaireWalkPaths(DataBase *db, char caracter,
                           vector<string_view> lineCut, long noLigne,
                           bool checkErrors, bool savePaths) {
    //*Path :
    //-------------------------------------------------------------------------------------------------------------
    // lineCut[0] = 'P'
    // lineCut[1] = individu#idIndividu#contig ou individu.contig
    // lineCut[2] = séquence <<><
    if (caracter == 'P') {
        if (checkErrors) {
            if (lineCut.size() != 4) {
                warningLine(noLigne, "Nombre d'arguments incorrect");
                db->addError(noLigne, "Nombre d'arguments incorrect");
                return;
            }
        }
        vector<string_view> infos = split(lineCut[1], '#');
        if (infos.size() != 3) {
            vector<string_view> infos = split(lineCut[1], '.');
            if (checkErrors) {
                if (infos.size() != 2) {
                    warningLine(noLigne, "Argument 2 incorrect : syntaxe "
                                         "individu#idIndividu#contig ou "
                                         "individu.contig réclamée");
                    db->addError(noLigne, "Argument 2 incorrect : syntaxe "
                                          "individu#idIndividu#contig ou "
                                          "individu.contig réclamée");
                    return;
                }
            }
            db-> gC().ajouterContig(db, infos[0], 0, infos[1], 0,
                                    lineCut[2], 'P', savePaths, noLigne);
            return;
        }
        db-> gC().ajouterContig(db, infos[0], 0, infos[2], 0, lineCut[2],
                                'P', savePaths, noLigne);
    }
    //*Walk :
    //-------------------------------------------------------------------------------------------------------------
    // lineCut[0] = 'W'
    // lineCut[1] = individu
    // lineCut[2] = haplotype
    // lineCut[3] = contig
    // lineCut[4] = startPosition
    // lineCut[5] = endPosition
    // lineCut[6] = séquence 123+,123-
    if (caracter == 'W') {
        if (checkErrors) {
            if (lineCut.size() != 7) {
                warningLine(noLigne, "Nombre d'arguments incorrect");
                db->addError(noLigne, "Nombre d'arguments incorrect");
                return;
            }
            try {
                stoi(lineCut[2].data());
            } catch (std::exception const &e) {
                warningLine(noLigne,
                            "Haplotype impossible à convertir en entier");
                db->addError(noLigne,
                             "Haplotype impossible à convertir en entier");
                return;
            }
            try {
                stoi(lineCut[4].data());
            } catch (std::exception const &e) {
                warningLine(
                    noLigne,
                    "Position de début impossible à convertir en entier");
                db->addError(
                    noLigne,
                    "Position de début impossible à convertir en entier");
                return;
            }
            try {
                stoi(lineCut[5].data());
            } catch (std::exception const &e) {
                warningLine(noLigne,
                            "Position de fin impossible à convertir en entier");
                db->addError(
                    noLigne,
                    "Position de fin impossible à convertir en entier");
                return;
            }
        }
        db-> gC().ajouterContig(db, lineCut[1], stoi(lineCut[2].data()),
                                lineCut[3], stoi(lineCut[4].data()), lineCut[6],
                                'W', savePaths, noLigne);
    }
}
