/**
 * @file QPChooseFile.h
 * @author Nael BB
 * @brief QPage d'entrée du programme, demandant de chosir un fichier
 */
#ifndef FIRST_ANALYSE_QT_
#define FIRST_ANALYSE_QT_
#include "../../QForms/QForms.h"
#include "../IQPages/IQPages.h"
#include <QGridLayout>
/**
 * @brief QPage d'entrée du programme, demandant de chosir un fichier
 * 
 */
class QPChooseFile : public IQPages {
    Q_OBJECT

  public:
  /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPChooseFile(QWidget *parent = 0);
  public slots:
    void goHere(int mode = MODE_RESTART);
    /**
     * @brief Lance l'analyse du fichier, notement la fonction #remplissageBDD
     * 
     */
    void startAnalyse();

  private:
    QForms *form;
};
#endif