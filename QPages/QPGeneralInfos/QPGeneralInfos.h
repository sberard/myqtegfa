/**
 * @file QPGeneralInfos.h
 * @author Nael BB
 * @brief QPage permetant d'afficher les informations générales du fichier
 */
#ifndef GENERAL_INFOS_QT_
#define GENERAL_INFOS_QT_
#include "../IQPages/IQPages.h"
#include "../PageId.h"
#include <QGridLayout>
#include <QtDebug>
/**
 * @brief QPage permetant d'afficher les informations générales du fichier
 */
class QPGeneralInfos : public IQPages {
    Q_OBJECT
    QVBoxLayout *page;

  public:
  /**
     * @brief Prépare la page et ses wigdets
     *
     * @param parent
     */
    QPGeneralInfos(QWidget *parent = 0);
    /**
     * @brief Calcule et affiche les informations
     * 
     */
    void recalcul();
  public slots:
    void goHere(int mode = MODE_RESTART);
};
#endif