#include "QPGeneralInfos.h"
#include "librairies/StringExtention/stringExtention.h"
#include "qlabel.h"
#include "qpushbutton.h"
QPGeneralInfos::QPGeneralInfos(QWidget *parent) {
    page = new QVBoxLayout(this);
    QHBoxLayout *header = new QHBoxLayout();
    QGridLayout *contents = new QGridLayout();

    QPushButton *ret = new QPushButton("Retour", parent);
    header->addWidget(ret);
    connect(ret, &QPushButton::clicked, [=](){emit goBack(myWindow);});

    QLabel *nom = new QLabel("<U>Informations générales</U>");
    header->addWidget(nom);
    addNameToWidget("nom", nom);

    contents->addWidget(new QLabel("Version du fichier"), 0, 0);
    this->addNameToWidget("LVersion", new QLabel("---"));
    contents->addWidget(this->getWidgetWithName("LVersion"), 0, 1);

    contents->addWidget(new QLabel("Nombre de sommets"), 1, 0);
    this->addNameToWidget("LSommets", new QLabel("---"));
    contents->addWidget(this->getWidgetWithName("LSommets"), 1, 1);
    QPushButton *but = new QPushButton("En savoir +", parent);
    connect(but, &QPushButton::clicked, [=](){emit openNew(myWindow, PageId::SegmInf);});
    contents->addWidget(but, 1, 2);

    contents->addWidget(new QLabel("Nombre de Liens"), 2, 0);
    this->addNameToWidget("LLiens", new QLabel("---"));
    contents->addWidget(this->getWidgetWithName("LLiens"), 2, 1);
    /*QPushButton *but1 = new QPushButton("En savoir +", parent);
    connect(but1, &QPushButton::clicked, [=](){emit openNew(myWindow, PageId::LinkInf);});
    contents->addWidget(but1, 2, 2);*/

    contents->addWidget(new QLabel("Nombre d'individus"), 3, 0);
    this->addNameToWidget("LInd", new QLabel("---"));
    contents->addWidget(this->getWidgetWithName("LInd"), 3, 1);
    QPushButton *but3 = new QPushButton("En savoir +", parent);
    connect(but3, &QPushButton::clicked, [=](){emit openNew(myWindow, PageId::IndInf);});

    contents->addWidget(but3, 3, 2);

    this->addNameToWidget("LPWAff", new QLabel("Nombre de paths/walks"));
    contents->addWidget(this->getWidgetWithName("LPWAff"), 4, 0);
    this->addNameToWidget("LPWCount", new QLabel("---"));
    contents->addWidget(this->getWidgetWithName("LPWCount"), 4, 1);
    /*QPushButton *but2 = new QPushButton("En savoir +", parent);
    connect(but2, &QPushButton::clicked, [=](){emit openNew(myWindow, PageId::PWInf);});
    contents->addWidget(but2, 4, 2);*/

    this->addNameToWidget("LErr1", new QLabel("Nombre d'erreurs"));
    contents->addWidget(this->getWidgetWithName("LErr1"), 5, 0);
    this->addNameToWidget("LErrCount", new QLabel("---"));
    contents->addWidget(this->getWidgetWithName("LErrCount"), 5, 1);
    QPushButton *but5 = new QPushButton("En savoir +", parent);
    this->addNameToWidget("LErr2", but5);
    connect(but5, &QPushButton::clicked, [=](){emit openNew(myWindow, PageId::ErrInf);});
    contents->addWidget(but5, 5, 2);

    page->addLayout(header);
    page->addLayout(contents);
};

void QPGeneralInfos::recalcul() {
    ((QLabel *)(this->getWidgetWithName("LVersion")))
        ->setText(QString(this->db->header.c_str()));
    ((QLabel *)(this->getWidgetWithName("LSommets")))
        ->setText(formatedFloatToQString(this->db-> gS().size()));
    ((QLabel *)(this->getWidgetWithName("LLiens")))
        ->setText(formatedFloatToQString(this->db->nbLiens));
    if (db->pathOrWalks == 'P') {
        ((QLabel *)(this->getWidgetWithName("LPWAff")))
            ->setText(QString("Nombre de paths"));
    } else {
        ((QLabel *)(this->getWidgetWithName("LPWAff")))
            ->setText(QString("Nombre de walks"));
    }
    ((QLabel *)(this->getWidgetWithName("LPWCount")))
        ->setText(formatedFloatToQString(this->db->nbPW));
    ((QLabel *)(this->getWidgetWithName("LInd")))
        ->setText(formatedFloatToQString(this->db-> gC().getData().size()));
    ((QLabel *)(this->getWidgetWithName("LErrCount")))
        ->setText(formatedFloatToQString(this->db-> gE().size()));
    this->show();
    if (!db->checkErrors) {
        this->getWidgetWithName("LErr1")->hide();
        this->getWidgetWithName("LErr2")->hide();
        this->getWidgetWithName("LErrCount")->hide();
    } else {
        this->getWidgetWithName("LErr1")->show();
        this->getWidgetWithName("LErr2")->show();
        this->getWidgetWithName("LErrCount")->show();
    }

    return;
}

void QPGeneralInfos::goHere(int mode) {
    ((QLabel *)(this->getWidgetWithName("nom")))->setText(db->nomFichier);
    if (mode != MODE_CONTINUE) {
        this->recalcul();
    }
};