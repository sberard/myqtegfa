/**
 * @file QFileSelect.h
 * @author Nael BB
 * @brief Widget permettant de choisir un fichier qui sera vérifié et dont le nom sera stocké
 */
#ifndef FILE_SELECT_QT_
#define FILE_SELECT_QT_

#include "qdir.h"
#include <QFileDialog>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QString>
#include <QWidget>
using namespace std;
/**
 * @brief Widget permettant de choisir un fichier qui sera vérifié et dont le nom sera stocké
 * 
 */
class QFileSelect : public QWidget {
    Q_OBJECT

  public:
    /**
     * @brief Crée un widget QFileSelect
     * 
     * @param restrictions Réstrictions appliquées au sélecteur : quels fichiers peuvent être séléctionnés
     * @param texteBouton Texte présent sur le bouton qui active la demande de choix d'un fichier
     * @param texteSiVide Texte affiché quand aucun fichier n'est choisi
     * @param parent 
     */
    QFileSelect(QString restrictions = "*.txt",
                QString texteBouton = " Choisir ",
                QString texteSiVide = " Aucun fichier choisi ",
                QWidget *parent = 0);
    /**
     * @brief Retourne vrai si un fichier est choisi, correct ou non
     * 
     * @return true 
     * @return false 
     */
    bool isThereFileSelect();
    /**
     * @brief Renvoie le fichier choisi
     * 
     * @return const QString& 
     */
    const QString& getFileSelect();
    /**
     * @brief Change le fichier choisi
     * 
     * @param nameFile 
     */
    void setFileSelect(const QString &nameFile);

  public slots:
    /**
     * @brief Slot qui lance un essai de récupération de fichier
     * 
     */
    void tryToCatchCorrectFile();

  signals:
  /**
   * @brief Signal renvoyé quand un fichier correct est choisi.
   * 
   * @param nameOfFile Nom du fichier passé
   */
    void whenCatchCorrectFile(QString nameOfFile);
    /**
     * @brief Signal renvoyé quand un fichier incorrect est choisi.
     * 
     */
    void whenCatchIncorrectFile();
    /**
     * @brief Signal partagé quand le bouton est cliqué
     * 
     */
    void clicked();
    /**
     * @brief Signal partagé quand le texte change
     * 
     */
    void textChanged();

  private:
    /**
   * @brief Réstrictions imposées sur le fichier à selectionner
   * 
   * Exemple : "*.txt"
   */
    QString restrictions;
    /**
   * @brief Texte affiché si aucun fichier n'est séléctionné
   * 
   * Exemple : "Cliquer sur le bouton pour choisir un fichier"
   */
    QString texteSiVide;
    /**
     * @brief Fichier choisi
     * 
     * Chaine vide quand aucun fichier n'a été choisi
     */
    QString fileSelect;
    /**
     * @brief Bouton qui lance la sélection
     * 
     */
    QPushButton *boutonSelection;
    /**
     * @brief Texte qui affiche l'état actuel du widget
     * 
     */
    QLineEdit *texteAffiche;
};

#endif