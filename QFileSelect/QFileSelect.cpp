#include "QFileSelect.h"
#include "qlineedit.h"
#include "qpushbutton.h"
#include <fstream>
QFileSelect::QFileSelect(QString restrictions, QString texteBouton,
                         QString texteSiVide, QWidget *parent) {
    this->restrictions = restrictions;
    this->texteSiVide = texteSiVide;
    this->fileSelect = "";

    QHBoxLayout *hbox = new QHBoxLayout(this);
    hbox->addWidget(this->boutonSelection = new QPushButton(texteBouton, parent));
    hbox->addWidget(this->texteAffiche =
                        new QLineEdit(texteSiVide, parent));
    this->texteAffiche->setReadOnly(true);

    connect(this->boutonSelection, &QPushButton::clicked, [=](){clicked();});
    connect(this->boutonSelection, &QPushButton::clicked, [=](){tryToCatchCorrectFile();});
    connect(this->texteAffiche, &QLineEdit::textChanged, [=](){textChanged();});
};

bool QFileSelect::isThereFileSelect() { return this->fileSelect != ""; };

const QString & QFileSelect::getFileSelect() {
    return fileSelect;
};
void QFileSelect::setFileSelect(const QString & nameFile) {
    fileSelect = nameFile;
};

void QFileSelect::tryToCatchCorrectFile() {
    QString fileName = QFileDialog::getOpenFileName(
        this, "Choisir un fichier", "/home", this->restrictions);

    if (fileName.toStdString().size() != 0) { // Un fichier est choisi
        ifstream file(fileName.toStdString());

        if (file.is_open()) {
            file.close();
            QString ch{"Choisi : "};
            ch = ch + fileName;
            this->texteAffiche->setText(ch);
            this->fileSelect = fileName;
            emit whenCatchCorrectFile(fileName);
        } else {
            QString ch{"Incorrect : "};
            ch = ch + fileName;
            this->texteAffiche->setText(ch);
            this->fileSelect = "";
            emit whenCatchIncorrectFile();
        }
    }
};